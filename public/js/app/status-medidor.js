$(document).ready(function() {

    $("#content-modal-status-medidor").on("click", "#fechar", function() {
        $('#modal-form-status-medidor').modal('toggle');
    });

    $("#content-modal-status-medidor").on("submit", "#form-status-medidor",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "status-medidor/store",
            data: $("#form-status-medidor").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-status-medidor').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-status-medidor');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "status-medidor/create", function(resposta){
        $("#content-modal-status-medidor").html(resposta);
        $('#modal-form-status-medidor').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "status-medidor/edit/" + id, function(resposta){
        $("#content-modal-status-medidor").html(resposta);
        $('#modal-form-status-medidor').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Status de medidor',
        html: 'Deseja realmente excluir o status de medidor <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "status-medidor/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Status de medidor',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do status de medidor.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
