$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });

    $('#content-modal-usuario').change('#cliente',function() {
        checkUserCliente();
    });
    
    $("#content-modal-usuario").on("submit", "#form-usuario",function(event){
        event.preventDefault();
        document.getElementById("salvar-usuario").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "usuario/store",
            data: $("#form-usuario").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-usuario').modal('toggle');
                    setTimeout(function(){
                        swal({
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }, 600);
                }else{
                    document.getElementById("salvar-usuario").disabled = false;
                    if(data.validate === true){
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);
                    }
                    else {
                        setTimeout(function(){
                            swal({
                                html: data.msg,
                                type: "warning",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }, 600);
                    }
                }
            }
        });
    });

    $("#content-modal-usuario").on("click", "#add-funcao", function()
    {
        var idFuncao = $("#funcao-option").val(), textFuncao = $("#funcao-option :selected").text(), funcoes = getFuncoes(), adicionar = true;

        if(idFuncao != '' && idFuncao != 0)
        {
            funcoes.forEach(function(item, index) {
                if(item.funcao == idFuncao && item.deletar == false)
                {
                    swal({
                        html: 'Função já existente na lista.',
                        type: "warning",
                        timer: 800,
                        showConfirmButton: false
                    });
                    adicionar = false;
                }
            });

            if(adicionar == true)
            {
                addFuncao(funcoes, 0, idFuncao, textFuncao, false);
            }

            $("#funcao-option").val(0).change();
        }
        else{
            swal({
                html: 'Função não selecionada.',
                type: "warning",
                timer: 800,
                showConfirmButton: false
            });
        }
    });


    $("#change-profile-form").on("submit",function(event){
        event.preventDefault();
        waitingDialog.show('Salvando...');

        document.getElementById("salvar-change-profile").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "usuario/profile/change",
            data: $("#change-profile-form").serialize(),
            success: function(data){
                if (data.success === true) {

                    setTimeout(function(){
                        swal({
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }, 600);
                    // showAlert('success', data.msg);
                    // setTimeout(function(){
                    //     location.reload();
                    // }, 1500);
                }else{
                    document.getElementById("salvar-change-profile").disabled = false;
                    if(data.validate === true){
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);
                    }
                    else {
                        setTimeout(function(){
                            swal({
                                title: 'Perfil',
                                html: data.msg,
                                type: "warning",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }, 600);
                    }
                }
                waitingDialog.hide();
            }
        });
    });
});

function inserirUsuario(){
    $.get(urlBase + 'usuario/create', function (resposta) {
        $("#content-modal-usuario").html(resposta);
        $('#modal-form-usuario').modal('show');
        $('#modal-form-usuario').on('shown.bs.modal', function () {
            $('#focus-usuario').focus();
        });
    }).fail(function(resposta) {
        if(resposta.status == 401){
            swal({
                html: "Sem permissão para acessar essa funcionalidade.",
                type: "warning",
                timer: 2000,
                showConfirmButton: false
            });
        }
    });
}

function editarUsuario(id){
    $.get(urlBase + "usuario/edit/" + id, function(resposta){
        $("#content-modal-usuario").html(resposta);
        $('#modal-form-usuario').modal('show');
        $('#modal-form-usuario').on('shown.bs.modal', function () {
            $('#focus-usuario').blur();
        });
    }).fail(function(resposta) {
        if(resposta.status == 401){
            swal({
                html: "Sem permissão para acessar essa funcionalidade.",
                type: "warning",
                timer: 2000,
                showConfirmButton: false
            });
        }
    });
}

function deletarUsuario (id, nome){
    swal({
        title: 'Excluir Usuário',
        html: 'Deseja realmente excluir o Usuário <b>' + nome + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((result) => {
        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "usuario/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Usuários", data.msg , "error");
                    }
                    waitingDialog.hide();
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do Usuário.' , "error");
                }
            });
        }
    });
    // $.confirm({
    //     title: 'Excluir Usuários',
    //     content: 'Deseja realmente excluir a usuário <strong>' + nome + '</strong>?',
    //     buttons: {
    //         removerFuncao: {
    //             btnClass: 'btn-red',
    //             text: 'Sim',
    //             action: function () {
    //                 $.ajax({
    //                     type: "POST",
    //                     url: urlBase + "usuario/destroy",
    //                     data: {_token: _token, id: id},
    //                     success: function(data){
    //                         if (data.success == true) {
    //                             showAlert('success', data.msg);
    //                             setTimeout(function(){ location.reload(); }, 1000);
    //                         }else{
    //                             showAlert('warning', data.msg);
    //                         }
    //                         waitingDialog.hide();
    //                     }, error: function(){
    //                         showAlert('warning', 'Não é permitido a exclusão do Usuário.');
    //                     }
    //                 });
    //             }
    //         },
    //         cancel: {
    //             text: 'Não',
    //             action: function() {return true;}
    //         }
    //     }
    // });
}

function addFuncao(funcoes, id, idFuncao, textFuncao, deletar)
{
    funcoes.push({ id: id, funcao: idFuncao, deletar: deletar });
    setFuncao(funcoes);
    $("#table-body-funcoes").append("<tr><td>"+ textFuncao +"</td><td style=\"width: 45px;\"><button type=\"button\" class=\"btn btn-danger btn-xs btn-flat\" onclick=\"deletarFuncao(this, '"+idFuncao+"');\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></button></td></tr>");
}

function getFuncoes()
{
    var funcoes = $("#funcoes-values").val();
    if(funcoes)
    {
        return JSON.parse(funcoes);
    }else{
        return [];
    }
}

function setFuncao(funcoes)
{
    $("#funcoes-values").val(JSON.stringify(funcoes));
}

function loadFuncoes()
{
    var idUser = $("#id-user").val();

    if(idUser != '')
    {

        $.ajax({
            type: "GET",
            url: urlBase + "usuario/get-funcoes/" + idUser,
            data: {},
            success: function(data){
                if(data)
                {
                    data.forEach(function(item, index) {
                        addFuncao(getFuncoes(), item.id, item.role_id,  item.description, false);
                    });
                }

                waitingDialog.hide();
            }, error: function(){
                waitingDialog.hide();
                showAlert('danger', 'Erro ao carregar Funções.');
            }
        });
    }
}

function deletarFuncao(element, idFuncao)
{
    var funcoes = getFuncoes();

    funcoes.forEach(function(item, index, object) {
        if(item.funcao == idFuncao)
        {
            if(item.id != '')
            {
                item.deletar = true;
            }else{
                object.splice(index, 1);
            }

            $(element).closest("tr").remove();
        }
    });

    setFuncao(funcoes);
}

function  checkUserCliente() {
    if($("#cliente").is(":checked") == true){
        $('#cliente').val(1);
        $('.row-permissions').hide();
        $('.row-clientes').show();
    }
    else {
        $('#cliente').val(0);
        $('.row-permissions').show();
        $('.row-clientes').hide();
    }
}

function checkinView() {
    if($("#cliente").is(":checked") === true){
        $('.row-permissions').hide();
        $('.row-clientes').show();
    }
    else{
        $('.row-permissions').show();
        $('.row-clientes').hide();
    }
}

