$(document).ready(function() {

    $("#content-modal-ineficiencia").on("click", "#fechar", function() {
        $('#modal-form-ineficiencia').modal('toggle');
    });

    $("#content-modal-ineficiencia").on("submit", "#form-ineficiencia",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "ineficiencia/store",
            data: $("#form-ineficiencia").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-ineficiencia').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-ineficiencia');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "ineficiencia/create", function(resposta){
        $("#content-modal-ineficiencia").html(resposta);
        $('#modal-form-ineficiencia').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "ineficiencia/edit/" + id, function(resposta){
        $("#content-modal-ineficiencia").html(resposta);
        $('#modal-form-ineficiencia').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Ineficiência',
        html: 'Deseja realmente excluir a ineficiência <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "ineficiencia/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Ineficiências',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão da ineficiência.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
