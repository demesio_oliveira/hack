$(document).ready(function() {

    $('.select2').select2();

    $("#content-modal-entrevista").on("click", "#fechar", function() {
        $('#modal-form-entrevista').modal('toggle');
    });

    $('#telefone').inputmask('(99) 9999[9]-9999', { removeMaskOnSubmit: true });
    $('#telefone_transf').inputmask('(99) 9999[9]-9999', { removeMaskOnSubmit: true });
    $('#cpf').inputmask('999.999.999-99', { removeMaskOnSubmit: true });
    $('#cpf_transf').inputmask('999.999.999-99', { removeMaskOnSubmit: true });
    $('#cpf_morador').inputmask('999.999.999-99', { removeMaskOnSubmit: true });
    $('#cep').inputmask('99.999-999', { removeMaskOnSubmit: true });
    $("#debito_total").inputmask( 'currency',{"autoUnmask": true,
        radixPoint:",",
        groupSeparator: ".",
        allowMinus: false,
        prefix: 'R$ ',
        digits: 2,
        digitsOptional: false,
        rightAlign: true,
        unmaskAsNumber: true,
        removeMaskOnSubmit: true
    });

    $("#form-entrevista").on("submit",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        validateMedidores();
        validateAparelhos();
        if (validateMedidores() == true && validateAparelhos() == true) {
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: urlBase + "entrevista/store",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data){
                    if (data.success === true) {
                        setTimeout(function () {
                            swal({
                                title: data.msg,
                                type: "success",
                                timer: 2000,
                                showConfirmButton: false
                            }).then((result) => {
                                location.reload();
                            });
                        });
                    } else {
                        document.getElementById("salvar").disabled = false;
                        if (data.success === false) {
                            var erros = data.msg;
                            var keys = Object.keys(erros);

                            validateErrorMessage(erros, 'danger', keys);

                        } else {

                            showAlertModal('warning', data.msg);

                        }
                    }
                }
            });
        }

    });

});



function  dadosCliente(id)
{
   var id = id;
   $.get(urlBase + "entrevista/dados-cliente/" + id, function(resposta){
       $("#instalacao").val(resposta.instalacao);
       $("#nr_cliente").val(resposta.nr_cliente);
       $("#nome").val(resposta.nome);
       $("#cpf").val(resposta.cpf);
       $("#telefone").val(resposta.telefone);
       $("#endereco").val(resposta.endereco);
       $("#bairro").val(resposta.bairro);
       $("#cep").val(resposta.cep);
       $("#nr_medidor").val(resposta.nr_medidor);
       $("#sequencia_leitura").val(resposta.sequencia_leitura);
       $("#ultima_leitura").val(resposta.ultima_leitura);
       $("#nota_leitura").val(resposta.nota_leitura);
       $("#debito_total").val(resposta.debito_total);
       $("#consumo_medio").val(resposta.consumo_medio);
       $("input[name='tarifa_social']").prop("checked", resposta.tarifa_social);
       $("input[name='cortado_sistema']").prop("checked", resposta.cortado_sistema);
       $("input[name='contrato_ativo']").prop("checked", resposta.contrato_ativo);
       $("input[name='toi']").prop("checked", resposta.toi);

   }).fail(function(resposta) {

   });
}

function deletar(id, nome){

    swal({
        title: 'Excluir Entrevista',
        html: 'Deseja realmente excluir a entrevista <b>' + nome + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "entrevista/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Entrevistas',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão da entrevista.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}

function deletarImagem(indice, src) {

    $.confirm({
        title: 'Deletar Imagem',
        content: 'Deseja realmente deletar esta imagem?',
        buttons: {
            deleteUser: {
                text: 'Sim',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: urlBase + "entrevista/delete-imagem",
                        data: {_token: _token, src: src},
                        success: function (data) {
                            if (data.success == true) {
                                location.reload();
                                showAlertModal('success', data.msg);
                            } else {
                                showAlertModal('warning', data.msg);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function () {
                    return true;
                }
            }
        }
    });
}

function exportarExcel() {
    window.location = urlBase + 'entrevista/export/excel?' + $("#form-avancado").serialize();
}

function validateVisita() {

    data = document.getElementById("data_visita").value;
    medidorVizinho = document.getElementById("medidor_vizinho").value;
    medidorAntes = document.getElementById("medidor_antes").value;
    medidorDepois = document.getElementById("medidor_depois").value;
    medidorEncontrado = document.getElementById("medidor_encontrado").value;

    if (data == null || data == "") {
        name = "data_visita";
        type = "danger";
        msg = "Campo obrigatório";
        simpleValidateErrors(name,type, msg);
    }
    else if (medidorVizinho == null || medidorVizinho == "") {
        name = "medidor_vizinho";
        type = "danger";
        msg = "Campo obrigatório";
        simpleValidateErrors(name,type, msg);
    }
    else if (medidorAntes == null || medidorAntes == "") {
        name = "medidor_antes";
        type = "danger";
        msg = "Campo obrigatório";
        simpleValidateErrors(name,type, msg);
    }
    else if (medidorDepois == null || medidorDepois == "") {
        name = "medidor_depois";
        type = "danger";
        msg = "Campo obrigatório";
        simpleValidateErrors(name,type, msg);
    }
    else if (medidorEncontrado == null || medidorEncontrado == "") {
        name = "medidor_encontrado";
        type = "danger";
        msg = "Campo obrigatório";
        simpleValidateErrors(name,type, msg);
    }
    else if(/\D/.test(medidorVizinho)){
        name = "medidor_vizinho";
        type = "danger";
        msg = "O campo medidor vizinho deve conter apenas números.";
        simpleValidateErrors(name,type, msg);
    }
    else if(/\D/.test(medidorAntes)){
        name = "medidor_antes";
        type = "danger";
        msg = "O campo medidor antes deve conter apenas números.";
        simpleValidateErrors(name,type, msg);
    }
    else if(/\D/.test(medidorDepois)){
        name = "medidor_depois";
        type = "danger";
        msg = "O campo medidor depois deve conter apenas números.";
        simpleValidateErrors(name,type, msg);
    }
    else if(/\D/.test(medidorEncontrado)){
        name = "medidor_encontrado";
        type = "danger";
        msg = "O campo medidor encontrado deve conter apenas números.";
        simpleValidateErrors(name,type, msg);
    }
    else {
        storeVisita();
    }
}

function pesquisar(){
    $("#form-avancado").submit();
}

function validateMedidores() {
    var x, text;
    var medidores = $("input[name='medidores_id[]']:checked").length;

    if (medidores === 0) {
        document.getElementById("salvar").disabled = false;
        text = "Selecione ao menos um medidor";
        document.getElementById("campoMedidores").innerHTML = text;
    } else {
        text = "";
        document.getElementById("campoMedidores").innerHTML = text;
        return true;
    }
}

function validateAparelhos() {
    var x, text;
    var aparelhosCarga = $("input[name='aparelhos_carga_id[]']:checked").length;

    if (aparelhosCarga === 0) {
        document.getElementById("salvar").disabled = false;
        text = "Selecione ao menos um aparelho";
        document.getElementById("campoAparelhos").innerHTML = text;
    } else {
        text = "";
        document.getElementById("campoAparelhos").innerHTML = text;
        return true;
    }

}
