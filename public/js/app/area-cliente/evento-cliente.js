$(document).ready(function () {
    var parqueUuid = $('#parque-uuid').val();

    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap3',
        header: {
            left:   'prev,next',
            center: 'title',
            right:  'month,agendaWeek,agendaDay'
        },
        buttonText: {
            month: 'Mês',
            agendaWeek: 'Semana',
            agendaDay: 'Dia'
        },
        locale: 'pt-br',
        height: 600,
        events      : { url: urlBase + 'area-cliente/evento/get-eventos/' + parqueUuid},
        selectable  : true,
        editable    : true,
        resizable: true,
        eventResize: function(event, delta, revertFunc) {
            if(event.cliente != false) {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: urlBase + "area-cliente/evento/update",
                    data: {
                        _token: _token,
                        uuid: event.uuid,
                        titulo: event.title,
                        inicio_evento: event.start.format("DD/MM/YYYY HH:mm"),
                        final_evento: event.end.format("DD/MM/YYYY HH:mm")
                    },
                    success: function (data) {
                        if (data.success != true) {
                            setTimeout(function () {
                                swal({
                                    html: data.msg,
                                    type: "warning",
                                    timer: 900,
                                    showConfirmButton: false
                                }).then((result) => {
                                    revertFunc();
                                });
                            }, 600);
                        }
                    }
                });
            }
            else{
                revertFunc();
            }
        },
        //quando eles selecionar(em) as data(s) abrir o modal de cadastro
        select: function (start, end) {
            $.get(urlBase + "area-cliente/evento/create/" + parqueUuid + '/' + start + '/' + end, function(resposta){
                $("#content-modal-evento-cliente").html(resposta);
                $('#modal-form-evento-cliente').modal('show');
                $('.colorpicker-component').colorpicker();
                $('#data-inicio').inputmask({'mask': "99/99/9999 99:99", greedy: false, reverse: false});
                $('#data-fim').inputmask({'mask': "99/99/9999 99:99", greedy: false, reverse: false});
            });
        },
        eventDrop: function(event, delta, revertFunc) {
            if(event.cliente != false){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: urlBase + "area-cliente/evento/update",
                    data: {_token : _token,uuid: event.uuid, titulo: event.title, inicio_evento: event.start.format("DD/MM/YYYY HH:mm"), final_evento: event.end.format("DD/MM/YYYY HH:mm")},
                    success: function(data){
                        if (data.success != true) {
                            setTimeout(function(){
                                swal({
                                    title: 'Evento',
                                    html: data.msg,
                                    type: "warning",
                                    timer: 900,
                                    showConfirmButton: false
                                }).then((result) => {
                                    revertFunc();
                            });
                            }, 600);
                        }
                    }
                });
            }
            else{
                revertFunc();
            }
        },
        eventClick: function (event, jsEvent, view) {
            if(event.cliente != false){
                swal({
                    html: '<b>Evento ' + event.title + '</b> <br/> De: ' + event.start.format("DD/MM/YYYY") + ' Até: ' +event.end.format("DD/MM/YYYY"),
                    type: "question",
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: "Editar",
                    cancelButtonText: "Deletar",
                    cancelButtonColor: "#DD6B55"
                }).then(result => {
                    if (result.value) {
                    $.get(urlBase + "area-cliente/evento/edit/" + event.uuid, function(resposta){
                        $("#content-modal-evento-cliente").html(resposta);
                        $('#modal-form-evento-cliente').modal('show');
                        $('.colorpicker-component').colorpicker();
                        $('#data-inicio').inputmask({'mask': "99/99/9999 99:99", greedy: false, reverse: false});
                        $('#data-fim').inputmask({'mask': "99/99/9999 99:99", greedy: false, reverse: false});
                    });
                }
                if(result.dismiss === "cancel"){
                    swal({
                        html: 'Deseja realmente excluir o evento <b>' + event.title + '</b>?',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Sim",
                        cancelButtonText: "Não",
                    }).then((result) => {
                        if(result.value){
                        $.ajax({
                            type: "POST",
                            url: urlBase + "area-cliente/evento/destroy",
                            data: {_token: _token, uuid: event.uuid},
                            success: function(data) {
                                if (data.success == true) {
                                    swal({
                                        text: data.msg,
                                        type: "success",
                                        timer: 800,
                                        showConfirmButton: false
                                    }).then((result) => {
                                        location.reload();
                                });
                                }
                                else{
                                    swal(data.msg , "error");
                                }
                            }
                        });

                    }
                });
                }

            });

            }
        }
    });

    $("#content-modal-evento-cliente").on("submit", "#form-evento-cliente",function(event){
        event.preventDefault();
        document.getElementById("salvar-evento-cliente").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "area-cliente/evento/store",
            data: $("#form-evento-cliente").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-evento-cliente').modal('toggle');
                    setTimeout(function(){
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 800,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                    });
                    }, 600);

                }else{
                    document.getElementById("salvar-evento-cliente").disabled = false;
                    if(data.validate === true){
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);
                    }
                    else {
                        setTimeout(function(){
                            swal({
                                html: data.msg,
                                type: "warning",
                                timer: 800,
                                showConfirmButton: false
                            });
                        }, 600);
                    }
                }
            }
        });
    });
});