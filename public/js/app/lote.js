$(document).ready(function() {

    $('#carregar-planilha').click(function () {

        waitingDialog.show("Carregando Planilha...");

        var formData = new FormData($("#form-importar")[0]);

        $.ajax({
            type: "POST",
            url: urlBase + "lote/importar/load",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data){
                $("#table-response").html(data);
                $("#importar-planilha").css("display", "block");
                waitingDialog.hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#importar-planilha").css("display", "none");
                waitingDialog.hide();
                swal("Error!", xhr.responseJSON, "error");
            }
        });

    });

});