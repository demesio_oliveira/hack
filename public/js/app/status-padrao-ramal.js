$(document).ready(function() {

    $("#content-modal-status-padrao-ramal").on("click", "#fechar", function() {
        $('#modal-form-status-padrao-ramal').modal('toggle');
    });

    $("#content-modal-status-padrao-ramal").on("submit", "#form-status-padrao-ramal",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "status-padrao-ramal/store",
            data: $("#form-status-padrao-ramal").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-status-padrao-ramal').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-status-padrao-ramal');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "status-padrao-ramal/create", function(resposta){
        $("#content-modal-status-padrao-ramal").html(resposta);
        $('#modal-form-status-padrao-ramal').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "status-padrao-ramal/edit/" + id, function(resposta){
        $("#content-modal-status-padrao-ramal").html(resposta);
        $('#modal-form-status-padrao-ramal').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Status de padrão de ramal',
        html: 'Deseja realmente excluir o status de padrão de ramal <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "status-padrao-ramal/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Status de padrão de ramal',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do status de padrão de ramal.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
