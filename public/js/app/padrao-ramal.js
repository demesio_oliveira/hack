$(document).ready(function() {

    $("#content-modal-padrao-ramal").on("click", "#fechar", function() {
        $('#modal-form-padrao-ramal').modal('toggle');
    });

    $("#content-modal-padrao-ramal").on("submit", "#form-padrao-ramal",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "padrao-ramal/store",
            data: $("#form-padrao-ramal").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-padrao-ramal').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-padrao-ramal');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "padrao-ramal/create", function(resposta){
        $("#content-modal-padrao-ramal").html(resposta);
        $('#modal-form-padrao-ramal').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "padrao-ramal/edit/" + id, function(resposta){
        $("#content-modal-padrao-ramal").html(resposta);
        $('#modal-form-padrao-ramal').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Padrão de ramal',
        html: 'Deseja realmente excluir o padrão de ramal <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "padrao-ramal/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Padrões de ramal',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do padrão de ramal.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
