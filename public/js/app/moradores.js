$(document).ready(function() {

    $("#content-modal-moradores").on("click", "#fechar", function() {
        $('#modal-form-moradores').modal('toggle');
    });

    $("#content-modal-moradores").on("submit", "#form-moradores",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "moradores/store",
            data: $("#form-moradores").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-moradores').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-moradores');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "moradores/create", function(resposta){
        $("#content-modal-moradores").html(resposta);
        $('#modal-form-moradores').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "moradores/edit/" + id, function(resposta){
        $("#content-modal-moradores").html(resposta);
        $('#modal-form-moradores').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Moradores',
        html: 'Deseja realmente excluir os moradores <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "moradores/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Moradores',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Moradores", data.msg , "error");
                    }
                }, error: function(){
                    swal("Moradores", 'Não é permitido a exclusão dos moradores.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Moradores", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
