$(document).ready(function() {

    $("#content-modal-tipo-estrutura").on("click", "#fechar", function() {
        $('#modal-form-tipo-estrutura').modal('toggle');
    });

    $("#content-modal-tipo-estrutura").on("submit", "#form-tipo-estrutura",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "tipo-estrutura/store",
            data: $("#form-tipo-estrutura").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-tipo-estrutura').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-tipo-estrutura');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "tipo-estrutura/create", function(resposta){
        $("#content-modal-tipo-estrutura").html(resposta);
        $('#modal-form-tipo-estrutura').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "tipo-estrutura/edit/" + id, function(resposta){
        $("#content-modal-tipo-estrutura").html(resposta);
        $('#modal-form-tipo-estrutura').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Tipo de estrutura',
        html: 'Deseja realmente excluir o tipo de estrutura <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "tipo-estrutura/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Tipos de estrutura',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do tipo de estrutura.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
