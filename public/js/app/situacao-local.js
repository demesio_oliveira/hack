$(document).ready(function() {

    $("#content-modal-situacao-local").on("click", "#fechar", function() {
        $('#modal-form-situacao-local').modal('toggle');
    });

    $("#content-modal-situacao-local").on("submit", "#form-situacao-local",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "situacao-local/store",
            data: $("#form-situacao-local").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-situacao-local').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-situacao-local');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "situacao-local/create", function(resposta){
        $("#content-modal-situacao-local").html(resposta);
        $('#modal-form-situacao-local').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "situacao-local/edit/" + id, function(resposta){
        $("#content-modal-situacao-local").html(resposta);
        $('#modal-form-situacao-local').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Situação do local',
        html: 'Deseja realmente excluir a situação do local <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "situacao-local/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Situações do local',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão da situação do local.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
