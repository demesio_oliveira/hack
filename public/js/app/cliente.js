$(document).ready(function() {

    $('.select2').select2();

    $("#content-modal-cliente").on("click", "#fechar", function() {
        $('#modal-form-cliente').modal('toggle');
    });

    $('#telefone').inputmask('(99) 9999[9]-9999', { removeMaskOnSubmit: true });
    $('#cpf').inputmask('999.999.999-99', { removeMaskOnSubmit: true });
    $('#cep').inputmask('99.999-999', { removeMaskOnSubmit: true });
    $("#debito_total").inputmask( 'currency',{"autoUnmask": true,
        radixPoint:",",
        groupSeparator: ".",
        allowMinus: false,
        prefix: 'R$ ',
        digits: 2,
        digitsOptional: false,
        rightAlign: true,
        unmaskAsNumber: true,
        removeMaskOnSubmit: true
    });

});

function deletar(id, descricao){

    swal({
        title: 'Excluir Cliente',
        html: 'Deseja realmente excluir o cliente <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "cliente/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Clientes',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do cliente.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}

function pesquisar(){
    $("#form-avancado").submit();
}
