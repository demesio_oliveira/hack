$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });

    $("#content-modal-funcao").on("click", "#add-permissao", function()
    {
        // options = select.getElementsByTagName("option");
        // option.removeAttribute("disabled");
        
        var idPermissao = $("#permissao-option").val(), textPermissao = $("#permissao-option :selected").text(), permissoes = getPermissoes(), adicionar = true;
        $("#permissao-option :selected").remove();
        
        if(idPermissao != "" && idPermissao != '0')
        {
            permissoes.forEach(function(item, index) {
                if(item.permissao == idPermissao && item.deletar == false)
                {
                    swal({
                        title: 'Permissões',
                        html: 'Permissão já existente na lista.',
                        type: "warning",
                        timer: 800,
                        showConfirmButton: false
                    });
                    adicionar = false;
                }
            });

            if(adicionar == true)
            {
                addPermissao(permissoes, 0, idPermissao, textPermissao,  false);
            }
            $("#permissao-option").val(0).change();
        }
        else{
            swal({
                title: 'Permissões',
                html: 'Permissão não selecionada.',
                type: "warning",
                timer: 800,
                showConfirmButton: false
            });
        }
    });

    $("#content-modal-funcao").on("submit", "#form-funcao",function(event){
        event.preventDefault();

        document.getElementById("salvar-funcao").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "funcao/store",
            data: $("#form-funcao").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-funcao').modal('toggle');
                    setTimeout(function(){
                        swal({
                            title: 'Funções',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }, 600);
                    // showAlertModal('success', data.msg);
                    // setTimeout(function(){
                    //     $('#modal-form-funcao').modal('toggle');
                    //     location.reload();
                    // }, 3000);
                }else{
                    document.getElementById("salvar-funcao").disabled = false;
                    if(data.validate === true){
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);
                    }
                    else {
                        setTimeout(function(){
                            swal({
                                title: 'Função',
                                html: data.msg,
                                type: "warning",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }, 600);
                    }
                }
            }
        });
    });
});


function inserirFuncao(){
    $.get(urlBase + 'funcao/create', function (resposta) {
        $("#content-modal-funcao").html(resposta);
        $('#modal-form-funcao').modal('show');
        $('#permissao-option').select2();
        $('#modal-form-funcao').on('shown.bs.modal', function () {
            $('#focus-funcao').focus();
        });
    }).fail(function(resposta) {
        if(resposta.status == 401){
            swal({
                html: "Sem permissão para acessar essa funcionalidade.",
                type: "warning",
                timer: 2000,
                showConfirmButton: false
            });
        }
    });
}

function editarFuncao(id){
    $.get(urlBase + "funcao/edit/" + id, function(resposta){
        $("#content-modal-funcao").html(resposta);
        $('#modal-form-funcao').modal('show');
        $('#permissao-option').select2();
    }).fail(function(resposta) {
        if(resposta.status == 401){
            swal({
                html: "Sem permissão para acessar essa funcionalidade.",
                type: "warning",
                timer: 2000,
                showConfirmButton: false
            });
        }
    });
}

function visualizarFuncao(id){
    $.get(urlBase + "funcao/show/" + id, function(resposta){
        $("#content-modal-funcao").html(resposta);
        $('#modal-form-funcao').modal('show');
        $('#permissao-option').select2();
    }).fail(function(resposta) {
        if(resposta.status == 401){
            swal({
                html: "Sem permissão para acessar essa funcionalidade.",
                type: "warning",
                timer: 2000,
                showConfirmButton: false
            });
        }
    });
}

function deletarFuncao (id, descricao){

    swal({
        title: 'Excluir Funções',
        html: 'Deseja realmente excluir a Função <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "funcao/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Funções',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão da Função.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}

function addPermissao(permissoes, id, idPermissao, textPermissao, deletar)
{
    permissoes.push({ id: id, permissao: idPermissao, deletar: deletar });
    setPermissao(permissoes);
    $("#table-body-permissoes").append("<tr><td>"+ textPermissao +"</td><td style=\"width: 45px;\"><button type=\"button\" class=\"btn btn-danger btn-xs btn-flat btn-danger-custom-no-border\" onclick=\"deletarPermissao(this, '"+idPermissao+"');\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></button></td></tr>");
}

function getPermissoes()
{
    var permissoes = $("#permissoes-values").val();
    console.log(permissoes);
    if(permissoes)
    {
        console.log(JSON.parse(permissoes));
        return JSON.parse(permissoes);
    }else{
        return [];
    }
}

function setPermissao(permissoes)
{
    $("#permissoes-values").val(JSON.stringify(permissoes));
}

function deletarPermissao(element, idPermissao)
{
    var permissoes = getPermissoes();
    permissoes.forEach(function(item, index, object) {
        var textSelectBack = ($(element).parent().parent().find('td')[0]);
        if(item.permissao == idPermissao)
        {
            $('#permissao-option').append($('<option>', {
                value: idPermissao,
                text: $(textSelectBack).html()
            }));
            if(item.id != '')
            {
                item.deletar = true;
            }else{
                object.splice(index, 1);
            }

            $(element).closest("tr").remove();
            sortSelect('permissao-option');
        }
    });
    setPermissao(permissoes);
}

function loadPermissoes() {
    var idRole = $('#id-role').val();

    if(idRole != ""){
        $.ajax({
            type: "GET",
            url: urlBase + "funcao/get-permissoes/" + idRole,
            data: {},
            success: function(data){
                if(data)
                {
                    data.forEach(function(item, index) {
                        addPermissao(getPermissoes(), item.id, item.permission_id,  item.description, false);
                    });
                }
            }, error: function(){
                showAlert('danger', 'Erro ao carregar Permissões.');
            }
        });
    }
}

function loadPermissoesOnlyView() {
    var idRole = $('#id-role').val();

    if(idRole != ""){
        $.ajax({
            type: "GET",
            url: urlBase + "funcao/get-permissoes/" + idRole,
            data: {},
            success: function(data){
                if(data)
                {
                    data.forEach(function(item, index) {
                        addPermissaoOnlyView(getPermissoes(), item.id, item.permission_id,  item.description, false);
                    });
                }
            }, error: function(){
                showAlert('danger', 'Erro ao carregar Permissões.');
            }
        });
    }
}

function addPermissaoOnlyView(permissoes, id, idPermissao, textPermissao, deletar)
{
    permissoes.push({ id: id, permissao: idPermissao, deletar: deletar });
    setPermissao(permissoes);
    $("#table-body-permissoes").append("<tr><td>"+ textPermissao +"</td><td style=\"width: 45px;\"></td></tr>");
}