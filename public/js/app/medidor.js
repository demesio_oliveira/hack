$(document).ready(function() {

    $("#content-modal-medidor").on("click", "#fechar", function() {
        $('#modal-form-medidor').modal('toggle');
    });

    $("#content-modal-medidor").on("submit", "#form-medidor",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "medidor/store",
            data: $("#form-medidor").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-medidor').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-medidor');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "medidor/create", function(resposta){
        $("#content-modal-medidor").html(resposta);
        $('#modal-form-medidor').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "medidor/edit/" + id, function(resposta){
        $("#content-modal-medidor").html(resposta);
        $('#modal-form-medidor').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Medidor',
        html: 'Deseja realmente excluir o medidor <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "medidor/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Medidores',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do medidor.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
