$(document).ready(function() {

    $("#content-modal-status-padrao-entrada").on("click", "#fechar", function() {
        $('#modal-form-status-padrao-entrada').modal('toggle');
    });

    $("#content-modal-status-padrao-entrada").on("submit", "#form-status-padrao-entrada",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "status-padrao-entrada/store",
            data: $("#form-status-padrao-entrada").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-status-padrao-entrada').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-status-padrao-entrada');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "status-padrao-entrada/create", function(resposta){
        $("#content-modal-status-padrao-entrada").html(resposta);
        $('#modal-form-status-padrao-entrada').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "status-padrao-entrada/edit/" + id, function(resposta){
        $("#content-modal-status-padrao-entrada").html(resposta);
        $('#modal-form-status-padrao-entrada').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Status de padrão de entrada',
        html: 'Deseja realmente excluir o status de padrão de entrada <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "status-padrao-entrada/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Status de padrão de entrada',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do status de padrão de entrada.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
