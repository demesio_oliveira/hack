$(document).ready(function() {

    $("#content-modal-comodos").on("click", "#fechar", function() {
        $('#modal-form-comodos').modal('toggle');
    });

    $("#content-modal-comodos").on("submit", "#form-comodos",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "comodos/store",
            data: $("#form-comodos").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-comodos').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-comodos');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "comodos/create", function(resposta){
        $("#content-modal-comodos").html(resposta);
        $('#modal-form-comodos').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "comodos/edit/" + id, function(resposta){
        $("#content-modal-comodos").html(resposta);
        $('#modal-form-comodos').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Cômodos',
        html: 'Deseja realmente excluir os cômodos <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "comodos/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Cômodos',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Cômodos", data.msg , "error");
                    }
                }, error: function(){
                    swal("Cômodos", 'Não é permitido a exclusão dos cômodos.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Cômodos", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
