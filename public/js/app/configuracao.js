var skin_change = "";
$(document).ready(function() {
    $('#cnpj').inputmask({'mask': "99.999.999/9999-99", greedy: false, reverse: false, autoUnmask: true});
    $('#cep').inputmask({'mask': "99999-999", greedy: false, reverse: true, autoUnmask: true});
    $('#telefone').inputmask({'mask': "(99) 9999-9999", greedy: false, reverse: true, autoUnmask: true});
    $('#celular').inputmask({'mask': "(99) 99999-9999", greedy: false, reverse: true, autoUnmask: true});
    $('#numero').inputmask({'mask': "99999", greedy: false, reverse: true, autoUnmask: true});
    $('#sigla').inputmask({'mask': "###", greedy: false, reverse: true, autoUnmask: true});

    $('.change-skin-layout').on("click", function () {
        var skinAntHtml = $('html').attr("class");
        var skinAnt = $('body').attr("class");
        skinAnt = skinAnt.split(" ");
        var skin = $(this).find('a').attr('data-skin');
        var classe = skin + " ";
        for (i = 1; i < skinAnt.length; i++) {
            classe += skinAnt[i] + " ";
        }
        $('body').removeClass().addClass(classe);
        $('header nav').removeClass(skinAnt[0] + "-pattern").addClass(skin + "-pattern");
        $('html').removeClass(skinAntHtml).addClass(skin + "-html");
        $("input[name=skin]").val(skin);
        /*$('body, html').animate({scrollTop: 0}, 500);*/
    });


    $("#form-configuracao").on("submit",function(event){
        event.preventDefault();
        //desabilitar quando for enviado a requisicao
        document.getElementById("salvar-configuracao").disabled = true;
        var formData = new FormData(this);

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "configuracao/store",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data){
                if (data.success === true) {
                    $('html,body').animate({scrollTop: 0},'slow');
                    setTimeout(function(){
                        swal({
                            title: 'Configuração',
                            html: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }, 600);

                }else{
                    $('html,body').animate({scrollTop: 0},'slow');
                    document.getElementById("salvar-configuracao").disabled = false;
                    if(data.validate === true){
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);
                    }
                    else {
                        setTimeout(function(){
                            swal({
                                title: 'Configuração',
                                html: data.msg,
                                type: "warning",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }, 600);
                    }
                }
            }
        });
    });
});

function deletarCliente (uuid, nome){
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
    swal({
        title: 'Excluir Cliente',
        html: 'Deseja realmente excluir o Cliente <b>' + nome + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((result) => {
        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "cliente/destroy",
                data: {_token: _token, uuid: uuid},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Cliente',
                            text: data.msg,
                            type: "success",
                            timer: 3000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                        // showAlert('success', data.msg);
                        // setTimeout(function(){ location.reload(); }, 1000);
                    }else{
                        swal("Cliente", data.msg , "error");
                    }
                }
            });
        }
    });
}

function deletarImagem(name){
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
    swal({
        title: 'Excluir' + ((name === "logo") ? ' Logo' : ' Banner'),
        html: 'Deseja realmente excluir ' + ((name === "logo") ? 'a Logo' : 'o Banner') + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true
    })
    .then((result) =>{
        if(result.value){
            $.ajax({
                type: 'POST',
                url: urlBase + 'configuracao/imagem/destroy',
                data: {_token: _token, name: name},
                success: function(data){
                    if (data.success === true) {
                        swal({
                            title: ((name === "logo") ? 'Logo' : 'Banner'),
                            text: data.msg,
                            type: "success",
                            timer: 3000,
                            showConfirmButton: false
                        }).then((result) => {
                            $('.image-upload-' + name).html('');
                        });
                        // showAlert('success', data.msg);
                        // setTimeout(function(){ location.reload(); }, 1000);
                    }else{
                        swal("Logo", data.msg , "error");
                    }
                }
            });
        }
    });
}

$("body").on("change", "#image-banner", function() {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.image-upload-banner').css("display", "block");
            $('.image-upload-banner img').remove();
            $('.image-upload-banner').html('<span title="Deletar banner" data-toggle="tooltip" class="fa fa-times delete-thumb delete-banner" onclick="$(\'.image-upload-banner\').html(\'\'); $(\'#image-banner\').val(\'\')"></span><div><img src="'+e.target.result+'"></div>');
        }
        reader.readAsDataURL(this.files[0]);
    }else{
        $('.image-upload-banner img').remove();
    }
});


$("body").on("change", "#image", function() {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.image-upload-logo').css("display", "block");
            $('.image-upload-logo').html('');
            $('.image-upload-logo').html('<span title="Deletar logo" data-toggle="tooltip" class="fa fa-times delete-thumb delete-logo" onclick="$(\'.image-upload-logo\').html(\'\'); $(\'#image\').val(\'\')"></span><div class="image-preview"><img src="'+e.target.result+'"></div>');
        }
        reader.readAsDataURL(this.files[0]);
    }else{
        $('.image-upload-logo img').remove();
    }
});

