$(document).ready(function() {

    $("#content-modal-problema").on("click", "#fechar", function() {
        $('#modal-form-problema').modal('toggle');
    });

    $("#content-modal-problema").on("submit", "#form-problema",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "problema/store",
            data: $("#form-problema").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-problema').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-problema');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "problema/create", function(resposta){
        $("#content-modal-problema").html(resposta);
        $('#modal-form-problema').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "problema/edit/" + id, function(resposta){
        $("#content-modal-problema").html(resposta);
        $('#modal-form-problema').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Problema',
        html: 'Deseja realmente excluir o problema <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "problema/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Problemas',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do problema.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
