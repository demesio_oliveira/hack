$(document).ready(function() {

    $("#content-modal-tipo").on("click", "#fechar", function() {
        $('#modal-form-tipo').modal('toggle');
    });

    $("#content-modal-tipo").on("submit", "#form-tipo",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "tipo/store",
            data: $("#form-tipo").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-tipo').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-tipo');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "tipo/create", function(resposta){
        $("#content-modal-tipo").html(resposta);
        $('#modal-form-tipo').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "tipo/edit/" + id, function(resposta){
        $("#content-modal-tipo").html(resposta);
        $('#modal-form-tipo').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Tipo',
        html: 'Deseja realmente excluir o tipo <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "tipo/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Tipos',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do tipo.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
