$(document).ready(function() {

    $("#content-modal-motivo-impedimento").on("click", "#fechar", function() {
        $('#modal-form-motivo-impedimento').modal('toggle');
    });

    $("#content-modal-motivo-impedimento").on("submit", "#form-motivo-impedimento",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "motivo-impedimento/store",
            data: $("#form-motivo-impedimento").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-motivo-impedimento').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-motivo-impedimento');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "motivo-impedimento/create", function(resposta){
        $("#content-modal-motivo-impedimento").html(resposta);
        $('#modal-form-motivo-impedimento').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "motivo-impedimento/edit/" + id, function(resposta){
        $("#content-modal-motivo-impedimento").html(resposta);
        $('#modal-form-motivo-impedimento').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Motivo de impedimento',
        html: 'Deseja realmente excluir o motivo de impedimento <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "motivo-impedimento/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Motivos de impedimento',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do motivo de impedimento.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
