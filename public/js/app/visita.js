$(document).ready(function() {

    $("#content-modal-dados-visita").on("click", "#fechar", function() {
        $('#modal-form-dados-visita').modal('toggle');
    });
    loadVisitas();
});

function remove(...forDeletion) {
    return this.filter(item => !forDeletion.includes(item))
}

function inserir(){
    $.get(urlBase + "dados-visita/create", function(resposta){
        $("#content-modal-dados-visita").html(resposta);
        $('#modal-form-dados-visita').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id){

    swal({
        title: 'Excluir visita',
        html: 'Deseja realmente excluir a visita?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "dados-visita/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Visita',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Visita", data.msg , "error");
                    }
                }, error: function(){
                    swal("Visita", 'Não é permitido a exclusão da visita.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Cômodos", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}

function getVisita() {
    var visita = $("#dados_visita").val();
    if (visita) {
        return JSON.parse(visita);
    } else {
        return [];
    }
}

function setVisita(visita) {
    $("#dados_visita").val(JSON.stringify(visita));
}

function deletarVisita(element){
    var visita = getVisita();
    var add = document.getElementById("adicionar_visita");
    var linhas = document.getElementsByClassName("linha_visita");
    let id = element.parent().parent().prop('id');
    element.parent().parent().remove()
    console.log(id);
    for(var i = visita.length - 1; i >= 0; i--) {
        if(visita[i].id == id) {
            visita.splice(i, 1);
        }
    }
    if (linhas.length < 3){
        add.classList.remove("disabled");
    }
    setVisita(visita);

}

function getId(visita) {
    if(visita.length <= 0) {
        return 1;
    }
    lastId = visita[visita.length - 1].id;
    return lastId+1;
}

function exibirVisita(item)
{
    var add = document.getElementById("adicionar_visita");
    var linhas = document.getElementsByClassName("linha_visita");
    var motivo_descricao = item.motivo_impedimento_descricao;
    if (x.length > 0) {
        x = document.getElementsByClassName('img-box');
        base64img = x[0].src;
    }

    var newRow = '<tr class="linha_visita" id=' +'"'+ item.id +'"'+'>'+'<td>'+ item.data_visita +'</td>';
    newRow+= '<td>' + item.medidor_encontrado +'</td>';
    newRow+= '<td>' + item.medidor_vizinho +'</td>';
    newRow+= '<td>' + item.medidor_antes +'</td>';
    newRow+= '<td>' + item.medidor_depois +'</td>';
    newRow+= '<td>' + motivo_descricao +'</td>';
    newRow+= '<td>' + '<img style="max-width: 30px" src="' + base64img + '"/>' +'</td>';
    newRow+= '<td align="center"><button type="button" data-toggle="tooltip" title="Remover" class="btn btn-cust-danger btn-xs" onclick="deletarVisita($(this));"><span class="fa fa-remove" aria-hidden="true"></span></button></td>';
    newRow+= '</tr>';
    $("#table-body-visitas").append(newRow);
    if (linhas.length >= 3){
        add.classList.add("disabled");
    }
    $('#modal-form-dados-visita').modal('toggle');




}

function storeVisita() {
    var visita = getVisita();
    x = document.getElementsByClassName('img-box');
    base64img = "";
    if (x.length > 0){
        base64img = x[0].src;
        base64img = base64img.replace("data:image/jpeg;base64,", "");
        base64img = base64img.replace('"', '');
    }

    visita.push({id : getId(visita), data_visita : $("#data_visita").val(), medidor_encontrado : $("#medidor_encontrado").val(), medidor_vizinho : $("#medidor_vizinho").val(), medidor_antes : $("#medidor_antes").val(), medidor_depois : $("#medidor_depois").val(), motivo_impedimento_descricao: $("#motivo_impedimento_id :selected").text(), motivo_impedimento_id : $("#motivo_impedimento_id").val() == 0 ? null : $("#motivo_impedimento_id").val(), imagem_visita : base64img, lat_inicio_visita : $("#lat_inicio_visita").val(), long_inicio_visita : $("#long_inicio_visita").val(), lat_termino_visita : $("#lat_termino_visita").val(), long_termino_visita : $("#long_termino_visita").val()});
    setVisita(visita);
    exibirVisita(visita[visita.length - 1]);

}

function loadVisitas() {
    var id = $("#id").val();
    var uuid = $("#uuid_visita").val();
    if (id != "" && uuid == "") {
        $.ajax({
            type: "GET",
            url: urlBase + "entrevista/load-visitas/" + id,
            success: function (data) {
                if (data) {
                    data.forEach(function (item, index) {
                        var visita = getVisita();
                        visita.push({id : getId(visita), data_visita : item.data_visita, medidor_encontrado : item.medidor_encontrado, medidor_vizinho : item.medidor_vizinho, medidor_antes : item.medidor_antes, medidor_depois : item.medidor_depois, motivo_impedimento_id : item.impedimento_id, motivo_impedimento_descricao: item.impedimento_descricao});
                        setVisita(visita);
                    });
                }
            }, error: function () {
                swal('Erro ao carregar visitas', '', "error");
            }
        });
    }
}

function imageToBase64(img)
{
    var canvas, ctx, dataURL, base64;
    canvas = document.createElement("canvas");
    ctx = canvas.getContext("2d");
    canvas.width = img.width;
    canvas.height = img.height;
    ctx.drawImage(img, 0, 0);
    dataURL = canvas.toDataURL("image/png");
    base64 = dataURL.replace(/^data:image\/png;base64,/, "");
    return base64;
}