$(document).ready(function() {

    $("#content-modal-renda-familiar-mensal").on("click", "#fechar", function() {
        $('#modal-form-renda-familiar-mensal').modal('toggle');
    });

    $("#content-modal-renda-familiar-mensal").on("submit", "#form-renda-familiar-mensal",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "renda-familiar-mensal/store",
            data: $("#form-renda-familiar-mensal").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-renda-familiar-mensal').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-renda-familiar-mensal');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "renda-familiar-mensal/create", function(resposta){
        $("#content-modal-renda-familiar-mensal").html(resposta);
        $('#modal-form-renda-familiar-mensal').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "renda-familiar-mensal/edit/" + id, function(resposta){
        $("#content-modal-renda-familiar-mensal").html(resposta);
        $('#modal-form-renda-familiar-mensal').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Renda familiar mensal',
        html: 'Deseja realmente excluir a renda familiar mensal <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "renda-familiar-mensal/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Rendas familiares mensais',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão da renda familiar mensal.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
