$(document).ready(function() {

    $("#content-modal-aparelho-carga").on("click", "#fechar", function() {
        $('#modal-form-aparelho-carga').modal('toggle');
    });

    $("#content-modal-aparelho-carga").on("submit", "#form-aparelho-carga",function(event){
        event.preventDefault();

        document.getElementById("salvar").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "aparelho-carga/store",
            data: $("#form-aparelho-carga").serialize(),
            success: function(data){
                if (data.success === true) {
                    $('#modal-form-aparelho-carga').modal('toggle');
                    setTimeout(function () {
                        swal({
                            title: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    });
                } else {
                    document.getElementById("salvar").disabled = false;
                    if (data.success === false) {
                        var erros = data.msg;
                        var keys = Object.keys(erros);
                        validateErrorMessage(erros, 'danger', keys);

                    } else {

                        showAlertModal('warning', data.msg);

                    }
                }
                CustomPreload.hide('content-modal-aparelho-carga');
            }
        });
    });
});

function inserir(){
    $.get(urlBase + "aparelho-carga/create", function(resposta){
        $("#content-modal-aparelho-carga").html(resposta);
        $('#modal-form-aparelho-carga').modal('show');
    }).fail(function(resposta) {

    });
}

function editar(id){
    $.get(urlBase + "aparelho-carga/edit/" + id, function(resposta){
        $("#content-modal-aparelho-carga").html(resposta);
        $('#modal-form-aparelho-carga').modal('show');
    }).fail(function(resposta) {

    });
}

function deletar(id, descricao){

    swal({
        title: 'Excluir Aparelho carga',
        html: 'Deseja realmente excluir o aparelho carga <b>' + descricao + '</b>?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true

    }).then((result) => {

        if(result.value){
            $.ajax({
                type: "POST",
                url: urlBase + "aparelho-carga/destroy",
                data: {_token: _token, id: id},
                success: function(data){
                    if (data.success == true) {
                        swal({
                            title: 'Aparelhos carga',
                            text: data.msg,
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        swal("Funções", data.msg , "error");
                    }
                }, error: function(){
                    swal("Funções", 'Não é permitido a exclusão do aparelho carga.' , "error");
                }
            }).fail(function(resposta) {
                if(resposta.status == 401){
                    swal("Funções", "Sem permissão para acessar essa funcionalidade." , "error");
                }
            });
        }
    });
}
