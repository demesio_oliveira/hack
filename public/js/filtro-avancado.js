$("#filtro_avancado").click(function(){
	$(".add-btn").parent().removeClass("col-md-7").addClass("col-md-3");
	$("input[name=filtro]").val("");
	var exists = $(".add-btn button");
	$(".add-btn button").remove();
	if(exists.length == 0){
		var btn = '<button class="btn btn-primary btn-flat btn-block btn-primary-custom" onclick="pesquisar();"><i class="fa fa-search"></i> Busca Avançada</button>';
		$(".add-btn").append(btn);
		$(".add-btn").parent().removeClass("col-md-3").addClass("col-md-7");
	}
	$(".filtro").slideToggle(5);
});