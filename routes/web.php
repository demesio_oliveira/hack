<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cpf', function (\Illuminate\Support\Facades\Request $request){

    $url = "https://ws.iwebservice.com.br/CPF/?chave=FBB4W-28AU2-520WR-9B1K1&cpf=088.172.824-13&dataNascimento=30/12/1992&formato=JSON";
    $agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)";

    $ch = curl_init();

    curl_setopt_array($ch, array(
        CURLOPT_URL => $url, //url que produz a imagem do captcha.
        CURLOPT_USERAGENT => $agent,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_SSL_VERIFYPEER => false
    ));

    $response = json_decode(curl_exec($ch));
    curl_close($ch);

    //dd($response->RetornoCpf->DadosTitular);

    $cliente = \App\Models\Cliente::where('cpf', '088.172.824-13')->first();

    if(!$cliente)
    {
        $cliente = new \App\Models\Cliente();
    }

    $cliente->cpf = $response->RetornoCpf->DadosTitular->Cpf;
    $cliente->data_nascimento = $response->RetornoCpf->DadosTitular->DataNascimento;
    $cliente->nome = $response->RetornoCpf->DadosTitular->Titular;
    $cliente->sexo = substr($response->RetornoCpf->DadosTitular->Genero, 0, 1) ;
    $cliente->logradouro = $response->RetornoCpf->EnderecoTitular->Logradouro;
    $cliente->numero = $response->RetornoCpf->EnderecoTitular->Numero;
    $cliente->complemento = $response->RetornoCpf->EnderecoTitular->Complemento;
    $cliente->bairro = $response->RetornoCpf->EnderecoTitular->Bairro;
    $cliente->cidade = $response->RetornoCpf->EnderecoTitular->Cidade;
    $cliente->estado = $response->RetornoCpf->EnderecoTitular->UF;
    $cliente->cep = $response->RetornoCpf->EnderecoTitular->Cep;

    $cliente->save();


    //dd($xx);
//    $url = "https://ws.iwebservice.com.br/CPF/?chave=426I3-062UH-858TN-6CDX1&cpf=088.172.824-13&dataNascimento=30/12/1992&formato=JSON";
//    $url = "http://ws.hubdodesenvolvedor.com.br/v2/cpf/?cpf=08817282413&data=&token=TOKEN";

//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_HEADER, false);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//    curl_setopt($ch,CURLOPT_TIMEOUT,450);
//    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10);
//
//    $result = curl_exec($ch);
//    curl_close($ch);
//
//    Decodifica o JSON
//    $result = @json_decode($result);
//
//    var_dump($result);

});

Route::get('/', 'HomeController@index');
Route::get('/bot', 'HomeController@bot');

//Auth::routes();
//
//Route::group(['middleware' => ['auth', 'CheckUserCliente']], function () {
//
//    Route::get('/', 'HomeController@index');
//    Route::get('/home', 'HomeController@index');
//
//    Route::get('/tabelas-sistema', function(){
//        return view('pages.tabelas-sistema.index');
//    });
//
//    //routes do sistema
//    require_once 'routes/configuracao.php';
//    require_once 'routes/funcao.php';
//    require_once 'routes/permissao.php';
//    require_once 'routes/lote.php';
//    require_once 'routes/entrevista.php';
//    require_once 'routes/usuario.php';
//
//    //tabelas do sistema
//    require_once 'routes/padrao-ramal.php';
//    require_once 'routes/padrao-entrada.php';
//    require_once 'routes/tipo-beneficio.php';
//    require_once 'routes/tipo-estrutura.php';
//    require_once 'routes/aparelho-carga.php';
//    require_once 'routes/motivo-impedimento.php';
//    require_once 'routes/medidor.php';
//    require_once 'routes/comodos.php';
//    require_once 'routes/moradores.php';
//    require_once 'routes/problema.php';
//    require_once 'routes/tipo.php';
//    require_once 'routes/ineficiencia.php';
//    require_once 'routes/renda-familiar-mensal.php';
//    require_once 'routes/situacao-local.php';
//    require_once 'routes/status-padrao-entrada.php';
//    require_once 'routes/status-padrao-ramal.php';
//    require_once 'routes/status-situacao-local.php';
//    require_once 'routes/status-medidor.php';
//    require_once 'routes/cliente.php';
//    require_once 'routes/dados-visita.php';
//
//});
