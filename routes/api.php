<?php

use App\Models\ComponenteInspecao;
use App\Models\ImagemComponente;
use App\Models\Inspecao;
use App\Models\HistoricoInspecao;
use \App\Models\HistoricoComponenteInspecao;
use \App\Models\HistoricoImagemComponente;
use App\Models\Parque;
use App\User;
use App\Models\Turbina;
use App\Models\Componente;
use App\Models\ModeloTurbina;
use App\Models\Escopo;
use App\Models\TipoInspecao;
use App\Models\Sistema;
use App\Models\Complexo;
use App\Models\Cliente;
use App\Models\NivelCriticidade;
use Illuminate\Http\Request;
use App\Enum\StatusInspecaoEnum;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\Auth\LoginController@login');
Route::post('refresh', 'Api\Auth\LoginController@refresh');

Route::middleware('auth:api')->group(function () {

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/users', function (Request $request) {
        return \App\User::all();
    });

    Route::group(['prefix' => 'cliente'], function () {
        Route::get('/{lote}', 'Api\ClienteController@getByLote');
    });

    Route::group(['prefix' => 'entrevista'], function () {
        Route::get('all-entrevistas', 'Api\EntrevistaController@sendAllEntrevistas');
        Route::post('store', 'Api\EntrevistaController@store');
        Route::post('store-imagens/situacao-local', 'Api\EntrevistaController@storeImagensSituacaoLocal');
        Route::post('store-imagens/medidor', 'Api\EntrevistaController@storeImagensMedidor');
        Route::post('store-imagens/transferencia-nome', 'Api\EntrevistaController@storeImagensTransf');
        Route::post('store-imagens/tarifa-social', 'Api\EntrevistaController@storeImagensTarifa');
    });

    Route::get('lotes', 'Api\LoteController@index');

    Route::post('sync', 'Api\SyncController@sync');


});
