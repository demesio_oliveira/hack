<?php

Route::group(['prefix' => 'cliente'], function () {

    Route::get('/', 'ClienteController@index');

    Route::get('/create', 'ClienteController@create');

    Route::post('/store', 'ClienteController@store');

    Route::get('/edit/{cliente}', 'ClienteController@edit');

    Route::post('/destroy', 'ClienteController@destroy');
});
