<?php

Route::group(['prefix' => 'tipo'], function () {

    Route::get('/', 'TipoController@index');

    Route::get('/create', 'TipoController@create');

    Route::post('/store', 'TipoController@store');

    Route::get('/edit/{tipo}', 'TipoController@edit');

    Route::post('/destroy', 'TipoController@destroy');
});
