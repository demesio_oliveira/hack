<?php

Route::group(['prefix' => 'status-situacao-local'], function () {

    Route::get('/', 'StatusSituacaoLocalController@index');

    Route::get('/create', 'StatusSituacaoLocalController@create');

    Route::post('/store', 'StatusSituacaoLocalController@store');

    Route::get('/edit/{statusSituacaoLocal}', 'StatusSituacaoLocalController@edit');

    Route::post('/destroy', 'StatusSituacaoLocalController@destroy');
});
