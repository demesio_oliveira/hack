<?php

Route::group(['prefix' => 'aparelho-carga'], function () {

    Route::get('/', 'AparelhoCargaController@index');

    Route::get('/create', 'AparelhoCargaController@create');

    Route::post('/store', 'AparelhoCargaController@store');

    Route::get('/edit/{aparelhoCarga}', 'AparelhoCargaController@edit');

    Route::post('/destroy', 'AparelhoCargaController@destroy');
});
