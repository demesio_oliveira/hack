<?php

Route::group(['prefix' => 'motivo-impedimento'], function () {

    Route::get('/', 'MotivoImpedimentoController@index');

    Route::get('/create', 'MotivoImpedimentoController@create');

    Route::post('/store', 'MotivoImpedimentoController@store');

    Route::get('/edit/{motivoImpedimento}', 'MotivoImpedimentoController@edit');

    Route::post('/destroy', 'MotivoImpedimentoController@destroy');
});
