<?php

Route::group(['prefix' => 'renda-familiar-mensal'], function () {

    Route::get('/', 'RendaFamiliarMensalController@index');

    Route::get('/create', 'RendaFamiliarMensalController@create');

    Route::post('/store', 'RendaFamiliarMensalController@store');

    Route::get('/edit/{rendaFamiliarMensal}', 'RendaFamiliarMensalController@edit');

    Route::post('/destroy', 'RendaFamiliarMensalController@destroy');
});
