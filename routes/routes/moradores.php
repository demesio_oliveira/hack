<?php

Route::group(['prefix' => 'moradores'], function () {

    Route::get('/', 'MoradoresController@index');

    Route::get('/create', 'MoradoresController@create');

    Route::post('/store', 'MoradoresController@store');

    Route::get('/edit/{moradores}', 'MoradoresController@edit');

    Route::post('/destroy', 'MoradoresController@destroy');
});
