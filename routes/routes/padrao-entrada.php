<?php

Route::group(['prefix' => 'padrao-entrada'], function () {

    Route::get('/', 'PadraoEntradaController@index');

    Route::get('/create', 'PadraoEntradaController@create');

    Route::post('/store', 'PadraoEntradaController@store');

    Route::get('/edit/{padraoEntrada}', 'PadraoEntradaController@edit');

    Route::post('/destroy', 'PadraoEntradaController@destroy');
});
