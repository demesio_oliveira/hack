<?php

Route::group(['prefix' => 'status-padrao-ramal'], function () {

    Route::get('/', 'StatusPadraoRamalController@index');

    Route::get('/create', 'StatusPadraoRamalController@create');

    Route::post('/store', 'StatusPadraoRamalController@store');

    Route::get('/edit/{statusPadraoRamal}', 'StatusPadraoRamalController@edit');

    Route::post('/destroy', 'StatusPadraoRamalController@destroy');
});
