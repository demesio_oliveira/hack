<?php

Route::group(['prefix' => 'lote'], function () {

    Route::get('/', 'LoteController@index');

    Route::get('/importar', 'LoteController@create');

    Route::post('/importar/load', 'LoteController@load');

    Route::post('/importar', 'LoteController@store');



});