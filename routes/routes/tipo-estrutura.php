<?php

Route::group(['prefix' => 'tipo-estrutura'], function () {

    Route::get('/', 'TipoEstruturaController@index');

    Route::get('/create', 'TipoEstruturaController@create');

    Route::post('/store', 'TipoEstruturaController@store');

    Route::get('/edit/{tipoEstrutura}', 'TipoEstruturaController@edit');

    Route::post('/destroy', 'TipoEstruturaController@destroy');
});
