<?php

Route::group(['prefix' => 'comodos'], function () {

    Route::get('/', 'ComodosController@index');

    Route::get('/create', 'ComodosController@create');

    Route::post('/store', 'ComodosController@store');

    Route::get('/edit/{comodos}', 'ComodosController@edit');

    Route::post('/destroy', 'ComodosController@destroy');
});
