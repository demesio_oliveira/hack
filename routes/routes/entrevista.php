<?php

Route::group(['prefix' => 'entrevista'], function () {

    Route::get('/', 'EntrevistaController@index');

    Route::get('/create', 'EntrevistaController@create');

    Route::post('/store', 'EntrevistaController@store');

    Route::post('/destroy', 'EntrevistaController@destroy');

    Route::get('/edit/{entrevista}', 'EntrevistaController@edit');

    Route::get('/dados-cliente/{id}', 'EntrevistaController@dadosCliente');

    Route::get('/load-visitas/{id}', 'EntrevistaController@loadVisitas');

    Route::get('/situacao-local/imagens/{entrevista}/{situacaoLocal}', 'EntrevistaController@situacaoLocalImagens');

    Route::get('/medidor/imagens/{entrevista}/{medidor}', 'EntrevistaController@medidorImagens');

    Route::get('/visita/imagens/{visita}', 'EntrevistaController@visitaImagens');

    Route::get('/transferencia-nome/imagens/{entrevista}', 'EntrevistaController@transfImagens');

    Route::get('/morador/imagens/{entrevista}', 'EntrevistaController@moradorImagens');

    Route::post('/delete-imagem', 'EntrevistaController@deleteImagem');

    Route::get('/export/excel', 'EntrevistaController@exportarExcel');
});
