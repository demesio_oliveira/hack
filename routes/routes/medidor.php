<?php

Route::group(['prefix' => 'medidor'], function () {

    Route::get('/', 'MedidorController@index');

    Route::get('/create', 'MedidorController@create');

    Route::post('/store', 'MedidorController@store');

    Route::get('/edit/{medidor}', 'MedidorController@edit');

    Route::post('/destroy', 'MedidorController@destroy');
});
