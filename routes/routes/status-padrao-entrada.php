<?php

Route::group(['prefix' => 'status-padrao-entrada'], function () {

    Route::get('/', 'StatusPadraoEntradaController@index');

    Route::get('/create', 'StatusPadraoEntradaController@create');

    Route::post('/store', 'StatusPadraoEntradaController@store');

    Route::get('/edit/{statusPadraoEntrada}', 'StatusPadraoEntradaController@edit');

    Route::post('/destroy', 'StatusPadraoEntradaController@destroy');
});
