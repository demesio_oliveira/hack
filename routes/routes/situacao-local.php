<?php

Route::group(['prefix' => 'situacao-local'], function () {

    Route::get('/', 'SituacaoLocalController@index');

    Route::get('/create', 'SituacaoLocalController@create');

    Route::post('/store', 'SituacaoLocalController@store');

    Route::get('/edit/{situacaoLocal}', 'SituacaoLocalController@edit');

    Route::post('/destroy', 'SituacaoLocalController@destroy');
});
