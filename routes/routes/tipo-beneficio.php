<?php

Route::group(['prefix' => 'tipo-beneficio'], function () {

    Route::get('/', 'TipoBeneficioController@index');

    Route::get('/create', 'TipoBeneficioController@create');

    Route::post('/store', 'TipoBeneficioController@store');

    Route::get('/edit/{tipoBeneficio}', 'TipoBeneficioController@edit');

    Route::post('/destroy', 'TipoBeneficioController@destroy');
});
