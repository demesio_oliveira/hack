<?php

Route::group(['prefix' => 'ineficiencia'], function () {

    Route::get('/', 'IneficienciaController@index');

    Route::get('/create', 'IneficienciaController@create');

    Route::post('/store', 'IneficienciaController@store');

    Route::get('/edit/{ineficiencia}', 'IneficienciaController@edit');

    Route::post('/destroy', 'IneficienciaController@destroy');
});
