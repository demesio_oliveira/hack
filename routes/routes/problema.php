<?php

Route::group(['prefix' => 'problema'], function () {

    Route::get('/', 'ProblemaController@index');

    Route::get('/create', 'ProblemaController@create');

    Route::post('/store', 'ProblemaController@store');

    Route::get('/edit/{problema}', 'ProblemaController@edit');

    Route::post('/destroy', 'ProblemaController@destroy');
});
