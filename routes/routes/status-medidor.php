<?php

Route::group(['prefix' => 'status-medidor'], function () {

    Route::get('/', 'StatusMedidorController@index');

    Route::get('/create', 'StatusMedidorController@create');

    Route::post('/store', 'StatusMedidorController@store');

    Route::get('/edit/{statusMedidor}', 'StatusMedidorController@edit');

    Route::post('/destroy', 'StatusMedidorController@destroy');
});
