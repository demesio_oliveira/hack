<?php

Route::group(['prefix' => 'dados-visita'], function () {

    Route::get('/', 'DadosVisita@index');

    Route::get('/create', 'DadosVisitaController@create');

    Route::post('/store', 'DadosVisitaController@store');

    Route::post('/store', 'DadosVisitaController@store');

    Route::get('/edit/{visita}', 'DadosVisitaController@edit');

    Route::post('/destroy', 'DadosVisitaController@destroy');

});
