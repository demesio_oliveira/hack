<?php

Route::group(['prefix' => 'padrao-ramal'], function () {

    Route::get('/', 'PadraoRamalController@index');

    Route::get('/create', 'PadraoRamalController@create');

    Route::post('/store', 'PadraoRamalController@store');

    Route::get('/edit/{padraoRamal}', 'PadraoRamalController@edit');

    Route::post('/destroy', 'PadraoRamalController@destroy');
});
