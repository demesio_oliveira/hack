<?php

use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Schema;

function versao(){
    return '1.0';
}

function arrayToSelect(array $values, $key, $value) {
    if(count($values) > 0)
    {
        $data = array();

        $data[0] = 'Selecione';
        foreach ($values as $row) {
            $data[$row[$key]] = $row[$value];
        }

        return $data;
    }else{
        return [''];
    }

}

function objectToSelect(array $values, $key, $value) {
    $data = array();

    $data[0] = 'Selecione';
    foreach ($values as $row) {
        if ($row->$value != '') {
            $data[$row->$key] = $row->$value;
        }
    }

    return $data;
}

function arrayToValidator($arr) {

    $erros = '<ul>';

    foreach ($arr->toArray() as $erro)
    {
        foreach ($erro as $msg)
        {
            $erros .= '<li>' .$msg. '</li>';
        }
    }

    $erros .= '</ul>';

    return $erros;
}

function queryToArray($arr, $key)
{
    $array = [];
    foreach ($arr as $row)
    {
        $array[] = $row->$key;
    }

    return $array;
}

function paginate($page, $request, $perPage, $dados)
{
    //Remove da url &page=numero da pagina para não ficar repetindo na url
    $paramsUrl = str_replace('&page='.$page, "", $request->fullUrl());
    //Total de registro por pagina
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($dados, $offset, $perPage, true), count($dados), $perPage, $page, ['path' => $paramsUrl] );
}

function exceptThumb($imagens)
{
    $arr = [];
    foreach ($imagens as $imagem)
    {
        if(!stripos($imagem, '-thumb'))
        {
            $arr[] = $imagem;
        }
    }
    return $arr;
}

function saveImage($prefix, $file, $path, $thumb = false)
{
    $validextensions = ["jpeg", "jpg", "png"];
    $extension = $file->getClientOriginalExtension();

    if(in_array( strtolower($extension), $validextensions)){
        $nameHash = hash("md5", uniqid(time()));

        $picture = $prefix.'_' .$nameHash. '.'.$extension;
        $destinationPath = $path;

        if($thumb)
        {
            $pictureThumb = $prefix.'_' .$nameHash. '-thumb.'.$extension;

            Image::make($file)->resize(300, null, function($constraint){
                $constraint->aspectRatio();
            })->save($destinationPath .'/'. $pictureThumb);
        }

        $file->move($destinationPath, $picture);
    }else{
        throw new Exception('Erro extenções permitidas: jpg, jpge e png.');
    }
}

function saveOnlyImage($prefix, $file, $path, $thumb = false)
{
    $validextensions = ["jpeg", "jpg", "png"];
    $extension = $file->getClientOriginalExtension();

    if(in_array( strtolower($extension), $validextensions)){
        $nameHash = hash("md5", uniqid(time()));

        $picture = $prefix.'_' . '.'.'jpg';
        $destinationPath = $path;

        if($thumb)
        {
            $pictureThumb = $prefix.'_' . '-thumb.'.'.jpg';

            Image::make($file)->resize(300, null, function($constraint){
                $constraint->aspectRatio();
            })->save($destinationPath .'/'. $pictureThumb);
        }

        $file->move($destinationPath, $picture);
    }else{
        throw new Exception('Erro extenções permitidas: jpg, jpge e png.');
    }
}

function saveImagesWalterMark($prefix, $file, $path) {
    $validextensions = ["jpeg", "jpg", "png"];
    $extension = $file->getClientOriginalExtension();

    if(in_array( strtolower($extension), $validextensions)){
        $nameHash = hash("md5", uniqid(time()));

        $picture = $prefix.'_' .$nameHash. '.'.$extension;
        $pictureThumb = $prefix.'_' .$nameHash. '-thumb.'.$extension;
        $destinationPath = $path;

        //pegar o tamanho da imagem para colocar a data no final dela
        $sizeImage = getimagesize($file);
        $width = $sizeImage[0] - 60;
        $height = $sizeImage[1] - 20;
        //colocar a data na picture
        date_default_timezone_set('America/Recife');
        $data = date("d/m/Y H:i:s ");

        //criar a imagem
        $image = Image::make($file);

        //colocando a watermark
        $waterMark = \File::glob(public_path().'/storage/imagens/configuracao/logo_*', GLOB_MARK);

        $image->insert(count($waterMark) > 0 ? public_path('/storage/imagens/configuracao/') . basename($waterMark[0]) : public_path() . '/img/logo_newwind.png', 'top-left', 5, 5);

        //colocar a data na imagem
        $image->text($data, $width, $height, function($font) {
            $font->size(10);
            $font->file(public_path().'/fonts/arialblack.ttf');
            $font->color('#d33724');
            $font->align('center');
            $font->valign('top');
        });
        //salvar a imagem
        $image->save($destinationPath .'/'. $pictureThumb);

    }else{
        throw new Exception('Erro extenções permitidas: jpg, jpge e png.');
    }
}

function deleteImage($request)
{
    $srcThumb = $request->input('src');
    $srcThumb = str_replace(url('/'), '', $srcThumb);
    $src = str_replace('-thumb', '', $srcThumb);

    if(!empty($src)){
        $imagens = File::glob(public_path($src), GLOB_MARK);
        File::delete($imagens);

        $imagensThumb = File::glob(public_path($srcThumb), GLOB_MARK);
        File::delete($imagensThumb);
        return response()->json(['success' => true, 'msg' => 'Imagem deletada da pasta com sucesso.']);
    }else{
        return response()->json(['success' => false, 'msg' => 'Não foi possivel deletar a imagem.']);
    }
}

function getImagens($path, $id)
{
    $imagens = File::glob($path .$id. '_*', GLOB_MARK);
    $imagens = exceptThumb($imagens);
    $imagens = str_replace('\\', '/', str_replace(public_path(''), url(''), $imagens)) ;
    return $imagens;
}

function getImagensThumb($path, $id)
{
    $imagens = File::glob($path .$id. '_*-thumb*' , GLOB_MARK);
    return $imagens = str_replace(public_path(), url('/'), $imagens);
}

function getImagensForPdf($path, $id)
{
    $imagens = File::glob($path .$id. '_*', GLOB_MARK);
    $imagens = exceptThumb($imagens);
    //        $imagens = str_replace('\\', '/', str_replace(public_path(''), url(''), $imagens)) ;
    return $imagens;
}

function listTableForeignKeys($table)
{
    $conn = Schema::getConnection()->getDoctrineSchemaManager();

    return array_map(function($key) {
        return $key->getName();
    }, $conn->listTableForeignKeys($table));
}

function xmlToArray($xml, $options = array()) {
    $defaults = array(
        'namespaceSeparator' => ':', //you may want this to be something other than a colon
        'attributePrefix' => '@', //to distinguish between attributes and nodes with the same name
        'alwaysArray' => array(), //array of xml tag names which should always become arrays
        'autoArray' => true, //only create arrays for tags which appear more than once
        'textContent' => '$', //key used for the text content of elements
        'autoText' => true, //skip textContent key if node has no attributes or child nodes
        'keySearch' => false, //optional search and replace on tag and attribute names
        'keyReplace' => false       //replace values for above search values (as passed to str_replace())
    );
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();
    $namespaces[''] = null; //add base (empty) namespace
    //get attributes from all namespaces
    $attributesArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
            //replace characters in attribute name
            if ($options['keySearch'])
                $attributeName = str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
            $attributeKey = $options['attributePrefix']
                . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                . $attributeName;
            $attributesArray[$attributeKey] = (string) $attribute;
        }
    }

    //get child nodes from all namespaces
    $tagsArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->children($namespace) as $childXml) {
            //recurse into child nodes
            $childArray = xmlToArray($childXml, $options);
            list($childTagName, $childProperties) = each($childArray);

            //replace characters in tag name
            if ($options['keySearch'])
                $childTagName = str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
            //add namespace prefix, if any
            if ($prefix)
                $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

            if (!isset($tagsArray[$childTagName])) {
                //only entry with this key
                //test if tags of this type should always be arrays, no matter the element count
                $tagsArray[$childTagName] = in_array($childTagName, $options['alwaysArray']) || !$options['autoArray'] ? array($childProperties) : $childProperties;
            } elseif (
                is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName]) === range(0, count($tagsArray[$childTagName]) - 1)
            ) {
                //key already exists and is integer indexed array
                $tagsArray[$childTagName][] = $childProperties;
            } else {
                //key exists so convert to integer indexed array with previous value in position 0
                $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
            }
        }
    }

    //get text content of node
    $textContentArray = array();
    $plainText = trim((string) $xml);
    if ($plainText !== '')
        $textContentArray[$options['textContent']] = $plainText;

    //stick it all together
    $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '') ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

    //return node as array
    return array(
        $xml->getName() => $propertiesArray
    );
}

/**
 * Realiza a formatação de datas YYYY-MM-DD para DD/MM/AAAA, se $hour == true irá exibir Data e Hora
 *
 * @param string $value
 * @param boolean $hour
 * @return string
 */
function dateToView(string $value, bool $hour = false) : string {

    if(!isset($value)) {
        return '';
    }

    return ($hour) ? date('d/m/Y H:i', strtotime($value)) : date('d/m/Y', strtotime($value));
}

function saveImageBase64($prefix, $file, $path) {
    $nameHash = hash("md5", uniqid(time()));

    $picture = $prefix.'_' .$nameHash. '.jpg';
    $pictureThumb = $prefix.'_' .$nameHash. '-thumb.jpg';
    $destinationPath = $path;

    Image::make($file)->save($destinationPath .'/'. $picture);

    Image::make($file)->resize(300, null, function($constraint){
        $constraint->aspectRatio();
    })->save($destinationPath .'/'. $pictureThumb);
}

function saveUcImageBase64($prefix, $file, $path) {

    $picture = $prefix. '_.jpg';
    $pictureThumb = $prefix. '_-thumb.jpg';
    $destinationPath = $path;

    Image::make($file)->save($destinationPath .'/'. $picture);

    Image::make($file)->resize(300, null, function($constraint){
        $constraint->aspectRatio();
    })->save($destinationPath .'/'. $pictureThumb);
}

function saveNativeImageBase64($uuid, $file, $path)
{
    $picture = $uuid. '.jpg';
    //$pictureThumb = $uuid. '-thumb.jpg';
    $destinationPath = $path;

    Image::make($file)->save($destinationPath .'/'. $picture);

    /*Image::make($file)->resize(300, null, function($constraint){
        $constraint->aspectRatio();
    })->save($destinationPath .'/'. $pictureThumb);*/
}

function dateToSave($data,$hour = null){
    if(!isset($data)){
        return null;
    }
    $formatDate = str_replace("/", ".", $data);
    if($hour){
        return date('Y-m-d H:i', strtotime($formatDate));
    }

    return date('Y-m-d', strtotime($formatDate));
}

function getSigla() {
    $config = \App\Models\Configuracao::select('sigla')->first();

    if($config){
        return $config->sigla;
    }
    return 'DEF';
}

function getSkin(){
    $config = \App\Models\Configuracao::select('skin')->first();

    if($config){
        return $config->skin;
    }
    return 'skin-blue';
}

function getSkinPattern(){
    $config = \App\Models\Configuracao::select('skin')->first();

    if($config){
        return $config->skin."-pattern";
    }
    return 'skin-blue-pattern';
}

function removeMask($texto) {
    return preg_replace('/[\-\|\(\)\/\.\:\_ ]/', '', $texto);
}

function getLogo()
{
    $folderConfig = public_path() . '/storage/imagens/configuracao/';

    $file = \File::glob($folderConfig.'logo_*', GLOB_MARK);

    $result = str_replace('\\', '/', str_replace(public_path(''), url(''), $file));

    return $result;
}

function validateErros($validate)
{
    $messages_erros = array();
    $messages       = $validate->messages();

    foreach($messages as $key => $message)
    {
        $messages_erros["$key"] = $message;
    }

    return $messages_erros;
}


function getDateToMonth($date)
{

    if(!isset($date)){
        return '';
    }

    $mes = date('m', strtotime($date));
    $dia = date('d', strtotime($date));
    $ano = date('Y', strtotime($date));

    return $dia . ' ' . getMonthDate($mes) . ' '. $ano;
}

function getMonthDate($mes){

    switch ($mes){

        case 1: $mes = "Janeiro"; break;
        case 2: $mes = "Fevereiro"; break;
        case 3: $mes = "Março"; break;
        case 4: $mes = "Abril"; break;
        case 5: $mes = "Maio"; break;
        case 6: $mes = "Junho"; break;
        case 7: $mes = "Julho"; break;
        case 8: $mes = "Agosto"; break;
        case 9: $mes = "Setembro"; break;
        case 10: $mes = "Outubro"; break;
        case 11: $mes = "Novembro"; break;
        case 12: $mes = "Dezembro"; break;

    }

    return $mes;
}

function validateCnpj($cnpj) {
    $valido = true;

    if(!ctype_digit($cnpj)){
        return false;
    }

    for ($x = 0; $x < 10; $x++) {
        if ($cnpj == str_repeat($x, 14)) {
            $valido = false;
        }
    }

    if ($valido) {
        if (strlen($cnpj) != 14) {
            $valido = false;
        } else {
            for ($t = 12; $t < 14; $t ++) {
                $d = 0;
                $c = 0;
                for ($m = $t - 7; $m >= 2; $m --, $c ++) {
                    $d += $cnpj {$c} * $m;
                }
                for ($m = 9; $m >= 2; $m --, $c ++) {
                    $d += $cnpj {$c} * $m;
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cnpj {$c} != $d) {
                    $valido = false;
                    break;
                }
            }
        }
    }

    return $valido;
}


function hex2rgba($color, $opacity = false) {

    $default = 'rgb(0,0,0)';

    //Return default if no color provided
    if(empty($color))
        return $default;

    //Sanitize $color if "#" is provided
    if ($color[0] == '#' ) {
        $color = substr( $color, 1 );
    }

    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
        $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
        $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
        return $default;
    }

    //Convert hexadec to rgb
    $rgb =  array_map('hexdec', $hex);

    //Check if opacity is set(rgba or rgb)
    if($opacity){
        if(abs($opacity) > 1)
            $opacity = 1.0;
        $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
    } else {
        $output = 'rgb('.implode(",",$rgb).')';
    }

    //Return rgb(a) color string
    return $output;
}


