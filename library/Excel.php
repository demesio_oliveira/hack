<?php
namespace Library;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;

abstract class Excel{

    public static function load($file)
    {
        try
        {
            // type file
            $fileType = IOFactory::identify($file);
            //create file to read
            $reader = IOFactory::createReader($fileType);
            $reader->setReadDataOnly(true);
            //load file
            $spreadsheet = $reader->load($file);
        }
        catch(Exception $e)
        {
            return ['Error ao ler o arquivo "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage()];
        }
        //return sheet active
        return  $spreadsheet;
    }

    /*
     * TODO função para exportação de arquivo de acordo com a preferencia de cada pessoa
     */
    public static function export($file, $config = [])
    {
        try{
            //style default to title
            $styleTitleArray = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                ],
                'font' => [
                    'bold' => true,
                    'size' => '14'
                ]
            ];

            //style default to columns
            $styleColorArray = [
                'fill' =>
                    [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color'    => ['rgb' => '6699cc'],
                    ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'font' => [
                    'color' => ['rgb' => 'ffffff'],
                    'size' => '11'
                ]
            ];

            //get merge columns title
            $qtdColumns = isset($config['merge']) > 0 ? $config['merge'] : 0;

            //load file to transform xlx
            $spreadsheet =  self::load($file);

            //required configs
            if(count($config) == 0)
            {
                return ["success" => false, "message" => "Configurações são nescessarias para poder gerar o arquivo"];
            }

            //required columns
            if(!isset($config["columns"]))
            {
                return ["success" => false, "message" => "Nescessário ao menos uma coluna."];
            }

            $keyExcel  = array_keys($config["columns"]);
            $columnEnd = end($keyExcel);

            //if title1 exist
            if(isset($config["title1"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("A1", $config["title1"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("A1:"."R2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("A1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //if title2 exist
            if(isset($config["title2"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("S1", $config["title2"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("S1:"."X2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("S1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //if title3 exist
            if(isset($config["title3"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("Y1", $config["title3"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("Y1:"."AD2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("Y1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //if title4 exist
            if(isset($config["title4"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("AE1", $config["title4"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("AE1:"."AJ2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("AE1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //if title5 exist
            if(isset($config["title5"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("AK1", $config["title5"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("AK1:"."AR2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("AK1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //if title6 exist
            if(isset($config["title6"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("AS1", $config["title6"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("AS1:"."BE2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("AS1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //if title7 exist
            if(isset($config["title7"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("BF1", $config["title7"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("BF1:"."BJ2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("BF1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //if title8 exist
            if(isset($config["title8"])){

                $spreadsheet->getActiveSheet(0)->setCellValue("BK1", $config["title8"]);

                $spreadsheet->getActiveSheet(0)->mergeCells("BK1:"."BQ2");

                $style = isset($config['style']['titulo']) ? $config['style']['titulo'] : $styleTitleArray;

                $spreadsheet->getActiveSheet(0)->getStyle("BK1:".$columnEnd.$qtdColumns)->applyFromArray($style);
            }
            //jump line
            $newQtdColumns = $qtdColumns + 1;

            if($qtdColumns == 0 && isset($config["title"]))
            {
                $newQtdColumns = $qtdColumns + 2;
            }

            //insert columns in sheet
            foreach ($config["columns"] as $key => $col)
            {
                $spreadsheet->getActiveSheet(0)->setCellValue($key.$newQtdColumns, $col);
            }

            //style for columns
            $styleColumns = isset($config['style']['colmuns']) ? $config['style']['colmuns'] : $styleColorArray;


            $spreadsheet->getActiveSheet(0)->getStyle("A".$newQtdColumns.":".$columnEnd.$newQtdColumns)->applyFromArray($styleColumns);

            //filter for columns
            if($config["filter"] == true){
                $spreadsheet->getActiveSheet(0)->setAutoFilter("A".$newQtdColumns.":".$columnEnd.$newQtdColumns);
            }

            if(isset($config["height"])){
                $spreadsheet->getActiveSheet()->getDefaultRowDimension()->setRowHeight($config["height"]);
            }

            if(isset($config["width"])){
                foreach ($keyExcel as $colu)
                {
                    $spreadsheet->getActiveSheet(0)->getColumnDimension($colu)->setWidth($config["width"]);
                }
            }

            return ["success" => true, "document" => $spreadsheet];
        }
        catch (\Exception $e)
        {
            return ["success" => true, "message" => "Erro ao gerar arquivo ".$e];
        }

    }

    public static function download($file, $name)
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$name.'.xls"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($file, 'Xls');
        $writer->save('php://output');
    }
}
