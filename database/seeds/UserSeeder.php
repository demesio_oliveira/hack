<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            
             \DB::table('users')->insert([
                'uuid'              => \Ramsey\Uuid\Uuid::uuid4(),
                'name'              => 'Administrador',
                'email'             => 'administrador@engeselt.com.br',
                'password'          => bcrypt('admin#@eng'),
                'password_mobile'   => hash('sha1', 'admin#@eng')
            ]);
            
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'msg' => 'Erro ao cadastrar Usuário.']);
        }
    }
}
