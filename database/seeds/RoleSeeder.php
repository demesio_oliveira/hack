<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->newRole('ADMINISTRADOR', 'Administrador do sistema');
    }

    function newRole($name, $description)
    {
        try{
            if(Schema::hasTable('roles'))
            {
                $roles                 = new \App\Models\Role();
                $roles->name           = $name;
                $roles->description    = $description;
                $roles->save();
            }
        }catch (\Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao cadastrar role.']);
        }
    }
}
