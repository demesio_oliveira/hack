<?php

use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->newRoleUser($this->getUser('Administrador') ,$this->getRole('ADMINISTRADOR'));

    }
    public function newRoleUser($user, $role)
    {
        if(Schema::hasTable('role_user')){
            date_default_timezone_set('America/Recife');

            \DB::table('role_user')->insert([
                'user_id'         => $user->id,
                'role_id'         => $role->id,
                'created_at'        => date('Y-m-d H:i:s')
            ]);
        }
    }

    public function getRole($nameRole)
    {
        if(Schema::hasTable('roles')){
            return \App\Models\Role::select('id')->where('name', '=', $nameRole)->first();
        }
    }

    public function getUser($nameUser)
    {
        if(Schema::hasTable('users')){
            return \App\User::select('id')->where('name', '=', $nameUser)->first();
        }
    }
}
