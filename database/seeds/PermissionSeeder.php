<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->newPermission('ADMIN', 'Administrador');
    }

    function newPermission($name, $description)
    {
        try{

            if(Schema::hasTable('permissions'))
            {
                $permission                 = new \App\Models\Permission();
                $permission->name           = $name;
                $permission->description    = $description;
                $permission->save();
            }

        }catch (\Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao cadastrar permissão.']);
        }
    }
}
