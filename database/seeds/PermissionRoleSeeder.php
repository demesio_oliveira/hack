<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ADMINISTRADOR
        $this->newPermissionRole($this->getPermission('ADMIN'), $this->getRole('ADMINISTRADOR'));

    }

    public function newPermissionRole($permission, $role)
    {
        if(Schema::hasTable('permission_role')){
            date_default_timezone_set('America/Recife');

            \Illuminate\Support\Facades\DB::table('permission_role')->insert([
                'permission_id'   => $permission->id,
                'role_id'         => $role->id,
                'created_at'         => date('Y-m-d H:i:s')

            ]);
        }
    }

    public function getRole($nameRole)
    {
        if(Schema::hasTable('roles')){
            return \App\Models\Role::select('id')->where('name', '=', $nameRole)->first();
        }
    }

    public function getPermission($namePermission)
    {
        if(Schema::hasTable('permissions')){
            return \App\Models\Permission::select('id')->where('name', '=', $namePermission)->first();
        }
    }
}
