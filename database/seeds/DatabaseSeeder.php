<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user       = \App\User::get();
        $permission = \App\Models\Permission::get();
        $roles = \App\Models\Role::get();
        $permissionRoles = \Illuminate\Support\Facades\DB::table('permission_role')->get();
        $RoleUser = \Illuminate\Support\Facades\DB::table('role_user')->get();
        $salve = false;
        $savePermission = false;
        $saveRole = false;

        if(count($user) == 0)
        {
            $this->call(UserSeeder::class);
            $salve = true;
        }

        if(count($roles) == 0)
        {
            $this->call(RoleSeeder::class);
            $saveRole = true;
        }

        if(count($permission) == 0 )
        {
            $this->call(PermissionSeeder::class);
            $savePermission = true;
        }

        if(count($permissionRoles) == 0 && $savePermission && $saveRole)
        {
            $this->call(PermissionRoleSeeder::class);
        }

        if(count($RoleUser) == 0 && $saveRole && $salve)
        {
            $this->call(RoleUserSeeder::class);
        }
    }
}
