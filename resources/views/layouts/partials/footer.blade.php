<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <b>Versão {{ versao()  }}</b></a>
    </div>
    <!-- Default to the left -->
    <strong><i>Copyright &copy; 2018 - </i><a href="#">Equipe Engeselt</a></strong>
</footer>