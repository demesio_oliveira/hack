<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
        </ul>
    </section>
</aside>
