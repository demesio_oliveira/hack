<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo" data-toggle="push-menu" role="button">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>UC</b>CRTL</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Hack</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span class="hidden-xs">Administrador</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <h1 style="color: #FFF"><i class="fa fa-user"></i></h1>
                            <p>
                                Administrador
                                <small>admin@hack.com</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a  href="{{url('/usuario/profile')}}" class="btn btn-default btn-flat">
                                    <i class="glyphicon glyphicon-user"></i> Perfil
                                </a>
                            </div>
                            <div class="pull-right">
                                @if(\Auth::check())
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                        <i class="glyphicon glyphicon-log-out"></i> Sair
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                @else
                                    <a href="{{ url('/login') }}" class="btn btn-default btn-flat">Entrar <i class="glyphicon glyphicon-log-in"></i></a>
                                @endif
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>