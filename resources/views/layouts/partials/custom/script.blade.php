<!-- jQuery 3.2.1 -->
<script type="text/javascript" src="{{ asset('AdminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI -->
<script type="text/javascript" src="{{ asset('AdminLTE/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" src="{{ asset('AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Funções JavaScripts -->
<script type="text/javascript" src="{{ asset('/js/funcoesJavaScript.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('AdminLTE/dist/js/adminlte.min.js') }}"></script>
<!-- Select Two -->
<script type="text/javascript" src="{{ asset('AdminLTE/bower_components/select2/dist/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('AdminLTE/bower_components/select2/dist/js/i18n/pt-br.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('AdminLTE/plugins/bootstrap3-editable-1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('AdminLTE/bower_components/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('AdminLTE/bower_components/moment/locale/pt-br.js') }}"></script>
<!-- Import css and js person -->
<script type="text/javascript" src="{{ asset('AdminLTE/plugins/bootstrap3-editable-1.5.1/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.2/wysihtml5-0.3.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('AdminLTE/plugins/bootstrap3-editable-1.5.1/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.2/bootstrap-wysihtml5-0.0.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('AdminLTE/plugins/bootstrap3-editable-1.5.1/inputs-ext/wysihtml5/wysihtml5.js') }}"></script>
<!-- Pace -->
<script src="{{ asset('AdminLTE/bower_components/PACE/pace.min.js') }}"></script>
<!-- Form Error message  -->
<script src="{{ asset('AdminLTE/plugins/form-error-message/form-error-message.js') }}"></script>
<!-- photoswipe  -->
<script src="{{ asset('plugins/photoswipe/js/photoswipe.js') }}"></script>
<script src="{{ asset('plugins/photoswipe/js/photoswipe-ui-default.min.js') }}"></script>
<script src="{{ asset('plugins/photoswipe/js/photoswipe-ui-default.min.js') }}"></script>
<!-- Preview Upload Images  -->
<script src="{{ asset('js/previewUploadImages.js') }}"></script>
<!-- Jquery Rotate  -->
<script src="{{ asset('plugins/jQueryRotate/jQueryRotate.js') }}"></script>
<!-- SweetAlert Plugin Js -->
<script type="text/javascript" src="{{ asset('AdminLTE/plugins/sweetalert/sweetalert2.min.js') }}"></script>
<!-- bootstrap-notify -->
<script src="{{ asset('AdminLTE/bower_components/bootstrap-notify/bootstrap-notify.min.js') }}"></script>