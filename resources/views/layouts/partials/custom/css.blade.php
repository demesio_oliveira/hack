<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
<!-- Css Person -->
<link rel="stylesheet" href="{{ asset('css/person.css') }}">
<!-- Select Two -->
<link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/select2/dist/css/select2.css') }}">
<!-- EditTable -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/bootstrap3-editable-1.5.1/bootstrap3-editable/css/bootstrap-editable.css') }}">
<!-- Custom Css -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<!-- Pace -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/pace/pace.min.css') }}">
<!-- photoswipe  -->
<link rel="stylesheet" href="{{ asset('plugins/photoswipe/css/photoswipe.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/photoswipe/css/default-skin/default-skin.css') }}">
<!-- SweetAlert Plugin Js -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/sweetalert/sweetalert2.min.css') }}">
<!-- Animate css-->
<link rel="stylesheet" href="{{ asset('plugins/animate/animate.css') }}">