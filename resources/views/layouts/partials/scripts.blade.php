<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script src="{{ asset('AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js') }}"></script>

<!-- PhotoSwipe -->
<script src="{{ url('/') }}/js/photoswipe/photoswipe.js"></script>
<script src="{{ url('/') }}/js/photoswipe/photoswipe-ui-default.min.js"></script>
<script src="{{ asset('/plugins/jQueryRotate/jQueryRotate.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        Pace.restart();
    });
    var urlBase = '{{ url('') }}/';
    var _token = '{{csrf_token()}}';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // para adicionar a classe active para o sideNav
    var url = document.location.toString();
    var urlorigin = window.location.origin;

    if( url === urlorigin + "/"){
        $('ul li a[href="' + urlorigin + '"]').parent().addClass('active');
    }
    else{
        $('ul li a[href="' + url + '"]').parent().addClass('active');
        $('ul a').filter(function () {
            return this.href == url;
        }).parent().addClass('active').parent().parent().addClass('active');
    }

    @if(Session::has('msg'))
        toastr.{{ Session::get('type') }}('{{ Session::get("msg") }}', '', {showDuration: 300, hideDuration: 1000, timeOut: 5000, closeButton: true, progressBar: true})
    @endif

</script>
<script type="text/javascript" src="{{ asset('js/filtro-avancado.js') }}"></script>
