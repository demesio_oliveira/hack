<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/skins/_all-skins.min.css') }}">
    <!-- Alert dialogs -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/sweetalert/sweetalert2.min.css') }}">
    <!-- Styles Css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- jQuery Confirm Style-->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/jQueryConfirm/jquery-confirm.min.css') }}">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Select Two -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/select2/dist/css/select2.css') }}">
    <!-- ColorPicker Bootstrap -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <!-- ColorPicker Bootstrap -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/nestable/nestable.css') }}">
    <!-- ColorPicker Bootstrap -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/pace/pace.min.css') }}">
    <!-- PhotoSwipe -->
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/photoswipe/photoswipe.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/photoswipe/default-skin/default-skin.css">

    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">

    <!-- Script html header -->

    <!-- jQuery 3.2.1 -->
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- jQuery 3.2.1 -->
    <script type="text/javascript" src="{{ asset('js/previewUploadImages.js') }}"></script>
    <!-- jQuery UI -->
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- Funções JavaScripts -->
    <script type="text/javascript" src="{{ asset('/js/funcoesJavaScript.js') }}"></script>
    <!-- Select Two -->
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/select2/dist/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/select2/dist/js/i18n/pt-br.js') }}"></script>
    <!-- SweetAlert Plugin Js -->
    <script type="text/javascript" src="{{ asset('AdminLTE/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <!-- jQuery Confirm -->
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/jQueryConfirm/jquery-confirm.min.js') }}"></script>
    <!-- ColorPicker Bootstrap -->
    <script type="text/javascript" src="{{ asset('AdminLTE/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script type="text/javascript" src="{{ asset('AdminLTE/dist/js/adminlte.min.js') }}"></script>
    <!-- InputMask -->


    <!-- TodoList -->
    <script src="{{ asset('AdminLTE/plugins/todo-list/todo-list.js') }}"></script>
    <!-- Pace -->
    <script src="{{ asset('AdminLTE/bower_components/PACE/pace.min.js') }}"></script>
    <!-- Form Error message  -->
    <script src="{{ asset('AdminLTE/plugins/form-error-message/form-error-message.js') }}"></script>

</head>