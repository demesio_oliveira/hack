<!doctype html>
<html lang="pt-br" class="{{ getSkin() }}-html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
     <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.partials.custom.css')
    @include('layouts.partials.custom.script')

    @section('htmlheader')
        @yield('styles-header')
        @yield('scripts-header')
    @show
</head>
<body>
    <section class="content">
        @yield('content')
    </section>

	@section('scripts')
        <script>
            var urlBase = '{{ url('') }}/';
            var _token = '{{csrf_token()}}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                Pace.restart();
            });
        </script>
        @yield('scripts-footer')
    @show
    @include('layouts.photoswipe-form')
</body>
</html>