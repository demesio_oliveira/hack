<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <style>
        html, body {
            background-color: #fff;
            color: #B0BEC5;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .box-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position {
            position: relative;
        }

        .content-page {
            text-align: center;
        }

        .title {
            font-size: 40px;
            padding: 20px;
        }
        .home i{
            color: #B0BEC5;
            text-decoration:none;
        }
        .home:hover i {
            color: #000;
        }
    </style>
</head>
<body>
    <div class="box-center position full-height">
        <div class="content-page">
            <div class="title">Opa! Você não tem permissão o suficiente para essa página.</div>
            <h3>Enquanto isso, você pode retornar ao home.</h3>
            <h1 class="home"><a href="{{ url('/') }}"> <i class="fa fa-home"></i></a></h1>
        </div>
    </div>
</body>
</html>