@extends('layouts.auth')

@section('content')
    <div class="login">
        <div class="box-login">
           <div class="box-login-header">
               <h2 class="title">Acesso ao sistema</h2>
           </div>
           <div class="box-login-body">
               <form method="POST" action="{{ route('login') }}">
                   {{ csrf_field() }}
                   <div class="row">
                       <div class="col-md-12">
                           <div class="input-group {{$errors->has('email') ? "input-group input-group-error" : ""}}">
                               <div class="input-group-addon"><i class="fa  fa-envelope-o"></i></div>
                               <input placeholder="E-mail" id="email" type="text" class="form-control" name="email" data-toggle="tooltip" data-placement="bottom" title="{{$errors->has('email') ? $errors->first('email') : ""}}" value="{{old('email')}}">
                           </div>
                       </div>
                   </div>
                   <br />
                   <div class="row">
                       <div class="col-md-12">
                           <div class="input-group {{$errors->has('password') ? "input-group input-group-error" : ""}}">
                               <div class="input-group-addon"><i class="fa fa-key"></i></div>
                               <input placeholder="Senha" id="password" type="password" class="form-control" name="password" data-toggle="tooltip" data-placement="bottom" title="{{$errors->has('password') ? $errors->first('password') : ""}}">
                           </div>
                       </div>
                   </div>
                   <br/>
                   <br/>
                   <div class="row">
                       <div class="col-md-12">
                           <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                       </div>
                   </div>
                   <br />
                   <div class="row">
                       <div class="col-md-12 text-center">
                           <a class="btn btn-link" href="{{ route('password.request') }}">Esqueceu sua senha?</a>
                       </div>
                   </div>
               </form>
           </div>
           <div class="box-login-footer"></div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.form-control').tooltip();

            $('.form-control').on('focus', function (e) {
                $(this).removeAttr("data-original-title");
                $(this).tooltip('hide');
                var input = $(this);
                var form_erro   =  $(this).parent().attr('class');
                var classes     = form_erro.split(' ');

                $(classes).each(function (index, element) {
                    if(element === "input-group-error"){
                        $(input).css({
                            'border-color'  : '#d2d6de',
                            'color'         : '#555'
                        });
                        $(input).parent().find('.input-group-addon').css({
                            'border-color'  : '#d2d6de',
                            'color'         : '#555'
                        });
                    }
                });
            });

        });
    </script>
@endsection
