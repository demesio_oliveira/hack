@extends('layouts.default')

@section('styles-header')

@stop
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header page-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="box-title"> <i class="fa fa-home" aria-hidden="true"></i> Hack</h4>
                        </div>
                    </div>

                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-striped table-responsive table-scroll">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Telefone</th>
                                    <th>CPF</th>
                                    <th>Data Nascimento</th>
                                    <th>Sexo</th>
                                    <th>Logradouro</th>
                                    <th>Numero</th>
                                    <th>Bairro</th>
                                    <th>Complemento</th>
                                    <th>Cidade</th>
                                    <th>Estado</th>
                                    <th>CEP</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clientes as $cliente)
                                    <tr>
                                        <td nowrap>{{ $cliente->nome }}</td>
                                        <td nowrap>{{ $cliente->email }}</td>
                                        <td nowrap>{{ $cliente->telefone }}</td>
                                        <td nowrap>{{ $cliente->cpf }}</td>
                                        <td nowrap>{{ $cliente->data_nascimento }}</td>
                                        <td nowrap>{{ $cliente->sexo }}</td>
                                        <td nowrap>{{ $cliente->logradouro }}</td>
                                        <td nowrap>{{ $cliente->numero }}</td>
                                        <td nowrap>{{ $cliente->bairro }}</td>
                                        <td nowrap>{{ $cliente->complemento }}</td>
                                        <td nowrap>{{ $cliente->cidade }}</td>
                                        <td nowrap>{{ $cliente->estado }}</td>
                                        <td nowrap>{{ $cliente->cep }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts-footer')

@stop