@extends('layouts.default')

@section('styles-header')
    <style type="text/css">
        .titulo {
            font-size: 30px !important;
        }
    </style>
@stop

@section('content')
    @if(\Auth::user()->authorizePermissionView(['ALL_FUN', 'CAD_FUN', 'VSL_FUN', 'EDT_FUN', 'EXC_FUN']))
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-header page-header">
                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="box-title titulo"> <i class="fa fa-folder-open" aria-hidden="true"></i> Funções</h4>
                            </div>
                            <div class="col-md-2 col-md-offset-6">
                                @if(\Auth::user()->authorizePermissionView(['ALL_FUN', 'CAD_FUN']))
                                    <button class="btn btn-primary btn-flat btn-block btn-primary-custom" onclick="inserirFuncao();">
                                        <i class="glyphicon glyphicon-plus"></i>Adicionar
                                    </button>
                                    @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-body">

                        <div id='alert' class="alert " style="display: none;"></div>

                        <div class="row mobile">
                            
                            {!! Form::open(['action' => ('FuncaoController@index'), 'id' => 'form', 'method' => 'GET']) !!}
                            <div class="col-md-4 col-md-offset-0">
                                <div class="input-group">
                                    {!! Form::text('filter', null, ['class' => 'form-control form-control-custom', 'placeholder' => 'Filtrar...']) !!}
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-flat btn_icon_filter btn-primary-custom marginleft"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span></button>
                                    </span>
                                    <span class="input-group-btn">
                                        <a href="{{url('funcao')}}" class="btn btn-primary btn-flat btn_icon_filter btn-primary-custom marginleft"><i class="fa fa-eraser"></i></a>
                                    </span>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Descrição</th>
                                                @if(\Auth::user()->authorizePermissionView(['ALL_FUN', 'CAD_FUN', 'EDT_FUN', 'EXC_FUN', 'VSL_FUN']))
                                                    <th style="width: 10%; text-align: center">Ações</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($funcoes as $row)
                                                @if($row->name != 'ADMINISTRADOR')
                                                    <tr>
                                                        <td>{{ $row->name }}</td>
                                                        <td>{{ $row->description }}</td>
                                                        @if(\Auth::user()->authorizePermissionView(['ALL_FUN', 'CAD_FUN', 'EDT_FUN', 'EXC_FUN', 'VSL_FUN']))
                                                            <td style="text-align: center;">
                                                                @if(\Auth::user()->authorizePermissionView(['ALL_FUN', 'CAD_FUN', 'EDT_FUN']))
                                                                    <button class="btn btn-primary btn-flat btn-primary-custom-to-color" onclick="editarFuncao('{{ $row->id }}');" title="Editar" data-toggle="tooltip" style="font-size: 0.8em;">
                                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                                    </button>
                                                                @elseif(\Auth::user()->authorizePermissionView(['ALL_FUN', 'VSL_FUN']))
                                                                    <button class="btn btn-primary btn-flat btn-primary-custom-to-color" onclick="visualizarFuncao('{{ $row->id }}');" title="" data-toggle="tooltip" style="font-size: 0.8em;" data-original-title="Visualizar">
                                                                        <i class="glyphicon glyphicon-search"></i>
                                                                    </button>
                                                                @endif
                                                                @if(\Auth::user()->authorizePermissionView(['ALL_FUN', 'EXC_FUN']))
                                                                    <button class="btn btn-danger btn-flat btn-danger-custom-to-color" title="Excluir" data-toggle="tooltip" onclick="deletarFuncao('{{ $row->id }}', '{{$row->name}}')"  style="font-size: 0.8em;">
                                                                            <i class="glyphicon glyphicon-trash"></i>
                                                                    </button>
                                                                @endif
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-9 skin-pattern">
                                    {!! $funcoes->render(); !!}
                                </div>
                                <div class="col-md-3" style="text-align: right;">
                                    <br/>
                                    Mostrando {!! $funcoes->firstItem() !!} a {!! $funcoes->lastItem() !!}
                                    de {!! $funcoes->total() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @includeIf('layouts.partials.modal', ['idModal' => 'modal-form-funcao', 'idContent' => 'content-modal-funcao'])
    @endif
@stop

@section('scripts-footer')
    <script src=" {{ asset('js/app/funcao.js') }} "></script>
@stop