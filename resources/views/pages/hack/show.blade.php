<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span></button>
    <h4 class="modal-title"> Visualizar Função</h4>
</div>
@if(isset($funcao))
    {!! Form::model($funcao, ['action' => ('FuncaoController@store'), 'id' => 'form-funcao']) !!}
@else
    {!! Form::open(['action' => ('FuncaoController@store'), 'id' => 'form-funcao']) !!}
@endif
<div class="modal-body">
    <div id='alert-modal' class="alert" style="display: none;"></div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id-role' ]) !!}
        {!! Form::hidden('permissoes', null, ['id' => 'permissoes-values']) !!}
        <div class="col-md-4">
            {!! Form::label('name', 'Nome', ['class' => 'required']) !!}
            {!! Form::text('name', $funcao->name, ['class' => 'form-control', 'readonly' => 'true']) !!}
        </div>
        <div class="col-md-8">
            {!! Form::label('description', 'Descrição', ['class' => 'required']) !!}
            {!! Form::text('description', $funcao->description, ['class' => 'form-control', 'readonly' => 'true']) !!}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 table-max-height">
            <br />
            <table class="table table-hover">
                <thead>
                <th>Permissões</th>
                </thead>
                <tbody id="table-body-permissoes"></tbody>
            </table>
        </div>

    </div>
</div>
<div class="modal-footer border">
    <button type="button" class="btn btn-default pull-right btn-flat" id="fechar" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> Fechar </button>
</div>
{!! Form::close() !!}

<script type="text/javascript">
    loadPermissoesOnlyView();
</script>
