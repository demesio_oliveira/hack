@extends('layouts.custom')

@section('styles-header')

@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <iframe width="100%" height="630" allow="microphone;" src="https://console.dialogflow.com/api-client/demo/embedded/8f90a9e4-5afb-4527-8d34-41c90e0c0501"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts-footer')
@stop