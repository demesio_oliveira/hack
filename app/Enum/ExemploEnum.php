<?php

namespace App\Enum;
use Enum;

abstract class
ExamploEnum extends Enum {
    
    protected static $attributs = [

           ['id' => 0, 'descricao' => 'Pendente'],
           ['id' => 1, 'descricao' => 'Aprovada'],
           ['id' => 2, 'descricao' => 'Reprovada'],
           ['id' => 3, 'descricao' => 'Em Campo'],
           ['id' => 4, 'descricao' => 'Disponível para Download'],

    ];   
    
}

