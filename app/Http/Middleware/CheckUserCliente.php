<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserCliente
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( isset(auth()->user()->cliente_uuid)){

            if(!\Request::is('area-cliente/*', 'area-cliente')){
                abort(404);
            }

        }
        else{
            if(\Request::is('area-cliente/*', 'area-cliente')){
                abort(404);
            }
        }

        return $next($request);
    }
}
