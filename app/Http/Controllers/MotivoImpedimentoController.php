<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MotivoImpedimento;

class MotivoImpedimentoController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $motivoImpedimento = MotivoImpedimento::orderBy('descricao');

        if($filter)
        {
            $motivoImpedimento->where("descricao", "ilike", "%$filter%");
        }

        $motivoImpedimento = $motivoImpedimento->paginate(10)->appends('filter', request('filter'));

        return view('pages.motivo-impedimento.index', compact('motivoImpedimento'));
    }

    public function create()
    {
        return view('pages.motivo-impedimento.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $motivoImpedimento = MotivoImpedimento::find($id);

        if (!$motivoImpedimento) {
            $motivoImpedimento = new MotivoImpedimento();
        }

        $motivoImpedimento->fill($request->all());

        $validate = validator($request->all(), $motivoImpedimento->rules(), $motivoImpedimento->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $motivoImpedimento->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Motivo de impedimento salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar motivo de impedimento!']);
        }
    }

    public function edit(MotivoImpedimento $motivoImpedimento)
    {
        return view('pages.motivo-impedimento.form', compact('motivoImpedimento'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('motivo_impedimento')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Motivo de impedimento excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir motivo de impedimento!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de motivos de impedimento em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir motivo de impedimento!']);
            }
        }
    }
}
