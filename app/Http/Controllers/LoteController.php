<?php

namespace App\Http\Controllers;

use App\ExcelFilter\ClienteFilter;
use App\Models\Cliente;
use App\Models\Entrevista;
use App\Models\Lote;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Ramsey\Uuid\Uuid;

class LoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $descricao = $request->input('descricao');
        $dataIni = $request->input('dataIni');
        $dataFim = $request->input('dataFim');

        $lotes = Lote::getLotes(null, $descricao, $dataIni, $dataFim);

        $lotes = $lotes->paginate(10);//->appends('filter', request('filter'));

        return view('pages.lote.index', compact('lotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $spreadsheet  = [];

        return view('pages.lote.importar', compact('spreadsheet'));
    }

    public function load(Request $request)
    {
        $file = $request->file('arquivo');

        if(!$file)
        {
            return response()->json('Arquivo não enviado', 500);
        }

        $columns = Lote::readColumns($file);
        $rows = Lote::readFile($file);

        return view('pages.lote.table-import', compact('rows','columns'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function store(Request $request)
    {
        $file = $request->file('arquivo');

        $lote = new Lote();
        $lote->fill($request->all());

        \DB::transaction(function () use ($lote, $file, $request) {

            try {

                $lote->save();

                $rows = Lote::readFile($file);
                $columns = Lote::readColumns($file);

                foreach ($rows as $key => $row)
                {
                    $cliente = Cliente::firstOrNew(['instalacao' => $row[$request->input('instalacao')]]);

                    $cliente->uuid =                Uuid::uuid4()->toString();
                    $cliente->lote_id =             $lote->id;
                    $cliente->instalacao =          (string)$row[$request->input('instalacao')];
                    $cliente->nome =                $row[$request->input('nome')];
                    $cliente->nr_cliente =          empty($row[$request->input('nr_cliente')]) ? 0 : (int)$row[$request->input('nr_cliente')];
                    $cliente->cpf =                 (string)$row[$request->input('cpf')];
                    $cliente->bairro =              $row[$request->input('bairro')];
                    $cliente->endereco =            $row[$request->input('endereco')];
                    $cliente->cep =                 (int)str_replace('-', '', $row[$request->input('cep')]);
                    $cliente->telefone =            (string)$row[$request->input('cpf')];
                    $cliente->nr_medidor =          (int)$row[$request->input('nr_medidor')];
                    $cliente->sequencia_leitura =   (int)$row[$request->input('cpf')];
                    $cliente->ultima_leitura =      empty($row[10]) ? 0 : (int)$row[$request->input('ultima_leitura')];
                    $cliente->tarifa_social =       (string)$row[$request->input('tarifa_social')] == 'Com Tarifa Social' ? true: false;
                    $cliente->cortado_sistema =     (string)$row[$request->input('cortado_sistema')] == 'Cortado' ? true : false;
                    $cliente->contrato_ativo =      (string)$row[$request->input('contrato_ativo')] == 'Contrato Ativo' ? true : false;
                    $cliente->nota_leitura =        empty($row[$request->input('nota_leitura')]) ? 0 : (int)$row[$request->input('nota_leitura')];
                    $cliente->debito_total =        empty($row[$request->input('debito_total')]) ? 0 : $row[$request->input('nota_leitura')];
                    $cliente->consumo_medio =       empty($row[$request->input('consumo_medio')]) ? 0 : $row[$request->input('consumo_medio')];
                    $cliente->toi =                 $row[$request->input('toi')] == 'Sim' ? true : false;
                    $cliente->user_id =             \Auth::user()->id;
                    $save = $cliente->save();

                    if ($save) {
                        $c = Cliente::where('uuid', $cliente->uuid)->first();

                        $entrevista = new Entrevista();

                        $entrevista->user_id = \Auth::user()->id;
                        $entrevista->cliente_id = $c->id;

                        if($entrevista->id == null){
                            $entrevista->id = Uuid::uuid4();
                        }

                        $save = $entrevista->save();
                    }
                }
                if ($save) {
                    return redirect()->back()->with(['msg' => 'Importação realizada com sucesso!', 'type' => 'success']);
                }

            } catch (\Exception $ex) {
                \Debugbar::info($ex);
                return redirect()->back()->with(['msg' => 'Erro ao realizar importação!', 'type' => 'error']);

            }

        });

        return view('pages.lote.importar', compact('rows'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
