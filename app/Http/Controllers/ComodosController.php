<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comodos;

class ComodosController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $comodos = Comodos::orderBy('descricao');

        if($filter)
        {
            $comodos->where("descricao", "ilike", "%$filter%");
        }

        $comodos = $comodos->paginate(10)->appends('filter', request('filter'));

        return view('pages.comodos.index', compact('comodos'));
    }

    public function create()
    {
        return view('pages.comodos.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $comodos = Comodos::find($id);

        if (!$comodos) {
            $comodos = new Comodos();
        }

        $comodos->fill($request->all());

        $validate = validator($request->all(), $comodos->rules(), $comodos->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $comodos->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Cômodos salvos com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar cômodos!']);
        }
    }

    public function edit(Comodos $comodos)
    {
        return view('pages.comodos.form', compact('comodos'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('comodos')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Cômodos excluídos com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir cômodos!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de cômodos em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir cômodos!']);
            }
        }
    }
}
