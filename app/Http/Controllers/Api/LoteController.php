<?php

namespace App\Http\Controllers\Api;

use App\Models\Cliente;
use App\Models\Entrevista;
use App\Models\Lote;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoteController extends Controller
{
    public function index() {

        $lotes = Lote::leftJoin('cliente AS cli', 'cli.lote_id', '=', 'lote.id')
                     ->leftJoin('entrevista AS ent', 'ent.cliente_id', '=', 'cli.id')
                     ->select('lote.id', 'lote.descricao', 'lote.created_at AS criadoEm',
                         \DB::raw('count(cli.uuid) AS clientes'),
                         \DB::raw("max(cli.updated_at) AS" .\DB::raw('"ultimaAtualizacaoCliente"').""),
                         \DB::raw("max(ent.updated_at) AS" .\DB::raw('"ultimaAtualizacaoEntrevista"').""))
                     ->groupBy('lote.id', 'lote.descricao', 'lote.created_at')
                     ->orderBy('lote.created_at', 'desc')
                     ->get();


        return response()->json($lotes, 200);
    }
}
