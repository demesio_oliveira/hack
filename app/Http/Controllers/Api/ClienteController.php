<?php

namespace App\Http\Controllers\Api;

use App\Models\Cliente;
use App\Models\Entrevista;
use App\Models\Lote;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClienteController extends Controller
{
    public function sendAllClientes($uuid) {
        $clientes =  Cliente::join('users', 'users.id', '=', 'cliente.user_id')
                            ->select('cliente.id',
                                     'cliente.uuid',
                                     'cliente.nome',
                                     'cliente.instalacao',
                                     'cliente.nr_cliente as numero_cliente',
                                     'cliente.cpf',
                                     'cliente.bairro',
                                     'cliente.endereco',
                                     'cliente.cep',
                                     'cliente.telefone',
                                     'cliente.nr_medidor as numero_medidor',
                                     'cliente.sequencia_leitura',
                                     'cliente.ultima_leitura',
                                     'cliente.tarifa_social',
                                     'cliente.cortado_sistema as cortado_sist',
                                     'cliente.contrato_ativo',
                                     'cliente.nota_leitura',
                                     'cliente.debito_total',
                                     'cliente.consumo_medio',
                                     'cliente.toi',
                                     'cliente.user_id as user')
                            ->where('users.uuid', $uuid)
                            ->get();

        foreach ($clientes as $row)
        {
            $row->user = User::select('uuid','name','email', 'password_mobile')->where('id',$row->user)->first();
            $row['entrevista'] = Entrevista::where('cliente_id', $row->id)->get();
        }

        return response()->json($clientes, 200);
    }

    public function getByLote($lote) {

        $clientes =  Cliente::join('users', 'users.id', '=', 'cliente.user_id')
            ->select('cliente.id',
                    'cliente.uuid',
                    'cliente.nome',
                    'cliente.lote_id as lote',
                    'cliente.instalacao',
                    'cliente.nr_cliente as numero_cliente',
                    'cliente.cpf',
                    'cliente.bairro',
                    'cliente.endereco',
                    'cliente.cep',
                    'cliente.telefone',
                    'cliente.nr_medidor as numero_medidor',
                    'cliente.sequencia_leitura',
                    'cliente.ultima_leitura',
                    'cliente.tarifa_social',
                    'cliente.cortado_sistema as cortado_sist',
                    'cliente.contrato_ativo',
                    'cliente.nota_leitura',
                    'cliente.debito_total',
                    'cliente.consumo_medio',
                    'cliente.toi',
                    \DB::raw('extract(epoch from cliente.created_at) AS createdAt'),
                    \DB::raw('extract(epoch from cliente.updated_at) AS updatedAt'),
                    'cliente.user_id as user')
            ->where('cliente.lote_id', $lote)
            ->get();

        foreach ($clientes as $row)
        {
            $row->lote = Lote::find($lote)->toArray();
            $row->user = User::select('uuid','name','email', 'password_mobile')->where('id',$row->user)->first();
            $row['entrevista'] = Entrevista::select('id as uuid',
                                                    'cliente_id AS cadastro_id',
                                                    'padrao_entrada_id',
                                                    'status_padrao_entrada_id AS padrao_entrada_status_id',
                                                    'padrao_ramal_id',
                                                    'status_padrao_ramal_id AS padrao_ramal_status_id',
                                                    //'situacao_local_id',
                                                    'status_situacao_local_id AS situacao_local_status_id',
                                                    //'medidor_id',
                                                    'status_medidor_id AS medidor_status_id',
                                                    //'tipo_estrutura_id',
                                                    //'tipo_estrutura_status_id',
                                                    'comodos_id',
                                                    'moradores_id',
                                                    'tipo_id',
                                                    'renda_familiar_id AS renda_familiar_mensal_id',
                                                    'possui_conta AS conta_bancaria',
                                                    'possui_cartao AS cartao_credito',
                                                    'lampadas',
                                                    'ventiladores',
                                                    'tomadas',
                                                    'observacoes'
                                                )
                                            ->where('cliente_id', $row->id)->first();
        }

        return response()->json($clientes, 200);
    }

}
