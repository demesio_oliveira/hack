<?php

namespace App\Http\Controllers\Api;

use App\Models\AparelhoCargaEntrevista;
use App\Models\Cliente;
use App\Models\Comodos;
use App\Models\DadosVisita;
use App\Models\Entrevista;
use App\Models\EntrevistaIneficiencias;
use App\Models\MedidorEntrevista;
use App\Models\Moradores;
use App\Models\MoradorSemTarifaSocial;
use App\Models\MotivoImpedimento;
use App\Models\PadraoEntrada;
use App\Models\PadraoRamal;
use App\Models\ProblemaEntrevista;
use App\Models\RendaFamiliarMensal;
use App\Models\SituacaoLocalEntrevista;
use App\Models\StatusMedidor;
use App\Models\StatusPadraoEntrada;
use App\Models\StatusPadraoRamal;
use App\Models\StatusSituacaoLocal;
use App\Models\Tipo;
use App\Models\TipoBeneficio;
use App\Models\TipoEstrutura;
use App\Models\TransferenciaNomeAtualizacaoCadastro;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EntrevistaController extends Controller
{
    public function sendAllEntrevistas() {
        $entrevistas = Entrevista::select('id', 'cliente_id as cliente', 'padrao_entrada_id as padrao_entrada',
            'status_padrao_entrada_id as status_padrao_entrada', 'padrao_ramal_id as padrao_ramal',
            'status_padrao_ramal_id as status_padrao_ramal', 'status_situacao_local_id as status_situacao_local',
            'status_medidor_id as status_medidor', 'tipo_estrutura_id as tipo_estrutura', 'comodos_id as comodos',
            'tipo_id as tipo', 'moradores_id as moradores', 'renda_familiar_id as renda_familiar', 'possui_conta',
            'possui_cartao', 'lampadas', 'ventiladores', 'tomadas', 'observacoes', 'user_id as user')->get();

        foreach ($entrevistas as $row)
        {
            $row->cliente = Cliente::select('id', 'lote_id as lote', 'instalacao', 'nome', 'nr_cliente', 'cpf',
                'bairro', 'endereco', 'cep', 'telefone', 'nr_medidor', 'sequencia_leitura', 'ultima_leitura',
                'tarifa_social', 'cortado_sistema', 'contrato_ativo', 'nota_leitura', 'debito_total', 'consumo_medio',
                'toi', 'user_id as user')->where('id',$row->cliente)->first();
            $row->user = User::select('uuid','name','email', 'password_mobile')->where('id',$row->user)->first();
            $row->padrao_entrada = PadraoEntrada::select('id','descricao')->where('id',$row->padrao_entrada)->first();
            $row->status_padrao_entrada = StatusPadraoEntrada::select('id','descricao')
                                                             ->where('id',$row->status_padrao_entrada)->first();
            $row->padrao_ramal = PadraoRamal::select('id','descricao')->where('id',$row->padrao_ramal)->first();
            $row->status_padrao_ramal = StatusPadraoRamal::select('id','descricao')
                                                         ->where('id',$row->status_padrao_ramal)->first();
            $row->status_situacao_local = StatusSituacaoLocal::select('id','descricao')
                                                             ->where('id',$row->status_situacao_local)->first();
            $row->status_medidor = StatusMedidor::select('id','descricao')
                                                ->where('id',$row->status_medidor)->first();
            $row->tipo_estrutura = TipoEstrutura::select('id','descricao')->where('id',$row->tipo_estrutura)->first();
            $row->comodos = Comodos::select('id','descricao')->where('id',$row->comodos)->first();
            $row->tipo = Tipo::select('id','descricao')->where('id',$row->tipo)->first();
            $row->moradores = Moradores::select('id','descricao')->where('id',$row->moradores)->first();
            $row->renda_familiar = RendaFamiliarMensal::select('id','descricao')
                                                      ->where('id',$row->renda_familiar)->first();



            $row['aparelho_carga_entrevista'] = AparelhoCargaEntrevista::join('aparelho_carga',
                                                'aparelho_carga.id', '=', 'aparelho_carga_entrevista.aparelho_carga_id')
                                       ->select('aparelho_carga.id as id',
                                                'aparelho_carga.descricao as descricao')
                                       ->where('aparelho_carga_entrevista.entrevista_id', $row->id)
                                       ->get()
                                       ->toArray();
            $row['entrevista_ineficiencias'] = EntrevistaIneficiencias::join('ineficiencia', 'ineficiencia.id', '=',
                                                                             'entrevista_ineficiencias.ineficiencia_id')
                                                                      ->select('ineficiencia.id as id',
                                                                               'ineficiencia.descricao as descricao')
                                                                      ->where('entrevista_ineficiencias.entrevista_id',
                                                                              $row->id)
                                                                      ->get()
                                                                      ->toArray();
            $row['medidor_entrevista'] = MedidorEntrevista::join('medidor', 'medidor.id', '=',
                                                                 'medidor_entrevista.medidor_id')
                                                          ->select('medidor.id as id', 'medidor.descricao as descricao')
                                                          ->where('medidor_entrevista.entrevista_id', $row->id)
                                                          ->get()
                                                          ->toArray();

            $row['morador_sem_tarifa_social'] = MoradorSemTarifaSocial::select('id', 'numero_beneficio', 'nome', 'cpf',
                                                                               'rg', 'data_nascimento',
                                                                               'tipo_beneficio_id as tipo_beneficio')
                                                                      ->where('morador_sem_tarifa_social.entrevista_id', $row->id)
                                                                      ->get();
            foreach ($row['morador_sem_tarifa_social'] as $moradorSemTarifaSocial){
                $moradorSemTarifaSocial->tipo_beneficio = TipoBeneficio::select('id', 'descricao')
                                                                       ->where('id', $moradorSemTarifaSocial->tipo_beneficio)->first();
            }

            $row['problemas_entrevista'] = ProblemaEntrevista::join('problema', 'problema.id', '=',
                                                                    'problemas_entrevista.problema_id')
                                                             ->select('problema.id as id', 'problema.descricao as descricao')
                                                             ->where('problemas_entrevista.entrevista_id', $row->id)
                                                             ->get()
                                                             ->toArray();

            $row['situacao_local_entrevista'] = SituacaoLocalEntrevista::join('situacao_local', 'situacao_local.id', '=',
                                                                              'situacao_local_entrevista.situacao_local_id')
                                                                       ->select('situacao_local.id as id',
                                                                                'situacao_local.descricao as descricao')
                                                                       ->where('situacao_local_entrevista.entrevista_id',
                                                                               $row->id)
                                                                       ->get()
                                                                       ->toArray();

            $row['transferencia_nome_atualizacao_cadastro'] = TransferenciaNomeAtualizacaoCadastro::select('id', 'nome',
                                                                                                           'cpf', 'rg',
                                                                                                           'telefone',
                                                                                                           'email')
                                                                                                  ->where('entrevista_id',
                                                                                                          $row->id)
                                                                                                  ->get()
                                                                                                  ->toArray();

            $row['dados_visita'] = DadosVisita::select('id', 'data_visita', 'medidor_encontrado', 'medidor_vizinho',
                                                       'medidor_antes', 'medidor_depois', 'impedimento_id as motivo_impedimento')
                ->where('dados_visita.entrevista_id', $row->id)
                ->get();
            foreach ($row['dados_visita'] as $dadosVisita){
                $dadosVisita->motivo_impedimento = MotivoImpedimento::select('id', 'descricao')
                                                                    ->where('id', $dadosVisita->motivo_impedimento)->first();
            }

        }

        return response()->json($entrevistas, 200);
    }

    public function store(Request $request) {

        $cadastro = $request->input('cadastro');

        if(isset($cadastro['uuid']) && !empty($cadastro['uuid']))
        {
            $idEntrevista = $request->input('uuid');

            if($idEntrevista)
            {
                $entrevista = Entrevista::find($idEntrevista);


                if(!$entrevista)
                {
                    $entrevista = new Entrevista();
                    $entrevista->id = $idEntrevista;
                }

                $cliente = Cliente::where('uuid', $cadastro['uuid'])->first();

                if(!$cliente)
                {
                    return response()->json('Cliente não encontrado', 500);
                }


                $entrevista->cliente_id = $cliente->id;
                if($request->input('padraoEntrada') && isset($request->input('padraoEntrada')['id']))
                {
                    $entrevista->padrao_entrada_id =  $request->input('padraoEntrada')['id'];
                }

                if($request->input('padraoEntradaStatus') && isset($request->input('padraoEntradaStatus')['id']))
                {
                    $entrevista->status_padrao_entrada_id =  $request->input('padraoEntradaStatus')['id'];
                }

                if($request->input('padraoRamal') && isset($request->input('padraoRamal')['id']))
                {
                    $entrevista->padrao_ramal_id =  $request->input('padraoRamal')['id'];
                }

                if($request->input('padraoRamalStatus') && isset($request->input('padraoRamalStatus')['id']))
                {
                    $entrevista->status_padrao_ramal_id =  $request->input('padraoRamalStatus')['id'];
                }

                if($request->input('moradores') && isset($request->input('moradores')['id']))
                {
                    $entrevista->moradores_id =  $request->input('moradores')['id'];
                }

                if($request->input('tipo') && isset($request->input('tipo')['id']))
                {
                    $entrevista->tipo_id =  $request->input('tipo')['id'];
                }

                if($request->input('tipoEstrutura') && isset($request->input('tipoEstrutura')['id']))
                {
                    $entrevista->tipo_estrutura_id =  $request->input('tipoEstrutura')['id'];
                }

                if($request->input('rendaFamiliarMensal') && isset($request->input('rendaFamiliarMensal')['id']))
                {
                    $entrevista->renda_familiar_id =  $request->input('rendaFamiliarMensal')['id'];
                }

                if($request->input('comodos') && isset($request->input('comodos')['id']))
                {
                    $entrevista->comodos_id =  $request->input('comodos')['id'];
                }

                $entrevista->possui_conta = $request->input("contaBancaria");
                $entrevista->possui_cartao = $request->input("cartaoCredito");
                $entrevista->lampadas = $request->input("lampada");
                $entrevista->ventiladores = $request->input("ventilador");
                $entrevista->tomadas = $request->input("tomada");
                $entrevista->user_id = \Auth::user()->id;
                $entrevista->observacoes = $request->input("observacao");

                $entrevista->save();

                $visitas = $request->input('visitas');

                if($visitas)
                {
                    foreach ($visitas as $visitaRow)
                    {
                        $idVisita = $visitaRow['uuid'];

                        if($idVisita)
                        {
                            $visita = DadosVisita::find($idVisita);

                            if(!$visita)
                            {
                                $visita = new DadosVisita();
                            }

                            $visita->id = $idVisita;
                            $visita->data_visita = date('Y-m-d');
                            $visita->medidor_encontrado = $visitaRow['medidorEncontrado'];
                            $visita->medidor_vizinho = $visitaRow["medidorVizinho"];
                            $visita->medidor_antes = $visitaRow["medidorAntesEntrevista"];
                            $visita->medidor_depois = $visitaRow["medidorDepoisEntrevista"];
                            $visita->entrevista_id = $idEntrevista;

                            if(isset($visitaRow['motivo_impedimento']))
                            {
                                $impedimento = $visitaRow['motivo_impedimento'];
                                if($impedimento)
                                {
                                    $visita->impedimento_id = $impedimento['id'];
                                }
                            }

                            if(isset($visitaRow['usuario']))
                            {
                                $user = $visitaRow['usuario'];

                                $verificador = User::where('uuid', $user['uuid'])->first();

                                if(!$verificador)
                                {
                                    return response()->json('Verificador não encontrado', 500);
                                }

                                $visita->verificador_id = $verificador->id;

                                $visita->save();
                            }else{
                                return response()->json('Verificador não enviado', 500);
                            }



                        }

                    }

                }

                $situacoesLocal = $request->input('situacoesLocal');
                SituacaoLocalEntrevista::where('entrevista_id', $idEntrevista)->delete();
                foreach ($situacoesLocal as $situacaoLocal)
                {
                    if(isset($situacaoLocal['situacaoLocal']['id']))
                    {
                        $situacaoLocalNew = new SituacaoLocalEntrevista();
                        $situacaoLocalNew->entrevista_id = $idEntrevista;
                        $situacaoLocalNew->situacao_local_id = $situacaoLocal['situacaoLocal']['id'];
                        $situacaoLocalNew->save();
                    }
                }

                $medidores = $request->input('medidores');
                MedidorEntrevista::where('entrevista_id', $idEntrevista)->delete();
                foreach ($medidores as $medidor)
                {
                    if(isset($medidor['medidor']['id']))
                    {
                        $medidorNew = new MedidorEntrevista();
                        $medidorNew->entrevista_id = $idEntrevista;
                        $medidorNew->medidor_id = $medidor['medidor']['id'];
                        $medidorNew->save();
                    }
                }

                $inecifiencias = $request->input('ineficiencias');
                EntrevistaIneficiencias::where('entrevista_id', $idEntrevista)->delete();
                foreach ($inecifiencias as $inecifiencia)
                {
                    if(isset($inecifiencia['ineficiencia']['id']))
                    {
                        $inecifienciaNew = new EntrevistaIneficiencias();
                        $inecifienciaNew->entrevista_id = $idEntrevista;
                        $inecifienciaNew->ineficiencia_id = $inecifiencia['ineficiencia']['id'];
                        $inecifienciaNew->save();
                    }
                }

                $problemas = $request->input('problemas');
                ProblemaEntrevista::where('entrevista_id', $idEntrevista)->delete();
                foreach ($problemas as $problema)
                {
                    if(isset($problema['problema']['id']))
                    {
                        $problemaNew = new ProblemaEntrevista();
                        $problemaNew->entrevista_id = $idEntrevista;
                        $problemaNew->problema_id = $problema['problema']['id'];
                        $problemaNew->save();
                    }
                }

                $cargasInstaladas = $request->input('cargasInstaladas');
                AparelhoCargaEntrevista::where('entrevista_id', $idEntrevista)->delete();
                foreach ($cargasInstaladas as $cargaInstalada)
                {
                    if(isset($cargaInstalada['cargaInstalada']['id']))
                    {
                        $cargaInstaladaNew = new AparelhoCargaEntrevista();
                        $cargaInstaladaNew->entrevista_id = $idEntrevista;
                        $cargaInstaladaNew->aparelho_carga_id = $cargaInstalada['cargaInstalada']['id'];
                        $cargaInstaladaNew->save();
                    }
                }

                return response()->json($entrevista, 201);

            }
        }

        return response()->json('UUID não enviada', 400);
    }

    public function storeImagensSituacaoLocal(Request $request) {

        $entrevistas = $request->entrevista;
        $situacoesLocal = $request->situacao_local;
        $uuid_img = $request->uuid;
        $file = $request->base64;

        foreach ($entrevistas as $entrevista) {
            $uuid = $entrevista;
        }

        foreach ($situacoesLocal as $situacaoLocal) {
            $id = $situacaoLocal;
        }

        $entrevista = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
            ->select('cliente.id as cliente_id',
                     'cliente.nome as nome',
                     'cliente.cpf as cpf',
                     'cliente.instalacao as instalacao',
                     'entrevista.id as id')
            ->where('entrevista.id', $uuid)
            ->first();

        $folderSituacaoLocal = public_path() . '/storage/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/situacao-local/imagens_situacao_local_'.$id;


        if(!\Storage::directories($folderSituacaoLocal))
        {
            \Storage::makeDirectory('public/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/situacao-local/imagens_situacao_local_'.$id);

        }

        try {
            $imageData = base64_decode($file);
            saveUcImageBase64($uuid, $imageData, $folderSituacaoLocal);
            return response()->json(['success' => true], 201);

        } catch (Exception $exc) {
            $imagensSituacaoLocal = File::glob(public_path('storage/entrevista/'.$entrevista->instalacao.'/imagens/situacao-local/imagens_situacao_local_'.$id.'/'.$uuid_img), GLOB_MARK);
            File::delete($imagensSituacaoLocal);
            \DB::rollback();
            return response()->json(['success' => false, 'msg' =>$exc->getMessage()], 500);
        }
    }
    public function storeImagensMedidor(Request $request) {

        $entrevistas = $request->entrevista;
        $medidores = $request->medidor;
        $file = $request->base64;
        $uuid_img = $request->uuid;

        foreach ($entrevistas as $entrevista) {
            $uuid = $entrevista;
        }

        foreach ($medidores as $medidor) {
            $id = $medidor;
        }

        $entrevista = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
            ->select('cliente.id as cliente_id',
                     'cliente.nome as nome',
                     'cliente.cpf as cpf',
                     'cliente.instalacao as instalacao',
                     'entrevista.id as id')
            ->where('entrevista.id', $uuid)
            ->first();

        $folderMedidor = public_path() . '/storage/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/medidor/imagens_medidor_'.$id;


        if(!\Storage::directories($folderMedidor))
        {
            \Storage::makeDirectory('public/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/medidor/imagens_medidor_'.$id);

        }

        try {
            $imageData = base64_decode($file);
            saveUcImageBase64($uuid, $imageData, $folderMedidor);
            return response()->json(['success' => true], 201);

        } catch (Exception $exc) {
            $imagensMedidor = File::glob(public_path('storage/entrevista/'.$entrevista->instalacao.'/imagens/medidor/imagens_medidor_'.$id.'/'.$uuid), GLOB_MARK);
            File::delete($imagensMedidor);
            \DB::rollback();
            return response()->json(['success' => false, 'msg' =>$exc->getMessage()], 500);
        }
    }

    public function storeImagensTransf(Request $request) {

        $entrevistas = $request->entrevista;
        $file = $request->base64;
        $uuid_img = $request->uuid;

        foreach ($entrevistas as $entrevista) {
            $uuid = $entrevista;
        }

        $entrevista = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
            ->select('cliente.id as cliente_id',
                'cliente.nome as nome',
                'cliente.cpf as cpf',
                'cliente.instalacao as instalacao',
                'entrevista.id as id')
            ->where('entrevista.id', $uuid)
            ->first();

        $folderTransf = public_path() . '/storage/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/transferencia-nome';


        if(!\Storage::directories($folderTransf))
        {
            \Storage::makeDirectory('public/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/transferencia-nome');

        }

        try {
            $imageData = base64_decode($file);
            saveUcImageBase64($uuid, $imageData, $folderTransf);
            return response()->json(['success' => true], 201);

        } catch (Exception $exc) {
            $imagemTransf = File::glob(public_path('storage/entrevista/'.$entrevista->instalacao.'/imagens/transferencia-nome/'.$uuid), GLOB_MARK);
            File::delete($imagemTransf);
            \DB::rollback();
            return response()->json(['success' => false, 'msg' =>$exc->getMessage()], 500);
        }
    }

    public function storeImagensTarifa(Request $request) {

        $entrevistas = $request->entrevista;
        $file = $request->base64;
        $uuid_img = $request->uuid;

        foreach ($entrevistas as $entrevista) {
            $uuid = $entrevista;
        }

        $entrevista = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
            ->select('cliente.id as cliente_id',
                'cliente.nome as nome',
                'cliente.cpf as cpf',
                'cliente.instalacao as instalacao',
                'entrevista.id as id')
            ->where('entrevista.id', $uuid)
            ->first();

        $folderTarifa = public_path() . '/storage/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/tarifa-social';


        if(!\Storage::directories($folderTarifa))
        {
            \Storage::makeDirectory('public/entrevista/'.$entrevista->instalacao.'/imagens/entrevista/tarifa-social');

        }

        try {
            $imageData = base64_decode($file);
            saveUcImageBase64($uuid, $imageData, $folderTarifa);
            return response()->json(['success' => true], 201);

        } catch (Exception $exc) {
            $imagemTarifa = File::glob(public_path('storage/entrevista/'.$entrevista->instalacao.'/imagens/tarifa-social/'.$uuid), GLOB_MARK);
            File::delete($imagemTarifa);
            \DB::rollback();
            return response()->json(['success' => false, 'msg' =>$exc->getMessage()], 500);
        }
    }
}


