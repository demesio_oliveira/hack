<?php

namespace App\Http\Controllers\Api;

use App\Models\AparelhoCarga;
use App\Models\Comodos;
use App\Models\Ineficiencia;
use App\Models\Medidor;
use App\Models\Moradores;
use App\Models\MotivoImpedimento;
use App\Models\PadraoEntrada;
use App\Models\PadraoRamal;
use App\Models\Problema;
use App\Models\RendaFamiliarMensal;
use App\Models\SituacaoLocal;
use App\Models\StatusPadraoRamal;
use App\Models\StatusPadraoEntrada;
use App\Models\StatusSituacaoLocal;
use App\Models\Tipo;
use App\Models\TipoBeneficio;
use App\Models\TipoEstrutura;
use App\User;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use stdClass;

class SyncController extends Controller
{
    /*
        public static final String ENTITY_USUARIO = "usuario";
        public static final String ENTITY_ROLE = "roles";
        public static final String ENTITY_ROLE_USUARIO = "role_user";
        public static final String ENTITY_PERMISSION = "permissions";
        public static final String ENTITY_PERMISSION_ROLE = "permission_role";
    */
    public function sync(Request $request)
    {
        $dados = new stdClass;
        $tabelas = $request->all();

        if(!$tabelas)
        {
            return $this->syncAll();
        }

        foreach ($tabelas as $tabela)
        {
            switch ($tabela['tabela'])
            {
                case 'usuario' :
                    $dados->usuario = $this->syncUsuario($tabela['data']);
                    break;
                case 'roles' :
                    $dados->roles = $this->syncRoles($tabela['data']);
                    break;
                case 'role_user' :
                    $dados->role_user = $this->syncRoleUser();
                    break;
                case 'permissions' :
                    $dados->permissions = $this->syncPermission($tabela['data']);
                    break;
                case 'permission_role' :
                    $dados->permission_role = $this->syncPermission($tabela['data']);
                    break;
                case 'aparelho_carga' :
                    $dados->carga_instalada = $this->syncAparelhoCarga($tabela['data']);
                    break;
                case 'comodos' :
                    $dados->comodos = $this->syncComodos($tabela['data']);
                    break;
                case 'ineficiencia' :
                    $dados->ineficiencias = $this->syncInteficiencia($tabela['data']);
                    break;
                case 'medidor' :
                    $dados->medidor = $this->syncMedidor($tabela['data']);
                    break;
                case 'moradores' :
                    $dados->moradores = $this->syncMoradores($tabela['data']);
                    break;
                case 'motivo_impedimento' :
                    $dados->motivo_impedimento = $this->syncMotivoImpedimento($tabela['data']);
                    break;
                case 'padrao_entrada' :
                    $dados->padrao_entrada = $this->syncPadraoEntrada($tabela['data']);
                    break;
                case 'status_padrao_entrada' :
                    $dados->padrao_entrada_status = $this->syncStatusPadraoEntrada($tabela['data']);
                    break;
                case 'padrao_ramal' :
                    $dados->padrao_ramal = $this->syncPadraoRamal($tabela['data']);
                    break;
                case 'status_padrao_ramal' :
                    $dados->padrao_ramal_status = $this->syncPadraoRamal($tabela['data']);
                    break;
                case 'problema' :
                    $dados->problemas = $this->syncProblema($tabela['data']);
                    break;
                case 'renda_familiar_mensal' :
                    $dados->renda_familiar_mensal = $this->syncRendaFamiliarMensal($tabela['data']);
                    break;
                case 'situacao_local' :
                    $dados->situacao_local = $this->syncSituacaoLocal($tabela['data']);
                    break;
                case 'status_situacao_local' :
                    $dados->situacao_local_status = $this->syncStatusSituacaoLocal($tabela['data']);
                    break;
                case 'tipo' :
                    $dados->tipo = $this->syncTipo($tabela['data']);
                    break;
                case 'tipo_beneficio' :
                    $dados->tipo_beneficio = $this->syncTipoBeneficio($tabela['data']);
                    break;
                case 'tipo_estrutura' :
                    $dados->tipo_estrutura = $this->syncTipoEstrutura($tabela['data']);
                    break;
            }
        }

        return response()->json($dados);
    }

    public function syncAll()
    {
        $dados = new stdClass;

        $dados->usuario = $this->syncUsuario(null);
        $dados->roles = $this->syncRoles(null);
        $dados->role_user = $this->syncRoleUser(null);
        $dados->permissions = $this->syncPermission(null);
        $dados->permission_role = $this->syncPermissionRole(null);
        $dados->carga_instalada = $this->syncAparelhoCarga(null);
        $dados->comodos = $this->syncComodos(null);
        $dados->ineficiencias = $this->syncInteficiencia(null);
        $dados->medidor = $this->syncMedidor(null);
        $dados->moradores = $this->syncMoradores(null);
        $dados->motivo_impedimento = $this->syncMotivoImpedimento(null);
        $dados->padrao_entrada = $this->syncPadraoEntrada(null);
        $dados->padrao_entrada_status = $this->syncStatusPadraoEntrada(null);
        $dados->padrao_ramal = $this->syncPadraoRamal(null);
        $dados->padrao_ramal_status = $this->syncStatusPadraoRamal(null);
        $dados->problemas = $this->syncProblema(null);
        $dados->renda_familiar_mensal = $this->syncRendaFamiliarMensal(null);
        $dados->situacao_local = $this->syncSituacaoLocal(null);
        $dados->situacao_local_status = $this->syncStatusSituacaoLocal(null);
        $dados->tipo = $this->syncTipo(null);
        $dados->tipo_beneficio = $this->syncTipoBeneficio(null);
        $dados->tipo_estrutura = $this->syncTipoEstrutura(null);

        return response()->json($dados);
    }

    private function syncUsuario($data)
    {
        $users = User::select('uuid', 'name', 'email' ,'password_mobile');

        if($data)
        {
            $users = $users->where('updated_at', '>=', $data);
        }

        $users = $users->get()->toArray();
        return $users;
    }

    private function syncRoles($data)
    {
        $roles = Role::select('id', 'name', 'description');

        if($data)
        {
            $roles = $roles->where('updated_at', '>=', $data);
        }

        $roles = $roles->get()->toArray();

        return $roles;
    }

    private function syncRoleUser()
    {
        $rolesUser = \DB::table('role_user')->select('id', 'role_id as role', 'user_id as user');

        $rolesUser = $rolesUser->get()->toArray();

        if($rolesUser != null)
        {
            foreach ($rolesUser as $row)
            {
                $row->role =  \DB::table('role_user')->select('role_id as id')->where('role_user.id', $row->id)->first();
                $row->user =  \DB::table('role_user')->select('user_id as id')->where('role_user.id', $row->id)->first();
            }
        }

        return $rolesUser;
    }

    private function syncPermission($data)
    {
        $permission = Permission::select('id', 'name', 'description');

        if($data)
        {
            $permission = $permission->where('updated_at', '>=', $data);
        }

        $permission = $permission->get()->toArray();

        return $permission;
    }

    private function syncPermissionRole($data)
    {
        $permissionRoles = \DB::table('permission_role')->select('id', 'permission_id as permission', 'role_id as role');

        if($data)
        {
            $permissionRoles = $permissionRoles->where('updated_at', '>=', $data);
        }
        $permissionRoles = $permissionRoles->get()->toArray();

        if($permissionRoles != null)
        {

            foreach ($permissionRoles as $row)
            {
                $row->permission =  \DB::table('permission_role')->select('permission_id as id')->where('permission_role.id', $row->id )->first();
                $row->role =  \DB::table('permission_role')->select('role_id as id')->where('permission_role.id', $row->id )->first();
            }
        }
        return $permissionRoles;
    }
    private function syncAparelhoCarga($data)
    {
        $aparelhoCarga = AparelhoCarga::select('id', 'descricao');

        if($data)
        {
            $aparelhoCarga = $aparelhoCarga->where('updated_at', '>=', $data);
        }

        $aparelhoCarga = $aparelhoCarga->get()->toArray();

        return $aparelhoCarga;
    }
    private function syncComodos($data)
    {
        $comodos = Comodos::select('id', 'descricao');

        if($data)
        {
            $comodos = $comodos->where('updated_at', '>=', $data);
        }

        $comodos = $comodos->get()->toArray();

        return $comodos;
    }
    private function syncInteficiencia($data)
    {
        $ineficiencia = Ineficiencia::select('id', 'descricao');

        if($data)
        {
            $ineficiencia = $ineficiencia->where('updated_at', '>=', $data);
        }

        $ineficiencia = $ineficiencia->get()->toArray();

        return $ineficiencia;
    }
    private function syncMedidor($data)
    {
        $medidor = Medidor::select('id', 'descricao');

        if($data)
        {
            $medidor = $medidor->where('updated_at', '>=', $data);
        }

        $medidor = $medidor->get()->toArray();

        return $medidor;
    }
    private function syncMoradores($data)
    {
        $moradores = Moradores::select('id', 'descricao');

        if($data)
        {
            $moradores = $moradores->where('updated_at', '>=', $data);
        }

        $moradores = $moradores->get()->toArray();

        return $moradores;
    }
    private function syncMotivoImpedimento($data)
    {
        $motivoImpedimento = MotivoImpedimento::select('id', 'descricao');

        if($data)
        {
            $motivoImpedimento = $motivoImpedimento->where('updated_at', '>=', $data);
        }

        $motivoImpedimento = $motivoImpedimento->get()->toArray();

        return $motivoImpedimento;
    }
    private function syncPadraoEntrada($data)
    {
        $padraoEntrada = PadraoEntrada::select('id', 'descricao');

        if($data)
        {
            $padraoEntrada = $padraoEntrada->where('updated_at', '>=', $data);
        }

        $padraoEntrada = $padraoEntrada->get()->toArray();

        return $padraoEntrada;
    }
    private function syncStatusPadraoEntrada($data)
    {
        $statusPadraoEntrada = statusPadraoEntrada::select('id', 'descricao');

        if($data)
        {
            $statusPadraoEntrada = $statusPadraoEntrada->where('updated_at', '>=', $data);
        }

        $statusPadraoEntrada = $statusPadraoEntrada->get()->toArray();

        return $statusPadraoEntrada;
    }
    private function syncPadraoRamal($data)
    {
        $padraoRamal = PadraoRamal::select('id', 'descricao');

        if($data)
        {
            $padraoRamal = $padraoRamal->where('updated_at', '>=', $data);
        }

        $padraoRamal = $padraoRamal->get()->toArray();

        return $padraoRamal;
    }
    private function syncStatusPadraoRamal($data)
    {
        $statusPadraoRamal = StatusPadraoRamal::select('id', 'descricao');

        if($data)
        {
            $statusPadraoRamal = $statusPadraoRamal->where('updated_at', '>=', $data);
        }

        $statusPadraoRamal = $statusPadraoRamal->get()->toArray();

        return $statusPadraoRamal;
    }
    private function syncProblema($data)
    {
        $problema = Problema::select('id', 'descricao');

        if($data)
        {
            $problema = $problema->where('updated_at', '>=', $data);
        }

        $problema = $problema->get()->toArray();

        return $problema;
    }
    private function syncRendaFamiliarMensal($data)
    {
        $rendaFamiliarMensal = RendaFamiliarMensal::select('id', 'descricao');

        if($data)
        {
            $rendaFamiliarMensal = $rendaFamiliarMensal->where('updated_at', '>=', $data);
        }

        $rendaFamiliarMensal = $rendaFamiliarMensal->get()->toArray();

        return $rendaFamiliarMensal;
    }
    private function syncSituacaoLocal($data)
    {
        $situacaoLocal = SituacaoLocal::select('id', 'descricao');

        if($data)
        {
            $situacaoLocal = $situacaoLocal->where('updated_at', '>=', $data);
        }

        $situacaoLocal = $situacaoLocal->get()->toArray();

        return $situacaoLocal;
    }
    private function syncStatusSituacaoLocal($data)
    {
        $statusSituacaoLocal = StatusSituacaoLocal::select('id', 'descricao');

        if($data)
        {
            $statusSituacaoLocal = $statusSituacaoLocal->where('updated_at', '>=', $data);
        }

        $statusSituacaoLocal = $statusSituacaoLocal->get()->toArray();

        return $statusSituacaoLocal;
    }
    private function syncTipo($data)
    {
        $tipo = Tipo::select('id', 'descricao');

        if($data)
        {
            $tipo = $tipo->where('updated_at', '>=', $data);
        }

        $tipo = $tipo->get()->toArray();

        return $tipo;
    }
    private function syncTipoBeneficio($data)
    {
        $tipoBeneficio = TipoBeneficio::select('id', 'descricao');

        if($data)
        {
            $tipoBeneficio = $tipoBeneficio->where('updated_at', '>=', $data);
        }

        $tipoBeneficio = $tipoBeneficio->get()->toArray();

        return $tipoBeneficio;
    }
    private function syncTipoEstrutura($data)
    {
        $tipoEstrutura = TipoEstrutura::select('id', 'descricao');

        if($data)
        {
            $tipoEstrutura = $tipoEstrutura->where('updated_at', '>=', $data);
        }

        $tipoEstrutura = $tipoEstrutura->get()->toArray();

        return $tipoEstrutura;
    }

}
