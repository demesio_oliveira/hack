<?php

namespace App\Http\Controllers;

use App\Models\DadosVisita;
use App\Models\Entrevista;
use App\Models\MotivoImpedimento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DadosVisitaController extends Controller
{
    public function create()
    {
        $motivoImpedimento = MotivoImpedimento::all()->toArray();
        return view('pages.entrevista.form-visita', compact('motivoImpedimento'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $visita = DadosVisita::find($id);

            $entrevistavisita = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
                ->select('cliente.id as cliente_id',
                    'cliente.nome as nome',
                    'cliente.cpf as cpf',
                    'cliente.instalacao as instalacao',
                    'entrevista.id as id')
                ->where('entrevista.id', $visita->entrevista_id)
                ->first();

            $delete = \DB::table('dados_visita')->where('id', $id)->delete();

            if ($delete) {
                $imagens = File::glob(public_path('storage/entrevista/'.$entrevistavisita->instalacao.'/imagens/entrevista/visita/'.$id.'_.jpg'), GLOB_MARK);
                File::delete($imagens);
                $imagensThumb = File::glob(public_path('storage/entrevista/'.$entrevistavisita->instalacao.'/imagens/entrevista/visita/'.$id.'_-thumb.jpg'), GLOB_MARK);
                File::delete($imagensThumb);
                return response()->json(['success' => true, 'msg' => 'Visita excluída com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir visita!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de visitas em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir visita!']);
            }
        }
    }
}
