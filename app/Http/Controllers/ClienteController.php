<?php

namespace App\Http\Controllers;

use App\Models\Lote;
use Illuminate\Http\Request;
use App\Models\Cliente;
use Ramsey\Uuid\Uuid;

class ClienteController extends Controller
{
    public function index(Request $request)
    {
        $lotes = Lote::toSelect();
        $clientes = Cliente::filtroAvancado($request);
        $clientes = $clientes->paginate(10)->appends($request->query());

        return view('pages.cliente.index', compact('clientes', 'lotes'));
    }

    public function create()
    {

        $lotes = Lote::toSelect();

        return view('pages.cliente.form', compact('lotes'));
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $cliente = Cliente::find($id);

        $tarifaSocial = $request->input('tarifa_social');
        $cortadoSistema = $request->input('cortado_sistema');
        $contratoAtivo = $request->input('contrato_ativo');
        $toi = $request->input('toi');

        if (!$cliente) {
            $cliente = new Cliente();
        }

        if($tarifaSocial == null) {
            $cliente->tarifa_social = false;
        }
        if($cortadoSistema == null) {
            $cliente->cortado_sistema = false;
        }
        if($contratoAtivo == null) {
            $cliente->contrato_ativo = false;
        }
        if($toi == null) {
            $cliente->toi = false;
        }

        $cliente->fill($request->all());
        $cliente->user_id = \Auth::user()->id;

        if($cliente->uuid == null){
            $cliente->uuid = Uuid::uuid4();
        }

        $validate  = validator($request->all(), $cliente->rules(), $cliente->mensages);
        $error_add = $validate->errors()->messages();

        if($validate->fails() || sizeof($error_add) > 0){
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $save = $cliente->save();

        if($save) {
            return redirect()->back()->with(['msg' => 'Cliente salvo com sucesso!', 'type' => 'success']);
        } else {
            return redirect()->back()->with(['msg' => 'Erro ao salvar cliente!', 'type' => 'error']);
        }
    }

    public function edit(Cliente $cliente)
    {
        $lotes = Lote::toSelect();

        return view('pages.cliente.form', compact('cliente', 'lotes'));
    }

    public function destroy(Request $request)
    {
        try {

            $id = $request->input('id');

            $delete = \DB::table('cliente')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Cliente excluído com sucesso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir cliente!']);
            }

        } catch(\Exception $e) {

            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de clientes em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir cliente!']);
            }

        }
    }
}
