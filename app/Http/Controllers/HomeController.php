<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Complexo;
use App\Models\ComponenteInspecao;
use App\Models\Configuracao;
use App\Models\Estados;
use App\Models\Inspecao;
use App\Models\ModeloTurbina;
use App\Models\Parque;
use App\Models\Turbina;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()

    {

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $configuracao = Configuracao::first();
//        $estados = arrayToSelect(Estados::select('id', 'uf')->get()->toArray(), 'id', 'uf');

        $folderConfig = public_path() . '/storage/imagens/configuracao';
        $clientes = Cliente::all();

        if(!\Storage::directories($folderConfig))
        {
            \Storage::makeDirectory('public/imagens/configuracao');
        }

        $imageBanner = \File::glob(public_path().'/storage/imagens/configuracao/banner_*', GLOB_MARK);
        $imageBanner = ($imageBanner != null) ? 'storage/imagens/configuracao/'.basename($imageBanner[0]) : null;
        $imageLogo   = \File::glob(public_path().'/storage/imagens/configuracao/logo_*', GLOB_MARK);
        $imageLogo   = ($imageLogo != null) ? 'storage/imagens/configuracao/'.basename($imageLogo[0]) : null;

        return view('pages.home.index', compact('clientes', 'estados', 'configuracao', 'imageLogo', 'imageBanner'));
    }

    public function bot(Request $request)
    {
        return view('pages.hack.home');
    }
}
