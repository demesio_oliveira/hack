<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusMedidor;

class StatusMedidorController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $statusMedidor = StatusMedidor::orderBy('descricao');

        if($filter)
        {
            $statusMedidor->where("descricao", "ilike", "%$filter%");
        }

        $statusMedidor = $statusMedidor->paginate(10)->appends('filter', request('filter'));

        return view('pages.status-medidor.index', compact('statusMedidor'));
    }

    public function create()
    {
        return view('pages.status-medidor.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $statusMedidor = StatusMedidor::find($id);

        if (!$statusMedidor) {
            $statusMedidor = new StatusMedidor();
        }

        $statusMedidor->fill($request->all());

        $validate = validator($request->all(), $statusMedidor->rules(), $statusMedidor->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $statusMedidor->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Status do medidor salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar status do medidor!']);
        }
    }

    public function edit(StatusMedidor $statusMedidor)
    {
        return view('pages.status-medidor.form', compact('statusMedidor'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('status_medidor')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Status do medidor excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir status do medidor!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de status de medidor em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir status de medidor!']);
            }
        }
    }
}
