<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PadraoRamal;

class PadraoRamalController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $padraoRamal = PadraoRamal::orderBy('descricao');

        if($filter)
        {
            $padraoRamal->where("descricao", "ilike", "%$filter%");
        }

        $padraoRamal = $padraoRamal->paginate(10)->appends('filter', request('filter'));

        return view('pages.padrao-ramal.index', compact('padraoRamal'));
    }

    public function create()
    {
        return view('pages.padrao-ramal.form');
    }

    public function store(Request $request)
    {
        $id = $request->input("id");

        $padraoRamal = PadraoRamal::find($id);

        if (!$padraoRamal) {
            $padraoRamal = new PadraoRamal();
        }

        $padraoRamal->fill($request->all());

        $validate = validator($request->all(), $padraoRamal->rules(), $padraoRamal->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $padraoRamal->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Padrão de ramal salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar padrão de ramal!']);
        }
    }

    public function edit(PadraoRamal $padraoRamal)
    {
        return view('pages.padrao-ramal.form', compact('padraoRamal'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('padrao_ramal')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Padrão de ramal excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir padrão de ramal!']);
            }
        }catch(\Exception $e){
            if ($e->getCode() == 23503){
                return response()->json(['sucess' => false, 'msg' => 'Não é permitida a exclusão de padrões de ramal em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir padrão de ramal!']);
            }
        }
    }
}
