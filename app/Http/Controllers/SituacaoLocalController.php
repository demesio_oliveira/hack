<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SituacaoLocal;

class SituacaoLocalController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $situacaoLocal = SituacaoLocal::orderBy('descricao');

        if($filter)
        {
            $situacaoLocal->where("descricao", "ilike", "%$filter%");
        }

        $situacaoLocal = $situacaoLocal->paginate(10)->appends('filter', request('filter'));

        return view('pages.situacao-local.index', compact('situacaoLocal'));
    }

    public function create()
    {
        return view('pages.situacao-local.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $situacaoLocal = SituacaoLocal::find($id);

        if (!$situacaoLocal) {
            $situacaoLocal = new SituacaoLocal();
        }

        $situacaoLocal->fill($request->all());

        $validate = validator($request->all(), $situacaoLocal->rules(), $situacaoLocal->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $situacaoLocal->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Situação do local salva com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar situação do local!']);
        }
    }

    public function edit(SituacaoLocal $situacaoLocal)
    {
        return view('pages.situacao-local.form', compact('situacaoLocal'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('situacao_local')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Situação do local excluída com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir situação do local!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de situações de local em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir situação do local!']);
            }
        }
    }
}
