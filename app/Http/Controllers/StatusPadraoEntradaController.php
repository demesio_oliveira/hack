<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusPadraoEntrada;

class StatusPadraoEntradaController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $statusPadraoEntrada = StatusPadraoEntrada::orderBy('descricao');

        if($filter)
        {
            $statusPadraoEntrada->where("descricao", "ilike", "%$filter%");
        }

        $statusPadraoEntrada = $statusPadraoEntrada->paginate(10)->appends('filter', request('filter'));

        return view('pages.status-padrao-entrada.index', compact('statusPadraoEntrada'));
    }

    public function create()
    {
        return view('pages.status-padrao-entrada.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $statusPadraoEntrada = StatusPadraoEntrada::find($id);

        if (!$statusPadraoEntrada) {
            $statusPadraoEntrada = new StatusPadraoEntrada();
        }

        $statusPadraoEntrada->fill($request->all());

        $validate = validator($request->all(), $statusPadraoEntrada->rules(), $statusPadraoEntrada->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $statusPadraoEntrada->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Status do padrão de entrada salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar status do padrão de entrada!']);
        }
    }

    public function edit(StatusPadraoEntrada $statusPadraoEntrada)
    {
        return view('pages.status-padrao-entrada.form', compact('statusPadraoEntrada'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('status_padrao_entrada')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Status do padrão de entrada excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir status do padrão de entrada!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de status de padrão de entrada em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir status de padrão de entrada!']);
            }
        }
    }
}
