<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Permission;
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        \Auth::user()->authorizePermission(['ALL_USU', 'CAD_USU', 'VSL_USU', 'EDT_USU', 'EXC_USU']);

        $filter = $request->input('filter');
        $usuarios = User::orderBy('name');

        if($filter)
        {
            $usuarios->where("name", "ilike", "%$filter%")
                ->orWhere("email", "ilike", "%$filter%");
        }
     

        $usuarios = $usuarios->paginate(10)->appends('filter', request('filter'));

        return view('pages.usuario.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Auth::user()->authorizePermission(['ALL_USU', 'CAD_USU']);

        $funcoes  = arrayToSelect(Role::select('id', 'description')->get()->toArray(), 'id', 'description');

        return view('pages.usuario.form', compact('funcoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Auth::user()->authorizePermission(['ALL_USU', 'CAD_USU', 'EDT_USU']);

        try{

            $result = \DB::transaction(function () use ($request) {

                $id = $request->input('id');
                $funcoes = $request->input('funcoes');

                $password = trim($request->input('password'));
                $repeat_password = $request->input('repeat-password');

                $usuario = User::where('id', $id)->first();

                if (!$usuario){
                    $usuario = new User();
                }

                $usuario->fill($request->all());

                if($usuario->uuid == null){
                    $usuario->uuid = Uuid::uuid4();
                }

                $validate = validator($request->all(), $usuario->rules(), $usuario->mensages);

                if($validate->fails())
                {
                    return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
                }

                $message = null;

                if(empty($id)){

                    if ($password === $repeat_password) {
                        $usuario->password = bcrypt($password);
                        $usuario->password_mobile = hash('sha1', $password);
                    } else {
                        $message['password'] = ['Informe sua senha.'];
                        $message['repeat-password'] = ['A confirmação da senha não corresponde.'];
                    }

                } elseif($password != null) {

                    if ($password === $repeat_password) {
                        $usuario->password = bcrypt($password);
                        $usuario->password_mobile = hash('sha1', $password);
                    } else {
                        $message['password'] = ['A confirmação da senha não corresponde.'];
                        $message['repeat-password'] = ['A confirmação da senha não corresponde.'];
                    }

                }
                if(count($message) > 0){
                    return response()->json(['success' => false, 'msg' => $message, 'validate' => true]);
                }

                $save = $usuario->save();

                if($save) {

                    if($funcoes)
                    {
                        $funcoes = json_decode($funcoes);
                        foreach($funcoes as $funcao)
                        {
                            if($funcao->id != "")
                            {
                                \DB::table('role_user')->where('id', $funcao->id)->delete();
                            }

                            if($funcao->id === 0 && !$funcao->deletar || $funcao->id != "" && !$funcao->deletar) {
                                \DB::table('role_user')->insert(
                                    ['role_id' => $funcao->funcao, 'user_id' => $usuario->id, 'created_at' => date('Y-m-d H:i:s')]
                                );
                            }
                        }
                    }

                    return response()->json(['success' => true, 'msg' => 'Usuário salvo com sucesso.']);

                }else{
                    return response()->json(['success' => false, 'msg' => 'Erro ao salvar Usuário.']);
                }

            });

            return $result;

        }catch(\Exception $exc){

            dd($exc);

            return response()->json(['success' => false, 'msg' => 'Erro ao salvar Usuário.']);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $usuario)
    {
        \Auth::user()->authorizePermission(['ALL_USU', 'CAD_USU', 'EDT_USU']);

        $funcoes = Role::select('id', 'description');

        //Verifica se o usuário tem permissão de adminstrador
        if(\Auth::user()->authorizePermission(['ADMIN'])) {
            $funcoes = arrayToSelect($funcoes->get()->toArray(), 'id', 'description');
        } else {
            $funcoes = arrayToSelect($funcoes->where('name', '<>', 'ADMINISTRADOR')->get()->toArray(), 'id', 'description');
        }

        return view('pages.usuario.form', compact('funcoes', 'usuario'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        \Auth::user()->authorizePermission(['ALL_USU', 'EXC_USU']);

        $id = $request->input('id');

        \DB::table('role_user')->where('user_id', $id)->delete();

        $delete = \DB::table('users')->where('id', $id)->delete();

        if($delete)
        {
            return response()->json(['success' => true, 'msg'=> 'Usuário excluído com sucesso.']);
        } else {
            return response()->json(['success' => false, 'msg' => 'Erro ao excluir Usuário.']);
        }
    }

    public function getFuncoes($user)
    {
        return \DB::table('role_user')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('role_user.id', 'role_user.role_id', 'roles.description')
            ->where('role_user.user_id', $user)
            ->get();
    }

    public function profile(Request $request){
        $usuario = $request->user();
        $funcoes = $this->getFuncoes($usuario->id);

        return view('pages.usuario.profile', compact('usuario','funcoes'));
    }

    public function changeProfile(Request $request){
        $usuario = $request->user();
        $newName = $request->input('nome');
        $newPassword = $request->input('nova-senha');
        $repeatNewPassrword = $request->input('repetir-senha');

        if($newName == $usuario->name && $newPassword == null && $repeatNewPassrword == null){
            return response()->json(['success' => false, 'msg'=> 'Nenhuma informação foi atualizada']);
        }

        if($newName != null && $newName != $usuario->name){
            $usuario->name = $newName;
        }

        if($newPassword != null){
            if($newPassword === $repeatNewPassrword){
                $usuario->password = bcrypt($newPassword);
                $usuario->password_mobile = hash('sha1', $newPassword);
            }else{
                return response()->json(['success' => false, 'msg'=> 'Atenção, as senhas divergem. Tente Novamente']);
            }
        }
        $save = $usuario->save();
        if($save){
            return response()->json(['success' => true, 'msg'=> 'Informações Atualizadas com Sucesso']);
        }else{
            return response()->json(['success' => false, 'msg'=> 'Ocorreu uma falha ao atualizar as informações']);
        }
    }
}
