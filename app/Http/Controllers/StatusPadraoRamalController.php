<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusPadraoRamal;

class StatusPadraoRamalController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $statusPadraoRamal = StatusPadraoRamal::orderBy('descricao');

        if($filter)
        {
            $statusPadraoRamal->where("descricao", "ilike", "%$filter%");
        }

        $statusPadraoRamal = $statusPadraoRamal->paginate(10)->appends('filter', request('filter'));

        return view('pages.status-padrao-ramal.index', compact('statusPadraoRamal'));
    }

    public function create()
    {
        return view('pages.status-padrao-ramal.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $statusPadraoRamal = StatusPadraoRamal::find($id);

        if (!$statusPadraoRamal) {
            $statusPadraoRamal = new StatusPadraoRamal();
        }

        $statusPadraoRamal->fill($request->all());

        $validate = validator($request->all(), $statusPadraoRamal->rules(), $statusPadraoRamal->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $statusPadraoRamal->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Status do padrão de ramal salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar status do padrão de ramal!']);
        }
    }

    public function edit(StatusPadraoRamal $statusPadraoRamal)
    {
        return view('pages.status-padrao-ramal.form', compact('statusPadraoRamal'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('status_padrao_ramal')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Status do padrão de ramal excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir status do padrão de ramal!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de status de padrão de ramal em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir status de padrão de ramal!']);
            }
        }
    }
}
