<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\Role;
use Illuminate\Http\Request;

class FuncaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        \Auth::user()->authorizePermission(['ALL_FUN', 'CAD_FUN', 'VSL_FUN', 'EDT_FUN', 'EXC_FUN']);

        $filter = $request->input('filter');
        $funcoes = Role::orderBy('name');

        if($filter)
        {
            $funcoes->where("name", "ilike", "%$filter%")
                    ->orWhere("description", "ilike", "%$filter%");
        }

        $funcoes = $funcoes->paginate(10)->appends('filter', request('filter'));

        return view('pages.funcao.index', compact('funcoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Auth::user()->authorizePermission(['ALL_FUN', 'CAD_FUN']);

        $permissoes = arrayToSelect(Permission::select('id', 'description')->get()->toArray(), 'id', 'description');

        return view('pages.funcao.form', compact('permissoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Auth::user()->authorizePermission(['ALL_FUN', 'CAD_FUN', 'EDT_FUN']);

        $result = \DB::transaction(function () use ($request){
            try{
                $id = $request->input('id');

                $funcao = Role::find($id);

                if(!$funcao)
                {
                    $funcao = new Role();
                }

                $funcao->fill($request->all());

                $validate = validator($request->all(), $funcao->rules(), $funcao->mensages);

                if($validate->fails())
                {
                    return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
                }

                $permissoes = $request->input('permissoes');

                if($permissoes == null)
                {
                    return response()->json(['success' => false, 'msg' => 'Nenhuma permissão foi adicionada à lista.']);
                }

                $save = $funcao->save();

                if($save)
                {
                    if($permissoes)
                    {
                        $permissoes = json_decode($permissoes);

                        foreach($permissoes as $permissao)
                        {
                            if($permissao->id != "")
                            {
                                \DB::table('permission_role')->where('id', $permissao->id)->delete();
                            }
                            if($permissao->id === 0 && !$permissao->deletar || $permissao->id != "" && !$permissao->deletar) {
                                date_default_timezone_set('America/Recife');
                               \DB::table('permission_role')->insert(
                                   ['permission_id' => $permissao->permissao, 'role_id' => $funcao->id , 'created_at' => date('Y-m-d H:i:s')]
                               );
                            }
                        }
                    }

                    return response()->json(['success' => true, 'msg' => 'Função salva com sucesso!']);
                }
                else{
                    return response()->json(['success' => false, 'msg' => 'Erro ao salvar Função!']);
                }

            }catch(\Exception $exc)
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao salvar Função. '.$exc->getMessage()]);
            }
        });

        return $result;
    }

    public function show(Role $funcao)
    {
        \Auth::user()->authorizePermission(['ALL_FUN', 'VSL_FUN']);

        $permissoes = arrayToSelect(Permission::select('id', 'description')->get()->toArray(), 'id', 'description');

        return view('pages.funcao.show', compact('permissoes', 'funcao'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $funcao)
    {
        \Auth::user()->authorizePermission(['ALL_FUN', 'CAD_FUN', 'EDT_FUN']);

        $permissoes = arrayToSelect(Permission::select('id', 'description')->get()->toArray(), 'id', 'description');

        return view('pages.funcao.form', compact('permissoes', 'funcao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        \Auth::user()->authorizePermission(['ALL_FUN', 'EXC_FUN']);

        try{
            $result = \DB::transaction(function () use ($request) {

                $id = $request->input('id');

                if($id)
                {
                    \DB::table('permission_role')->where('role_id', $id)->delete();
                    \DB::table('role_user')->where('role_id', $id)->delete();
                    \DB::table('roles')->where('id', $id)->delete();

                    return response()->json(['success' => true, 'msg' => 'Função excluída com sucesso.' ]);
                }else{
                    return response()->json(['success' => false, 'msg' => 'Código da função inválido.' ]);
                }
            });

            return $result;
        } catch(\Exception $exc) {
            return response()->json(['success' => false, 'msg' => 'Erro ao excluir função.' ]);
        }
    }

    public function getPermissoes($funcao)
    {
        return \DB::table('permission_role')
            ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->select('permission_role.id', 'permission_role.permission_id', 'permissions.description')
            ->where('permission_role.role_id', $funcao)
            ->get();
    }
}
