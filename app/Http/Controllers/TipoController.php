<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tipo;

class TipoController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $tipo = Tipo::orderBy('descricao');

        if($filter)
        {
            $tipo->where("descricao", "ilike", "%$filter%");
        }

        $tipo = $tipo->paginate(10)->appends('filter', request('filter'));

        return view('pages.tipo.index', compact('tipo'));
    }

    public function create()
    {
        return view('pages.tipo.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $tipo = Tipo::find($id);

        if (!$tipo) {
            $tipo = new Tipo();
        }

        $tipo->fill($request->all());

        $validate = validator($request->all(), $tipo->rules(), $tipo->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $tipo->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Tipo salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar tipo!']);
        }
    }

    public function edit(Tipo $tipo)
    {
        return view('pages.tipo.form', compact('tipo'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('tipo')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Tipo excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir tipo!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de tipos em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir tipo!']);
            }
        }
    }
}
