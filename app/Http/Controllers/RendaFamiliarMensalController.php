<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RendaFamiliarMensal;

class RendaFamiliarMensalController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $rendaFamiliarMensal = RendaFamiliarMensal::orderBy('descricao');

        if($filter)
        {
            $rendaFamiliarMensal->where("descricao", "ilike", "%$filter%");
        }

        $rendaFamiliarMensal = $rendaFamiliarMensal->paginate(10)->appends('filter', request('filter'));

        return view('pages.renda-familiar-mensal.index', compact('rendaFamiliarMensal'));
    }

    public function create()
    {
        return view('pages.renda-familiar-mensal.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $rendaFamiliarMensal = RendaFamiliarMensal::find($id);

        if (!$rendaFamiliarMensal) {
            $rendaFamiliarMensal = new RendaFamiliarMensal();
        }

        $rendaFamiliarMensal->fill($request->all());

        $validate = validator($request->all(), $rendaFamiliarMensal->rules(), $rendaFamiliarMensal->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $rendaFamiliarMensal->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Renda familiar mensal salva com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar renda familiar mensal!']);
        }
    }

    public function edit(RendaFamiliarMensal $rendaFamiliarMensal)
    {
        return view('pages.renda-familiar-mensal.form', compact('rendaFamiliarMensal'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('renda_familiar_mensal')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Renda familiar mensal excluída com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir renda familiar mensal!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de rendas familiares mensais em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir renda familiar mensal!']);
            }
        }
    }
}
