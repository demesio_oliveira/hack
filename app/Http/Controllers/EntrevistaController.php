<?php

namespace App\Http\Controllers;

use App\Models\AparelhoCarga;
use App\Models\AparelhoCargaEntrevista;
use App\Models\Comodos;
use App\Models\DadosVisita;
use App\Models\Entrevista;
use App\Models\MotivoImpedimento;
use App\Models\Visita;
use App\Models\EntrevistaIneficiencias;
use App\Models\Ineficiencia;
use App\Models\Medidor;
use App\Models\Moradores;
use App\Models\MoradorSemTarifaSocial;
use App\Models\PadraoEntrada;
use App\Models\PadraoRamal;
use App\Models\Problema;
use App\Models\RendaFamiliarMensal;
use App\Models\SituacaoLocal;
use App\Models\StatusMedidor;
use App\Models\StatusPadraoEntrada;
use App\Models\StatusPadraoRamal;
use App\Models\StatusSituacaoLocal;
use App\Models\Tipo;
use App\Models\TipoBeneficio;
use App\Models\TipoEstrutura;
use App\Models\Cliente;
use App\Models\MedidorEntrevista;
use App\Models\SituacaoLocalEntrevista;
use App\Models\ProblemaEntrevista;
use App\Models\TransferenciaNomeAtualizacaoCadastro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Library\Excel;
use Ramsey\Uuid\Uuid;
use PhpOffice\PhpSpreadsheet\IOFactory;

class EntrevistaController extends Controller
{
    public function index(Request $request)
    {
        $entrevistas = Entrevista::filtroAvancado($request);
        $entrevistas = $entrevistas->paginate(10)->appends($request->query());
//        $entrevistas = Entrevista::paginate(10);

        return view('pages.entrevista.index', compact('entrevistas'));

    }

    public function create()
    {
        $clientes = arrayToSelect(Cliente::select('id', \DB::raw("CAST(nome || ' | ' || cpf AS VARCHAR) AS descricao"))->get()->toArray(), 'id', 'descricao');
        $visitas = DadosVisita::where('entrevista_id', '<>', null);
        $n_visitas = 0;
        $tiposBeneficio = TipoBeneficio::all()->toArray();
        $padroesEntrada = PadraoEntrada::all();
        $statusPadroesEntrada = StatusPadraoEntrada::all();
        $padroesRamal = PadraoRamal::all();
        $statusPadroesRamal = StatusPadraoRamal::all();
        $situacoesLocal = SituacaoLocal::all();
        $statusSituacoesLocal = StatusSituacaoLocal::all();
        $medidores = Medidor::all();
        $statusMedidores = StatusMedidor::all();
        $tiposEstrutura = TipoEstrutura::all();
        $comodos = Comodos::all();
        $tipos = Tipo::all();
        $moradores = Moradores::all();
        $ineficiencias = Ineficiencia::all();
        $rendasFamiliar = RendaFamiliarMensal::all();
        $problemas = Problema::all();
        $aparelhosCarga = AparelhoCarga::all();


        return view('pages.entrevista.form', compact('clientes', 'padroesEntrada', 'statusPadroesEntrada', 'padroesRamal', 'statusPadroesRamal',
        'situacoesLocal', 'statusSituacoesLocal', 'medidores', 'statusMedidores', 'tiposEstrutura', 'comodos', 'tipos',  'moradores', 'rendasFamiliar',
        'problemas', 'aparelhosCarga', 'ineficiencias', 'tiposBeneficio', 'visitas', 'n_visitas'));


    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $entrevista = Entrevista::find($id);
        $possuiConta = $request->input('possui_conta');
        $possuiCartao = $request->input('possui_cartao');

        if(!$entrevista) {
            $entrevista = new Entrevista();
        }
        $entrevista->fill($request->all());
        $entrevista->user_id = \Auth::user()->id;

        if($entrevista->id == null){
            $entrevista->id = Uuid::uuid4();
        }
        if($possuiConta == null) {
            $entrevista->possui_conta = false;
        }
        if($possuiCartao == null) {
            $entrevista->possui_cartao = false;
        }
        $validate = validator($request->all(), $entrevista->rules(), $entrevista->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }
        $save = $entrevista->save();

        if ($save) {

            $medidores = $request->input('medidores_id') ? $request->input('medidores_id') : [];
            $situacoesLocal = $request->input('situacoes_local_id') ? $request->input('situacoes_local_id') : [];
            $ineficiencias = $request->input('ineficiencias_id') ? $request->input('ineficiencias_id') : [];
            $problemas = $request->input('problemas_id') ? $request->input('problemas_id') : [];
            $aparelhosCarga = $request->input('aparelhos_carga_id') ? $request->input('aparelhos_carga_id') : [];

            $medidorEntrevista = MedidorEntrevista::where('entrevista_id', $entrevista->id);
            $medidorEntrevista->delete();
            foreach ($medidores as $medidor) {
                $medidorEntrevista = new MedidorEntrevista();
                $medidorEntrevista->medidor_id = $medidor;
                $medidorEntrevista->entrevista_id = $entrevista->id;
                $medidorEntrevista->save();

                if ($request->hasFile("imagens_medidor_".$medidor)) {

                    $folderMedidor = public_path() . '/storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/medidor/imagens_medidor_'.$medidor;

                    $files = $request->file('imagens_medidor_'.$medidor);

                    if(!\Storage::directories($folderMedidor))
                    {
                        \Storage::makeDirectory('public/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/medidor/imagens_medidor_'.$medidor);

                    }

                    foreach($files as $file){
                        try {
                            saveOnlyImage($entrevista->id, $file, $folderMedidor, true);

                        } catch (Exception $exc) {
                            $imagensMedidor = File::glob(public_path('storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/medidor/imagens_medidor_'.$medidor.'/'.$entrevista->id. '_*'), GLOB_MARK);
                            File::delete($imagensMedidor);
                            \DB::rollback();
                            return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
                        }
                    }
                }
            }

//            if ($request->hasFile('imagens_medidor')) {
//                $folderMedidor = public_path() . '/storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/medidor';
//
//                $files = $request->file('imagens_medidor');
//
//                if(!\Storage::directories($folderMedidor))
//                {
//                    \Storage::makeDirectory('public/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/medidor');
//                }
//
//                foreach($files as $file){
//
//                    try {
//                        saveImage($entrevista->id, $file, $folderMedidor, true);
//                    } catch (Exception $exc) {
//                        $imagensMedidor = File::glob(public_path('storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/medidor/' .$entrevista->id.'_*'), GLOB_MARK);
//                        File::delete($imagensMedidor);
//                        \DB::rollback();
//                        return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
//                    }
//                }
//            }

            $situacaoLocalEntrevista = SituacaoLocalEntrevista::where('entrevista_id', $entrevista->id);
            $situacaoLocalEntrevista->delete();

            foreach ($situacoesLocal as $situacaoLocal) {
                $situacaoLocalEntrevista = new SituacaoLocalEntrevista();
                $situacaoLocalEntrevista->situacao_local_id = $situacaoLocal;
                $situacaoLocalEntrevista->entrevista_id = $entrevista->id;
                $situacaoLocalEntrevista->save();

                if ($request->hasFile("imagens_situacao_local_".$situacaoLocal)) {

                    $folderSituacaoLocal = public_path() . '/storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/situacao-local/imagens_situacao_local_'.$situacaoLocal;

                    $files = $request->file('imagens_situacao_local_'.$situacaoLocal);

                    if(!\Storage::directories($folderSituacaoLocal))
                    {
                        \Storage::makeDirectory('public/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/situacao-local/imagens_situacao_local_'.$situacaoLocal);

                    }

                    foreach($files as $file){
                        try {
                            saveOnlyImage($entrevista->id, $file, $folderSituacaoLocal, true);

                        } catch (Exception $exc) {
                            $imagensSituacaoLocal = File::glob(public_path('storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/situacao-local/imagens_situacao_local_'.$situacaoLocal.'/'.$entrevista->id. '_*'), GLOB_MARK);
                            File::delete($imagensSituacaoLocal);
                            \DB::rollback();
                            return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
                        }
                    }
                }
            }
//            if ($request->hasFile('imagens_situacao_local')) {
//                $folderSituacaoLocal = public_path() . '/storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/situacao-local';
//
//                $files = $request->file('imagens_situacao_local');
//
//                if(!\Storage::directories($folderSituacaoLocal))
//                {
//                    \Storage::makeDirectory('public/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/situacao-local');
//                }
//
//                foreach($files as $file){
//
//                    try {
//                        saveImage($entrevista->id, $file, $folderSituacaoLocal, true);
//                    } catch (Exception $exc) {
//                        $imagensSituacaoLocal = File::glob(public_path('storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/situacao-local/' .$entrevista->id. '_*'), GLOB_MARK);
//                        File::delete($imagensSituacaoLocal);
//                        \DB::rollback();
//                        return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
//                    }
//                }
//            }

            $ineficienciasEntrevista = EntrevistaIneficiencias::where('entrevista_id', $entrevista->id);
            $ineficienciasEntrevista->delete();
            foreach ($ineficiencias as $ineficiencia) {
                $ineficienciasEntrevista = new EntrevistaIneficiencias();
                $ineficienciasEntrevista->ineficiencia_id = $ineficiencia;
                $ineficienciasEntrevista->entrevista_id = $entrevista->id;
                $ineficienciasEntrevista->save();
            }

            $problemasEntrevista = ProblemaEntrevista::where('entrevista_id', $entrevista->id);
            $problemasEntrevista->delete();
            foreach ($problemas as $problema) {
                $problemasEntrevista = new ProblemaEntrevista();
                $problemasEntrevista->problema_id = $problema;
                $problemasEntrevista->entrevista_id = $entrevista->id;
                $problemasEntrevista->save();
            }

            $aparelhosCargaEntrevista = AparelhoCargaEntrevista::where('entrevista_id', $entrevista->id);
            $aparelhosCargaEntrevista->delete();
            foreach ($aparelhosCarga as $aparelhoCarga) {
                $aparelhosCargaEntrevista = new AparelhoCargaEntrevista();
                $aparelhosCargaEntrevista->aparelho_carga_id = $aparelhoCarga;
                $aparelhosCargaEntrevista->entrevista_id = $entrevista->id;
                $aparelhosCargaEntrevista->save();
            }

            $transferenciaNomeAtualizacaoCadastro = TransferenciaNomeAtualizacaoCadastro::where('entrevista_id', $entrevista->id)->first();

            if (isset($request->nome_transf) ||
                isset($request->cpf_transf) ||
                isset($request->rg_transf) ||
                isset($request->telefone_transf) ||
                isset($request->email_transf)) {
                if (!$transferenciaNomeAtualizacaoCadastro){
                    $transferenciaNomeAtualizacaoCadastro = new TransferenciaNomeAtualizacaoCadastro();
                }
                $transferenciaNomeAtualizacaoCadastro->entrevista_id = $entrevista->id;
                $transferenciaNomeAtualizacaoCadastro->nome = $request->nome_transf;
                $transferenciaNomeAtualizacaoCadastro->cpf = $request->cpf_transf;
                $transferenciaNomeAtualizacaoCadastro->rg = $request->rg_transf;
                $transferenciaNomeAtualizacaoCadastro->telefone = $request->telefone_transf;
                $transferenciaNomeAtualizacaoCadastro->email = $request->email_transf;

                $validate = validator($request->all(), $transferenciaNomeAtualizacaoCadastro->rules(), $transferenciaNomeAtualizacaoCadastro->mensages);

                if($validate->fails())
                {
                    return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
                }
                $transferenciaNomeAtualizacaoCadastro->save();

                if ($request->hasFile('imagem_transf')) {
                $folderTransf = public_path() . '/storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/transferencia-nome';

                $file = $request->file('imagem_transf');

                if(!\Storage::directories($folderTransf))
                {
                    \Storage::makeDirectory('public/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/transferencia-nome');
                }

                try {
                    saveOnlyImage($entrevista->id, $file, $folderTransf, true);
                } catch (Exception $exc) {
                    $imagemTransf = File::glob(public_path('storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/transferencia-nome/' .$entrevista->id. '_*'), GLOB_MARK);
                    File::delete($imagemTransf);
                    \DB::rollback();
                    return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
                }

            }
            }

            $visitas = json_decode($request->input('dados_visita'));

            if($visitas) {;

                foreach ($visitas as $dadosVisita) {
                    $visita = new DadosVisita();

                    $visita->id = Uuid::uuid4();
                    $visita->entrevista_id = $entrevista->id;
                    $visita->data_visita = $dadosVisita->data_visita;
                    $visita->medidor_encontrado = $dadosVisita->medidor_encontrado;
                    $visita->medidor_vizinho = $dadosVisita->medidor_vizinho;
                    $visita->medidor_antes = $dadosVisita->medidor_antes;
                    $visita->medidor_depois = $dadosVisita->medidor_depois;
                    $visita->impedimento_id = $dadosVisita->motivo_impedimento_id;
                    if(isset($dadosVisita->lat_inicio_visita) && $dadosVisita->lat_inicio_visita != ""){
                        $visita->lat_inicio_visita = $dadosVisita->lat_inicio_visita;
                    }
                    if(isset($dadosVisita->long_inicio_visita) && $dadosVisita->long_inicio_visita != "") {
                        $visita->long_inicio_visita = $dadosVisita->long_inicio_visita;
                    }
                    if(isset($dadosVisita->lat_termino_visita) && $dadosVisita->lat_termino_visita != ""){
                        $visita->lat_termino_visita = $dadosVisita->lat_termino_visita;
                    }
                    if(isset($dadosVisita->long_termino_visita) && $dadosVisita->long_termino_visita != ""){
                        $visita->long_termino_visita = $dadosVisita->long_termino_visita;
                    }
                    $visita->save();

                    $entrevistavisita = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
                        ->select('cliente.id as cliente_id',
                            'cliente.nome as nome',
                            'cliente.cpf as cpf',
                            'cliente.instalacao as instalacao',
                            'entrevista.id as id')
                        ->where('entrevista.id', $entrevista->id)
                        ->first();

                    $folderVisita = public_path() . '/storage/entrevista/'.$entrevistavisita->instalacao.'/imagens/entrevista/visita';


                    if(!\Storage::directories($folderVisita))
                    {
                        \Storage::makeDirectory('public/entrevista/'.$entrevistavisita->instalacao.'/imagens/entrevista/visita');

                    }
                    try {
                        if (isset($dadosVisita->imagem_visita) && $dadosVisita->imagem_visita != "") {
                            $imageData = base64_decode($dadosVisita->imagem_visita);
                            saveUcImageBase64($visita->id, $imageData, $folderVisita);
                        }
                    } catch (Exception $exc) {
                        $imagemVisita = File::glob(public_path('storage/entrevista/'.$entrevista->instalacao.'/imagens/visita/'.$visita->id), GLOB_MARK);
                        File::delete($imagemVisita);
                        \DB::rollback();
                        return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
                    }

                }

            }

            $moradorSemTarifaSocial = MoradorSemTarifaSocial::where('entrevista_id', $entrevista->id)->first();

            if (isset($request->cpf_morador) || isset($request->nome_morador) || $request->tipo_beneficio_id != 0 ||
                isset($request->numero_beneficio) || isset($request->rg_morador) || isset($request->data_nascimento)){
                if (!$moradorSemTarifaSocial){
                    $moradorSemTarifaSocial = new MoradorSemTarifaSocial();
                }
                $moradorSemTarifaSocial->entrevista_id = $entrevista->id;
                $moradorSemTarifaSocial->nome = $request->nome_morador;
                $moradorSemTarifaSocial->tipo_beneficio_id = $request->tipo_beneficio_id;
                $moradorSemTarifaSocial->numero_beneficio = $request->numero_beneficio;
                $moradorSemTarifaSocial->cpf = $request->cpf_morador;
                $moradorSemTarifaSocial->rg = $request->rg_morador;
                $moradorSemTarifaSocial->data_nascimento = $request->data_nascimento;

                $validate = validator($request->all(), $moradorSemTarifaSocial->rules(), $moradorSemTarifaSocial->mensages);

                if($validate->fails())
                {
                    return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
                }
                $moradorSemTarifaSocial->save();

                if ($request->hasFile('imagem_morador')) {
                    $folderTarifa = public_path() . '/storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/tarifa-social';

                    $file = $request->file('imagem_morador');

                    if(!\Storage::directories($folderTarifa))
                    {
                        \Storage::makeDirectory('public/entrevista/'.$entrevista->cliente->instalacao.'/imagens/entrevista/tarifa-social');
                    }

                    try {
                        saveOnlyImage($entrevista->id, $file, $folderTarifa, true);
                    } catch (Exception $exc) {
                        $imagemTarifa = File::glob(public_path('storage/entrevista/'.$entrevista->cliente->instalacao.'/imagens/tarifa-social/' .$entrevista->id. '_*'), GLOB_MARK);
                        File::delete($imagemTarifa);
                        \DB::rollback();
                        return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
                    }

                }
            }
        }

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Entrevista salva com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar entrevista!']);
        }
    }

    public function situacaoLocalImagens(Entrevista $entrevista, $situacaoLocal)
    {
        if($entrevista)
        {
            return response()->json($entrevista->getImagensSituacaoLocal($situacaoLocal));
        }else{
            return Response()->json('Nenhum Registro.');
        }
    }

    public function medidorImagens(Entrevista $entrevista, $medidor)
    {
        if($entrevista)
        {
            return response()->json($entrevista->getImagensMedidor($medidor));
        }else{
            return Response()->json('Nenhum Registro.');
        }
    }

    public function visitaImagens(Entrevista $entrevista, $visita)
    {
        if($entrevista)
        {
            return response()->json($entrevista->getImagensVisita($visita));
        }else{
            return Response()->json('Nenhum Registro.');
        }
    }

    public function transfImagens(Entrevista $entrevista)
    {
        if($entrevista)
        {
            return response()->json($entrevista->getImagemTransf());
        }else{
            return Response()->json('Nenhum Registro.');
        }
    }

    public function moradorImagens(Entrevista $entrevista)
    {
        if($entrevista)
        {
            return response()->json($entrevista->getImagemMorador());
        }else{
            return Response()->json('Nenhum Registro.');
        }
    }

    public function edit(Entrevista $entrevista)
    {
        $clientes = arrayToSelect(Cliente::select('id', \DB::raw("CAST(nome || ' | ' || cpf AS VARCHAR) AS descricao"))
                                         ->get()
                                         ->toArray(), 'id', 'descricao');
        $tiposBeneficio = TipoBeneficio::all()->toArray();
        $padroesEntrada = PadraoEntrada::all();
        $statusPadroesEntrada = StatusPadraoEntrada::all();
        $padroesRamal = PadraoRamal::all();
        $statusPadroesRamal = StatusPadraoRamal::all();
        $situacoesLocal = SituacaoLocal::all();
        $statusSituacoesLocal = StatusSituacaoLocal::all();
        $medidores = Medidor::all();
        $statusMedidores = StatusMedidor::all();
        $tiposEstrutura = TipoEstrutura::all();
        $comodos = Comodos::all();
        $tipos = Tipo::all();
        $moradores = Moradores::all();
        $ineficiencias = Ineficiencia::all();
        $rendasFamiliar = RendaFamiliarMensal::all();
        $problemas = Problema::all();
        $aparelhosCarga = AparelhoCarga::all();

        $medidoresEntrevista = MedidorEntrevista::select('medidor_id')->where('entrevista_id', $entrevista->id)->get()->toArray();
        $situacoesLocalEntrevista = SituacaoLocalEntrevista::select('situacao_local_id')->where('entrevista_id', $entrevista->id)->get()->toArray();
        $ineficienciasEntrevista = EntrevistaIneficiencias::select('ineficiencia_id')->where('entrevista_id', $entrevista->id)->get()->toArray();
        $problemasEntrevista = ProblemaEntrevista::select('problema_id')->where('entrevista_id', $entrevista->id)->get()->toArray();
        $aparelhosCargaEntrevista = AparelhoCargaEntrevista::select('aparelho_carga_id')->where('entrevista_id', $entrevista->id)->get()->toArray();
        $transferenciaNomeAtualizacaoCadastro = TransferenciaNomeAtualizacaoCadastro::where('entrevista_id', $entrevista->id)->first();
        $moradorSemTarifaSocial = MoradorSemTarifaSocial::where('entrevista_id', $entrevista->id)->first();

        $visitas = DadosVisita::leftJoin('motivo_impedimento as mi', 'mi.id', '=', 'dados_visita.impedimento_id')
                              ->where('entrevista_id', $entrevista->id)
                              ->select("dados_visita.id as visita_id", "dados_visita.entrevista_id", "dados_visita.verificador_id",
                                       "dados_visita.impedimento_id", "dados_visita.data_visita", "dados_visita.medidor_encontrado",
                                       "dados_visita.medidor_vizinho", "dados_visita.medidor_antes", "dados_visita.medidor_depois",
                                       "dados_visita.created_at", "dados_visita.lat_inicio_visita", "dados_visita.long_inicio_visita",
                                       "dados_visita.lat_termino_visita", "dados_visita.long_termino_visita","dados_visita.updated_at",
                                       "mi.descricao as motivo_descricao" )
                              ->get();
        $n_visitas = count($visitas);
        return view('pages.entrevista.form', compact('entrevista', 'clientes', 'padroesEntrada', 'statusPadroesEntrada',
                          'padroesRamal', 'statusPadroesRamal', 'situacoesLocal', 'statusSituacoesLocal', 'medidores', 'statusMedidores', 'tiposEstrutura',
                          'comodos', 'tipos',  'moradores', 'rendasFamiliar', 'problemas', 'aparelhosCarga', 'ineficiencias', 'medidoresEntrevista',
                          'situacoesLocalEntrevista', 'ineficienciasEntrevista', 'problemasEntrevista', 'aparelhosCargaEntrevista', 'tiposBeneficio',
                          'transferenciaNomeAtualizacaoCadastro', 'moradorSemTarifaSocial', 'visitas', 'n_visitas'));
    }

    public function dadosCliente($id)
    {
        $cliente = Cliente::find($id);
        return response()->json($cliente);
    }

    public function loadVisitas($id)
    {
        $data = DadosVisita::leftJoin('motivo_impedimento as mi', 'mi.id', '=', 'dados_visita.impedimento_id')
                           ->where('entrevista_id', $id)
                           ->select('dados_visita.*', 'mi.descricao as impedimento_descricao')
                           ->get();

        return response()->json($data);
    }

    public function deleteImagem(Request $request){
        return deleteImage($request);
    }

    public function exportarExcel(Request $request)
    {
        $modelo = \File::glob(public_path() . '/modelos/modelo.xlsx', GLOB_MARK)[0];
        $entrevista = Entrevista::filtroAvancado($request)->orderByRaw("cliente.nome ASC")->get();

        /*
         * Configurações para poder exportar o xlsx
         */

        $configs = [
            "title1"     => "DADOS CADASTRADOS",
            "title2"     => "DADOS DA VISITA 1",
            "title3"     => "DADOS DA VISITA 2",
            "title4"     => "DADOS DA VISITA 3",
            "title5"     => "PADRÃO DE ENTRADA E MEDIDOR",
            "title6"     => "ATUALIZAÇÃO DE CADASTRO",
            "title7"     => "TRANSFERÊNCIA DE NOME / ATUALIZAÇÃO DE CADASTRO",
            "title8"     => "MORADOR SEM TARIFA SOCIAL",
            "merge"     => 2,
            "columns"   => [
//                Dados cadastrados
                "A" => "NOME",
                "B" => "CPF",
                "C" => "INSTALACAO",
                "D" => "NR",
                "E" => "TELEFONE",
                "F" => "ENDERECO",
                "G" => "BAIRRO",
                "H" => "CEP",
                "I" => "NR MEDIDOR",
                "J" => "SEQUÊNCIA DE LEITURA",
                "K" => "ÚLTIMA LEITURA",
//                Status da instalação
                "L" => "NOTA DE LEITURA",
                "M" => "DÉBITO TOTAL",
                "N" => "CONSUMO MÉDIO",
                "O" => "TARIFA SOCIAL",
                "P" => "CORTADO DO SISTEMA",
                "Q" => "CONTRATO ATIVO",
                "R" => "TOI",

//                Dados da visita 1
                "S" => "DATA",
                "T" => "MEDIDOR ENCONTRADO",
                "U" => "MEDIDOR VIZINHO",
                "V" => "MEDIDOR ANTES",
                "W" => "MEDIDOR DEPOIS",
                "X" => "MOTIVO DE IMPEDIMENTO",

//                Dados da visita 2
                "Y" => "DATA",
                "Z" => "MEDIDOR ENCONTRADO",
                "AA" => "MEDIDOR VIZINHO",
                "AB" => "MEDIDOR ANTES",
                "AC" => "MEDIDOR DEPOIS",
                "AD" => "MOTIVO DE IMPEDIMENTO",

//                Dados da visita 3
                "AE" => "DATA",
                "AF" => "MEDIDOR ENCONTRADO",
                "AG" => "MEDIDOR VIZINHO",
                "AH" => "MEDIDOR ANTES",
                "AI" => "MEDIDOR DEPOIS",
                "AJ" => "MOTIVO DE IMPEDIMENTO",

//                Padrão de entrada e medidor
                "AK" => "PADRÃO DE ENTRADA",
                "AL" => "STATUS PADRÃO DE ENTRADA",
                "AM" => "PADRÃO DE RAMAL",
                "AN" => "STATUS PADRÃO DE RAMAL",
                "AO" => "SITUAÇÃO DO LOCAL",
                "AP" => "STATUS SITUAÇÃO DO LOCAL",
                "AQ" => "MEDIDOR",
                "AR" => "STATUS MEDIDOR",
                "AS" => "TIPO DE ESTRUTURA",
                "AT" => "CÔMODOS",
                "AU" => "MORADORES",
                "AV" => "INEFICIÊNCIAS",
                "AW" => "PROBLEMAS",
                "AX" => "TIPO",
                "AY" => "RENDA FAMILIAR MENSAL",
                "AZ" => "POSSUI CONTA BANCÁRIA",
                "BA" => "POSSUI CARTÃO",
                "BB" => "LAMPADAS",
                "BC" => "TOMADAS",
                "BD" => "VENTILADORES",
                //Carga instalada
                "BE" => "CARGA INSTALADA",
                //Transferência de nome / Atualização de cadastro
                "BF" => "NOME COMPLETO",
                "BG" => "CPF",
                "BH" => "RG",
                "BI" => "TELEFONE",
                "BJ" => "EMAIL",
                //Morador sem tarifa social
                "BK" => "TIPO DE BENEFÍCIO",
                "BL" => "NÚMERO DO BENEFÍCIO",
                "BM" => "NOME",
                "BN" => "CPF",
                "BO" => "RG",
                "BP" => "DATA DE NASCIMENTO",
                //Observações
                "BQ" => "OBSERVAÇÕES",
            ],
            "filter"    => true,
            "width"     => 20,
            "height"    => 20
        ];

        $document = Excel::export($modelo, $configs);

        if(!$document["success"]) {
            return response()->json(["success" => false, "message" => $document["message"]]);
        }

        $activeSheet = $document["document"]->getActiveSheet(0)->toArray();

        $styleColumns = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
        ];

        if (count($activeSheet) > 0) {
            $line = 3;
            foreach ($entrevista as $key => $entr) {
                $line+=1;
                $visitas = DadosVisita::where('entrevista_id', $entr->uuid)->get();
                $document = $this->buildExcelLine($document,$line,$entr,$visitas,$styleColumns);
            }
        }

        $folderExcel = public_path() . '/storage/excel';

        $files = $document["document"];

        if(!\Storage::directories($folderExcel))
        {
            \Storage::makeDirectory('public/entrevista/excel');
        }

        $writer = IOFactory::createWriter($files, 'Xls');
        $writer->save(public_path(). '/storage/entrevista/excel/entrevistas.xls');

        try {
           // \Storage::disk('local')->put('/public/entrevista/excel/file.Xls', $writer);
            $files = glob('storage/entrevista/');
            \Zipper::make('storage/entrevistas.zip')->add($files)->close();
            $planilha = \File::glob(public_path('storage/entrevista/excel/entrevistas.xls'), GLOB_MARK);
            \File::delete($planilha);

            return response()->download(storage_path('app/public/' .'entrevistas.zip'))->deleteFileAfterSend(true);
        } catch (Exception $exc) {
            $planilha = \File::glob(public_path('storage/entrevista/excel/'), GLOB_MARK);
            \File::delete($planilha);
            return response()->json(['success' => false, 'msg' =>$exc->getMessage()]);
        }

    }



    private function buildExcelLine($document,$line,$entr,$visitas,$styleColumns){
        //Consultas para preenchimento dos campos da planilha
        $array = [ 0 => ['S','T','U','V','W','X'], 1 => ['Y','Z','AA','AB','AC','AD'], 2 => ['AE','AF','AG','AH','AI','AJ']];
        $padrao_entrada = PadraoEntrada::where('id',$entr->padrao_entrada)->first();
        $status_padrao_entrada = StatusPadraoEntrada::where('id',$entr->status_padrao_entrada)->first();
        $padrao_ramal = PadraoRamal::where('id',$entr->padrao_ramal)->first();
        $status_padrao_ramal = StatusPadraoRamal::where('id',$entr->status_padrao_ramal)->first();
        $consulta_situacao_local = SituacaoLocalEntrevista::join('situacao_local', 'situacao_local.id', '=',
                                                        'situacao_local_entrevista.situacao_local_id')
                                               ->select('situacao_local.descricao as descricao')
                                                ->where('situacao_local_entrevista.entrevista_id',
                                                        $entr->uuid)
                                                ->get()
                                                ->toArray();
        $consulta_medidor = MedidorEntrevista::join('medidor', 'medidor.id', '=',
            'medidor_entrevista.medidor_id')
            ->select('medidor.descricao as descricao')
            ->where('medidor_entrevista.entrevista_id',
                $entr->uuid)
            ->get()
            ->toArray();
        $consulta_ineficiencia = EntrevistaIneficiencias::join('ineficiencia', 'ineficiencia.id', '=',
            'entrevista_ineficiencias.ineficiencia_id')
            ->select('ineficiencia.descricao as descricao')
            ->where('entrevista_ineficiencias.entrevista_id',
                $entr->uuid)
            ->get()
            ->toArray();
        $consulta_problemas = ProblemaEntrevista::join('problema', 'problema.id', '=',
            'problemas_entrevista.problema_id')
            ->select('problema.descricao as descricao')
            ->where('problemas_entrevista.entrevista_id',
                $entr->uuid)
            ->get()
            ->toArray();
        $consulta_aparelhos = AparelhoCargaEntrevista::join('aparelho_carga', 'aparelho_carga.id', '=',
            'aparelho_carga_entrevista.aparelho_carga_id')
            ->select('aparelho_carga.descricao as descricao')
            ->where('aparelho_carga_entrevista.entrevista_id',
                $entr->uuid)
            ->get()
            ->toArray();
        $transferenciaNome = TransferenciaNomeAtualizacaoCadastro::select('transferencia_nome_atualizacao_cadastro.nome as nome',
                                                                          'transferencia_nome_atualizacao_cadastro.cpf as cpf',
                                                                          'transferencia_nome_atualizacao_cadastro.rg as rg',
                                                                          'transferencia_nome_atualizacao_cadastro.telefone as telefone',
                                                                          'transferencia_nome_atualizacao_cadastro.email as email')
                                                                  ->where('entrevista_id', $entr->uuid)
                                                                  ->first();
        $moradorSemTarifa = MoradorSemTarifaSocial::join('tipo_beneficio', 'tipo_beneficio.id', '=',
                                                         'morador_sem_tarifa_social.tipo_beneficio_id')
                                                  ->select('morador_sem_tarifa_social.numero_beneficio as numero_beneficio',
                                                           'morador_sem_tarifa_social.nome as nome',
                                                           'morador_sem_tarifa_social.cpf as cpf',
                                                           'morador_sem_tarifa_social.rg as rg',
                                                           'morador_sem_tarifa_social.data_nascimento as data_nascimento',
                                                           'tipo_beneficio.descricao as tipo_beneficio')
                                                  ->first();
        // Pega a entrada "descricao" de cada array de situacao_local e transforma em string separando por vírgula.
        if (isset($consulta_situacao_local) && $consulta_situacao_local != []) {
            $situacao_local = implode(', ', array_map(function ($entry) {
                return $entry['descricao'];
            }, $consulta_situacao_local));
        }
        // Pega a entrada "descricao" de cada array de medidor e transforma em string separando por vírgula.
        if (isset($consulta_medidor) && $consulta_medidor != []) {
            $medidor = implode(', ', array_map(function ($entry) {
                return $entry['descricao'];
            }, $consulta_medidor));
        }
        // Pega a entrada "descricao" de cada array de ineficiência e transforma em string separando por vírgula.
        if (isset($consulta_ineficiencia) && $consulta_ineficiencia != []) {
            $ineficiencia = implode(', ', array_map(function ($entry) {
                return $entry['descricao'];
            }, $consulta_ineficiencia));
        }
        // Pega a entrada "descricao" de cada array de problemas e transforma em string separando por vírgula.
        if (isset($consulta_problemas) && $consulta_ineficiencia != []) {
            $problema = implode(', ', array_map(function ($entry) {
                return $entry['descricao'];
            }, $consulta_problemas));
        }
        // Pega a entrada "descricao" de cada array de aparelhos carga e transforma em string separando por vírgula.
        if (isset($consulta_aparelhos) && $consulta_aparelhos != []) {
            $aparelhoCarga = implode(', ', array_map(function ($entry) {
                return $entry['descricao'];
            }, $consulta_aparelhos));
        }
        $status_situacao_local = StatusSituacaoLocal::where('id',$entr->status_situacao_local)->first();
        $status_medidor = StatusMedidor::where('id',$entr->status_medidor)->first();
        $tipo_estrutura = TipoEstrutura::where('id',$entr->tipo_estrutura)->first();
        $comodos = Comodos::where('id',$entr->comodos)->first();
        $moradores = Moradores::where('id',$entr->moradores)->first();
        $tipo = Tipo::where('id',$entr->tipo)->first();
        $rendaFamiliar = RendaFamiliarMensal::where('id',$entr->renda_familiar)->first();

//      Dados cadastrados
        $document["document"]->getActiveSheet(0)->setCellValue('A' . $line, isset($entr->cliente->nome) ? $entr->cliente->nome : '');
        $document["document"]->getActiveSheet(0)->setCellValue('B' . $line, isset($entr->cliente->cpf) ? $entr->cliente->cpf : '');
        $document["document"]->getActiveSheet(0)->setCellValue('C' . $line, isset($entr->cliente->instalacao) ? $entr->cliente->instalacao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('D' . $line, isset($entr->cliente->nr_cliente) ? $entr->cliente->nr_cliente : '');
        $document["document"]->getActiveSheet(0)->setCellValue('E' . $line, isset($entr->cliente->telefone) ? $entr->cliente->telefone : '');
        $document["document"]->getActiveSheet(0)->setCellValue('F' . $line, isset($entr->cliente->endereco) ? $entr->cliente->endereco : '');
        $document["document"]->getActiveSheet(0)->setCellValue('G' . $line, isset($entr->cliente->bairro) ? $entr->cliente->bairro : '');
        $document["document"]->getActiveSheet(0)->setCellValue('H' . $line, isset($entr->cliente->cep) ? $entr->cliente->cep : '');
        $document["document"]->getActiveSheet(0)->setCellValue('I' . $line, isset($entr->cliente->nr_medidor) ? $entr->cliente->nr_medidor : '');
        $document["document"]->getActiveSheet(0)->setCellValue('J' . $line, isset($entr->cliente->sequencia_leitura) ? $entr->cliente->sequencia_leitura : '');
        $document["document"]->getActiveSheet(0)->setCellValue('K' . $line, isset($entr->cliente->ultima_leitura) ? $entr->cliente->ultima_leitura : '');
        $document["document"]->getActiveSheet(0)->setCellValue('L' . $line, isset($entr->cliente->nota_leitura) ? $entr->cliente->nota_leitura : '');
        $document["document"]->getActiveSheet(0)->setCellValue('M' . $line, isset($entr->cliente->debito_total) ? $entr->cliente->debito_total : '');
        $document["document"]->getActiveSheet(0)->setCellValue('N' . $line, isset($entr->cliente->consumo_medio) ? $entr->cliente->consumo_medio : '');
        $document["document"]->getActiveSheet(0)->setCellValue('O' . $line, isset($entr->cliente->tarifa_social) == true ? 'SIM' : 'NÃO');
        $document["document"]->getActiveSheet(0)->setCellValue('P' . $line, $entr->cliente->cortado_sistema == true ? 'SIM' : 'NÃO');
        $document["document"]->getActiveSheet(0)->setCellValue('Q' . $line, $entr->cliente->contrato_ativo == true ? 'SIM' : 'NÃO');
        $document["document"]->getActiveSheet(0)->setCellValue('R' . $line, $entr->cliente->toi == true ? 'SIM' : 'NÃO');
        if (isset($visitas)) {
            foreach ($visitas as $key => $visita) {
                $motivo_impedimento = MotivoImpedimento::where('id',$visita->impedimento_id)->first();
                $document["document"]->getActiveSheet(0)->setCellValue($array[$key][0] . $line, dateToView($visita->data_visita));
                $document["document"]->getActiveSheet(0)->setCellValue($array[$key][1] . $line, $visita->medidor_encontrado);
                $document["document"]->getActiveSheet(0)->setCellValue($array[$key][2]. $line, $visita->medidor_vizinho);
                $document["document"]->getActiveSheet(0)->setCellValue($array[$key][3]. $line, $visita->medidor_antes);
                $document["document"]->getActiveSheet(0)->setCellValue($array[$key][4] . $line, $visita->medidor_depois);
                $document["document"]->getActiveSheet(0)->setCellValue($array[$key][5]. $line, isset($motivo_impedimento) ? $motivo_impedimento->descricao : 'Nenhum');
            }
        }
        $document["document"]->getActiveSheet(0)->setCellValue('AK' . $line, isset($padrao_entrada) ? $padrao_entrada->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AL' . $line, isset($status_padrao_entrada) ? $status_padrao_entrada->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AM' . $line, isset($padrao_ramal) ? $padrao_ramal->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AN' . $line, isset($status_padrao_ramal) ? $status_padrao_ramal->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AO' . $line, isset($situacao_local) ? $situacao_local : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AP' . $line, isset($status_situacao_local) ? $status_situacao_local->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AQ' . $line, isset($medidor) ? $medidor : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AR' . $line, isset($status_medidor) ? $status_medidor->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AS' . $line, isset($tipo_estrutura) ? $tipo_estrutura->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AT' . $line, isset($comodos) ? $comodos->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AU' . $line, isset($moradores) ? $moradores->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AV' . $line, isset($ineficiencia) ? $ineficiencia : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AW' . $line, isset($problema) ? $problema : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AX' . $line, isset($tipo) ? $tipo->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AY' . $line, isset($tipo) ? $rendaFamiliar->descricao : '');
        $document["document"]->getActiveSheet(0)->setCellValue('AZ' . $line, $entr->possui_conta == true ? 'SIM' : 'NÃO');
        $document["document"]->getActiveSheet(0)->setCellValue('BA' . $line, $entr->possui_cartao == true ? 'SIM' : 'NÃO');
        $document["document"]->getActiveSheet(0)->setCellValue('BB' . $line, isset($entr->lampadas) ? $entr->lampadas  : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BC' . $line, isset($entr->tomadas) ? $entr->tomadas : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BD' . $line, isset($entr->ventiladores) ? $entr->ventiladores : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BE' . $line, isset($aparelhoCarga) ? $aparelhoCarga : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BF' . $line, isset($transferenciaNome) ? $transferenciaNome->nome : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BG' . $line, isset($transferenciaNome) ? $transferenciaNome->cpf : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BH' . $line, isset($transferenciaNome) ? $transferenciaNome->rg : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BI' . $line, isset($transferenciaNome) ? $transferenciaNome->telefone : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BJ' . $line, isset($transferenciaNome) ? $transferenciaNome->email : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BK' . $line, isset($moradorSemTarifa) ? $moradorSemTarifa->tipo_beneficio : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BL' . $line, isset($moradorSemTarifa) ? $moradorSemTarifa->numero_beneficio : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BM' . $line, isset($moradorSemTarifa) ? $moradorSemTarifa->nome : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BN' . $line, isset($moradorSemTarifa) ? $moradorSemTarifa->cpf : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BO' . $line, isset($moradorSemTarifa) ? $moradorSemTarifa->rg : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BP' . $line, isset($moradorSemTarifa) ? dateToView($moradorSemTarifa->data_nascimento) : '');
        $document["document"]->getActiveSheet(0)->setCellValue('BQ' . $line, isset($entr->observacoes) ? $entr->observacoes : '');
        $document["document"]->getActiveSheet(0)->getStyle('A' . $line . ':BQ' . $line)->applyFromArray($styleColumns);

        return $document;
    }

     public function destroy(Request $request)
     {
         try {

             $id = $request->input('id');

             $entrevistaDel = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
                 ->select('cliente.id as cliente_id',
                     'cliente.nome as nome',
                     'cliente.cpf as cpf',
                     'cliente.instalacao as instalacao',
                     'entrevista.id as id')
                 ->where('entrevista.id', $id)
                 ->first();

             \DB::table('medidor_entrevista')->where('entrevista_id', $id)->delete();
             \DB::table('entrevista_ineficiencias')->where('entrevista_id', $id)->delete();
             \DB::table('problemas_entrevista')->where('entrevista_id', $id)->delete();
             \DB::table('situacao_local_entrevista')->where('entrevista_id', $id)->delete();
             \DB::table('aparelho_carga_entrevista')->where('entrevista_id', $id)->delete();
             \DB::table('dados_visita')->where('entrevista_id', $id)->delete();
             \DB::table('transferencia_nome_atualizacao_cadastro')->where('entrevista_id', $id)->delete();
             \DB::table('morador_sem_tarifa_social')->where('entrevista_id', $id)->delete();

             $delete = \DB::table('entrevista')->where('id', $id)->delete();
             if ($delete) {
                 $folderInstalacao = public_path('storage/entrevista/'.$entrevistaDel->instalacao);
                 File::deleteDirectory($folderInstalacao);
                 return response()->json(['success' => true, 'msg' => 'Entrevista excluída com sucesso!']);
             } else {
                 return response()->json(['success' => false, 'msg' => 'Erro ao excluir entrevista!']);
             }
         } catch(\Exception $e) {
             if ($e->getCode() == 23503) {
                 return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de entrevistas em uso!']);
             } else {
                 return response()->json(['success' => false, 'msg' => 'Erro ao excluir entrevista!']);
             }

         }
     }
}
