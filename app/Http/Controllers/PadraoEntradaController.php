<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PadraoEntrada;

class PadraoEntradaController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $padraoEntrada = PadraoEntrada::orderBy('descricao');

        if($filter)
        {
            $padraoEntrada->where("descricao", "ilike", "%$filter%");
        }

        $padraoEntrada = $padraoEntrada->paginate(10)->appends('filter', request('filter'));

        return view('pages.padrao-entrada.index', compact('padraoEntrada'));
    }

    public function create()
    {
        return view('pages.padrao-entrada.form');
    }

    public function store(Request $request)
    {
        $id = $request->input("id");

        $padraoEntrada = PadraoEntrada::find($id);

        if (!$padraoEntrada) {
            $padraoEntrada = new PadraoEntrada();
        }

        $padraoEntrada->fill($request->all());

        $validate = validator($request->all(), $padraoEntrada->rules(), $padraoEntrada->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $padraoEntrada->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Padrão de entrada salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar padrão de entrada!']);
        }
    }

    public function edit(PadraoEntrada $padraoEntrada)
    {
        return view('pages.padrao-entrada.form', compact('padraoEntrada'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('padrao_entrada')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Padrão de entrada excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir padrão de entrada!']);
            }
        }catch(\Exception $e){
            if ($e->getCode() == 23503){
                return response()->json(['sucess' => false, 'msg' => 'Não é permitida a exclusão de padrões de entrada em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir padrão de entrada!']);
            }
        }
    }
}
