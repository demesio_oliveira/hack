<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoEstrutura;

class TipoEstruturaController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $tipoEstrutura = TipoEstrutura::orderBy('descricao');

        if($filter)
        {
            $tipoEstrutura->where("descricao", "ilike", "%$filter%");
        }

        $tipoEstrutura = $tipoEstrutura->paginate(10)->appends('filter', request('filter'));

        return view('pages.tipo-estrutura.index', compact('tipoEstrutura'));
    }

    public function create()
    {
        return view('pages.tipo-estrutura.form');
    }

    public function store(Request $request)
    {
        $id = $request->input("id");

        $tipoEstrutura = TipoEstrutura::find($id);

        if (!$tipoEstrutura) {
            $tipoEstrutura = new TipoEstrutura();
        }

        $tipoEstrutura->fill($request->all());

        $validate = validator($request->all(), $tipoEstrutura->rules(), $tipoEstrutura->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $tipoEstrutura->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Tipo de estrutura salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar tipo de estrutura!']);
        }
    }

    public function edit(TipoEstrutura $tipoEstrutura)
    {
        return view('pages.tipo-estrutura.form', compact('tipoEstrutura'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('tipo_estrutura')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Tipo de estrutura excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir tipo de estrutura!']);
            }
        }catch(\Exception $e){
            if ($e->getCode() == 23503){
                return response()->json(['sucess' => false, 'msg' => 'Não é permitida a exclusão de tipos de estrutura em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir tipo de estrutura!']);
            }
        }
    }
}
