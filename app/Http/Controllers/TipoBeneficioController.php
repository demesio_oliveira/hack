<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoBeneficio;

class TipoBeneficioController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $tipoBeneficio = TipoBeneficio::orderBy('descricao');

        if($filter)
        {
            $tipoBeneficio->where("descricao", "ilike", "%$filter%");
        }

        $tipoBeneficio = $tipoBeneficio->paginate(10)->appends('filter', request('filter'));

        return view('pages.tipo-beneficio.index', compact('tipoBeneficio'));
    }

    public function create()
    {
        return view('pages.tipo-beneficio.form');
    }

    public function store(Request $request)
    {
        $id = $request->input("id");

        $tipoBeneficio = TipoBeneficio::find($id);

        if (!$tipoBeneficio) {
            $tipoBeneficio = new TipoBeneficio();
        }

        $tipoBeneficio->fill($request->all());

        $validate = validator($request->all(), $tipoBeneficio->rules(), $tipoBeneficio->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $tipoBeneficio->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Tipo de benefício salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar tipo de benefício!']);
        }
    }

    public function edit(TipoBeneficio $tipoBeneficio)
    {
        return view('pages.tipo-beneficio.form', compact('tipoBeneficio'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('tipo_beneficio')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Tipo de benefício excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir tipo de benefício!']);
            }
        }catch(\Exception $e){
            if ($e->getCode() == 23503){
                return response()->json(['sucess' => false, 'msg' => 'Não é permitida a exclusão de tipos de benefício em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir tipo de benefício!']);
            }
        }
    }
}
