<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Moradores;

class MoradoresController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $moradores = Moradores::orderBy('descricao');

        if($filter)
        {
            $moradores->where("descricao", "ilike", "%$filter%");
        }

        $moradores = $moradores->paginate(10)->appends('filter', request('filter'));

        return view('pages.moradores.index', compact('moradores'));
    }

    public function create()
    {
        return view('pages.moradores.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $moradores = Moradores::find($id);

        if (!$moradores) {
            $moradores = new Moradores();
        }

        $moradores->fill($request->all());

        $validate = validator($request->all(), $moradores->rules(), $moradores->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $moradores->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Moradores salvos com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar moradores!']);
        }
    }

    public function edit(Moradores $moradores)
    {
        return view('pages.moradores.form', compact('moradores'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('moradores')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Moradores excluídos com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir moradores!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de moradores em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir moradores!']);
            }
        }
    }
}
