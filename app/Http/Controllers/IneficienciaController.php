<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ineficiencia;

class IneficienciaController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $ineficiencia = Ineficiencia::orderBy('descricao');

        if($filter)
        {
            $ineficiencia->where("descricao", "ilike", "%$filter%");
        }

        $ineficiencia = $ineficiencia->paginate(10)->appends('filter', request('filter'));

        return view('pages.ineficiencia.index', compact('ineficiencia'));
    }

    public function create()
    {
        return view('pages.ineficiencia.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $ineficiencia = Ineficiencia::find($id);

        if (!$ineficiencia) {
            $ineficiencia = new Ineficiencia();
        }

        $ineficiencia->fill($request->all());

        $validate = validator($request->all(), $ineficiencia->rules(), $ineficiencia->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $ineficiencia->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Ineficiência salva com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar Ineficiência!']);
        }
    }

    public function edit(Ineficiencia $ineficiencia)
    {
        return view('pages.ineficiencia.form', compact('ineficiencia'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('ineficiencia')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Ineficiência excluída com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir Ineficiência!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de Ineficiências em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir Ineficiência!']);
            }
        }
    }
}
