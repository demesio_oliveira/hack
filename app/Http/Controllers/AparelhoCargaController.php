<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AparelhoCarga;

class AparelhoCargaController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $aparelhoCarga = AparelhoCarga::orderBy('descricao');

        if($filter)
        {
            $aparelhoCarga->where("descricao", "ilike", "%$filter%");
        }

        $aparelhoCarga = $aparelhoCarga->paginate(10)->appends('filter', request('filter'));

        return view('pages.aparelho-carga.index', compact('aparelhoCarga'));
    }

    public function create()
    {
        return view('pages.aparelho-carga.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $aparelhoCarga = AparelhoCarga::find($id);

        if (!$aparelhoCarga) {
            $aparelhoCarga = new AparelhoCarga();
        }

        $aparelhoCarga->fill($request->all());

        $validate = validator($request->all(), $aparelhoCarga->rules(), $aparelhoCarga->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $aparelhoCarga->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Aparelho carga salva com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar aparelho carga!']);
        }
    }

    public function edit(AparelhoCarga $aparelhoCarga)
    {
        return view('pages.aparelho-carga.form', compact('aparelhoCarga'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('aparelho_carga')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Aparelho carga excluída com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir aparelho carga!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de aparelhos carga em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir aparelho carga!']);
            }
        }
    }
}
