<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Problema;

class ProblemaController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $problema = Problema::orderBy('descricao');

        if($filter)
        {
            $problema->where("descricao", "ilike", "%$filter%");
        }

        $problema = $problema->paginate(10)->appends('filter', request('filter'));

        return view('pages.problema.index', compact('problema'));
    }

    public function create()
    {
        return view('pages.problema.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $problema = Problema::find($id);

        if (!$problema) {
            $problema = new Problema();
        }

        $problema->fill($request->all());

        $validate = validator($request->all(), $problema->rules(), $problema->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $problema->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Problema salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar problema!']);
        }
    }

    public function edit(Problema $problema)
    {
        return view('pages.problema.form', compact('problema'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('problema')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Problema excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir problema!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de problemas em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir problema!']);
            }
        }
    }
}
