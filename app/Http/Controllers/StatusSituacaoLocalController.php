<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusSituacaoLocal;

class StatusSituacaoLocalController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $statusSituacaoLocal = StatusSituacaoLocal::orderBy('descricao');

        if($filter)
        {
            $statusSituacaoLocal->where("descricao", "ilike", "%$filter%");
        }

        $statusSituacaoLocal = $statusSituacaoLocal->paginate(10)->appends('filter', request('filter'));

        return view('pages.status-situacao-local.index', compact('statusSituacaoLocal'));
    }

    public function create()
    {
        return view('pages.status-situacao-local.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $statusSituacaoLocal = StatusSituacaoLocal::find($id);

        if (!$statusSituacaoLocal) {
            $statusSituacaoLocal = new StatusSituacaoLocal();
        }

        $statusSituacaoLocal->fill($request->all());

        $validate = validator($request->all(), $statusSituacaoLocal->rules(), $statusSituacaoLocal->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $statusSituacaoLocal->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Status da situação do local salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar status da situação do local!']);
        }
    }

    public function edit(StatusSituacaoLocal $statusSituacaoLocal)
    {
        return view('pages.status-situacao-local.form', compact('statusSituacaoLocal'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('status_situacao_local')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Status da situação do local excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir status da situação do local!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de status de situação do local em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir status de situação do local!']);
            }
        }
    }
}
