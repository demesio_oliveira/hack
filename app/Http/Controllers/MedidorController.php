<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medidor;

class MedidorController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $medidor = Medidor::orderBy('descricao');

        if($filter)
        {
            $medidor->where("descricao", "ilike", "%$filter%");
        }

        $medidor = $medidor->paginate(10)->appends('filter', request('filter'));

        return view('pages.medidor.index', compact('medidor'));
    }

    public function create()
    {
        return view('pages.medidor.form');
    }

    public function store(Request $request)
    {
        $id = $request->input('id');

        $medidor = Medidor::find($id);

        if (!$medidor) {
            $medidor = new Medidor();
        }

        $medidor->fill($request->all());

        $validate = validator($request->all(), $medidor->rules(), $medidor->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => validateErros($validate->errors()), 'validate' => true]);
        }

        $save = $medidor->save();

        if($save) {
            return response()->json(['success' => true, 'msg' => 'Medidor salvo com sucesso!']);
        } else {
            return response()->json(['success' => null, 'msg' => 'Erro ao salvar medidor!']);
        }
    }

    public function edit(Medidor $medidor)
    {
        return view('pages.medidor.form', compact('medidor'));
    }

    public function destroy(Request $request)
    {
        try {
            $id = $request->input('id');

            $delete = \DB::table('medidor')->where('id', $id)->delete();

            if ($delete) {
                return response()->json(['success' => true, 'msg' => 'Medidor excluído com sucesso!']);
            } else {
                return response()->json(['success' => null, 'msg' => 'Erro ao excluir medidor!']);
            }
        } catch(\Exception $e) {
            if ($e->getCode() == 23503) {
                return response()->json(['success' => false, 'msg' => 'Não é permitida a exclusão de medidores em uso!']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir medidor!']);
            }
        }
    }
}
