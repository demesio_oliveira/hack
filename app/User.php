<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use App\Models\Permission;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Uuid;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'uuid','name', 'email', 'password_mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function authorizePermissionView($permission)
    {
        if ($this->hasPermissions('ADMIN')) {
            return true;
        }

        if ($this->hasAnyPermissions($permission)) {
            return true;
        }
    }

    //Validar por permissão
    public function authorizePermission($permission)
    {
        if( $this->hasPermissions('ADMIN'))
        {
            return true;
        }

        if($this->hasAnyPermissions($permission))
        {
            return true;
        }

        abort(401, 'Ação não autorizada.');

    }

    public function hasAnyPermissions($permissions)
    {
        if(is_array($permissions))
        {
            foreach ($permissions as $permission)
            {
                if($this->hasPermissions($permission))
                {
                    return true;
                }
            }
        }elseif (is_object($permissions)){
            foreach ($permissions as $permission)
            {
                if($this->hasPermissions($permission->name))
                {
                    return true;
                }
            }
        } else{
            if($this->hasPermissions($permissions))
            {
                return true;
            }
        }

        return false;
    }

    public function hasPermissions($permission)
    {
        $user = \DB::table('role_user')->join('permission_role', 'role_user.role_id', '=', 'permission_role.role_id')
            ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->where('role_user.user_id', $this->id)
            ->where('permissions.name', $permission)
            ->first();

        if($user){
            return true;
        }

        return false;
    }

    public function rules(){
        return $rules = [
            'name' => 'required|max:255',
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($this->id, 'id')],
        ];
    }

    public $mensages = [
        'name.required' => 'Nome não informado.',
        'name.max' => 'O tamanho máximo do nome é de 255 caracteres',
        'email.required' => 'E-mail não informado.',
        'email.max' => 'O tamanho máximo do e-mail é de 255 caracteres',
        'email.email' => 'E-mail em um formato inválido',
        'email.unique' => 'E-mail já cadastrado para outro Usuário.'
    ];


}
