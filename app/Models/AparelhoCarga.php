<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AparelhoCarga extends Model
{
    protected $table    = "aparelho_carga";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:aparelho_carga,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do aparelho carga não informada.',
        'descricaodescricao.unique' => 'Descrição do aparelho carga já cadastrada.',
        'descricao.max' => 'Descrição do aparelho carga deve conter no máximo 75 caracteres.',
    ];

    public function checkAparelhosCarga($arraySelect) {
        return (in_array($this->id, array_column($arraySelect, 'aparelho_carga_id'))) ? true : false;
    }
}
