<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusMedidor extends Model
{
    protected $table    = "status_medidor";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:status_medidor,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição de status do medidor não informada.',
        'descricao.unique' => 'Descrição de status do medidor já cadastrada.',
        'descricao.max' => 'Descrição de status do medidor deve conter no máximo 75 caracteres.',
    ];
}
