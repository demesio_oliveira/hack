<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEstrutura extends Model
{
    protected $table    = "tipo_estrutura";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:tipo_estrutura,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do tipo de estrutura não informada.',
        'descricao.unique' => 'Descrição do tipo de estrutura já cadastrada.',
        'descricao.max' => 'Descrição do tipo de estrutura deve conter no máximo 75 caracteres.',
    ];
}
