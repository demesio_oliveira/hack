<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comodos extends Model
{
    protected $table    = "comodos";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:comodos,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição dos cômodos não informada.',
        'descricao.unique' => 'Descrição dos cômodos já cadastrada.',
        'descricao.max' => 'Descrição dos cômodos deve conter no máximo 75 caracteres.',
    ];
}
