<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PadraoRamal extends Model
{
    protected $table    = "padrao_ramal";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:padrao_ramal,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do padrão de ramal não informada.',
        'descricaodescricao.unique' => 'Descrição do padrão de ramal já cadastrada.',
        'descricao.max' => 'Descrição do padrão de ramal deve conter no máximo 75 caracteres.',
    ];
}
