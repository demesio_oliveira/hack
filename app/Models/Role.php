<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Role extends Model
{
    protected $fillable = ['name', 'description'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }


    public function rules() {
        return [
            'name'          => ['required', Rule::unique('roles')->ignore($this->id, 'id')],
            'description'   => 'required'
        ];
    }

    public $mensages = [
        'name.required'         => 'Nome é obrigatório.',
        'name.unique'           => 'Nome já cadastrada.',
        'description.required'  => 'Descrição é obrigatoria.'
    ];
}
