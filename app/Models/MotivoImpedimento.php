<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MotivoImpedimento extends Model
{
    protected $table    = "motivo_impedimento";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:motivo_impedimento,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do motivo de impedimento não informada.',
        'descricao.unique' => 'Descrição do motivo de impedimento já cadastrada.',
        'descricao.max' => 'Descrição do motivo de impedimento deve conter no máximo 75 caracteres.',
    ];
}
