<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoradorSemTarifaSocial extends Model
{
    protected $table    = "morador_sem_tarifa_social";
    protected $fillable = ['nome', 'cpf', 'rg', 'telefone', 'data_nascimento', 'tipo_beneficio_id', 'numero_beneficio', 'entrevista_id'];

    public function rules() {
        return [
            'nome_morador' => 'required|max:125',
            'cpf_morador' => 'required|max:11|unique:morador_sem_tarifa_social,cpf'. (($this->id) ? ', ' . $this->id : ''),
            'rg_morador' => 'required|max:8|unique:morador_sem_tarifa_social,rg'. (($this->id) ? ', ' . $this->id : ''),
            'data_nascimento' => 'required|max:11',
            'tipo_beneficio_id' => 'required|not_in:0',
            'numero_beneficio' => 'required',
        ];
    }

    public $mensages = [
        'nome_morador.required' => 'Nome do Morador não informado.',
        'nome_morador.max' => 'Nome do Morador deve conter no máximo 75 caracteres.',
        'cpf_morador.required' => 'CPF do Morador não informado.',
        'cpf_morador.max' => 'CPF do Morador deve conter no máximo 11 caracteres.',
        'rg_morador.unique' => 'RG do Morador já cadastrado.',
        'rg_morador.max' => 'RG do Morador deve conter no máximo 8 caracteres.',
        'data_nascimento.required' => 'Data de nascimento do Morador não informado.',
        'tipo_beneficio_id.not_in' => 'Tipo de benefício do Morador não informado.',
        'numero_beneficio.required' => 'Número do benefício do Morador não informado.',
    ];
}
