<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Lote extends Model
{
    protected $table    = "lote";
    protected $fillable = ['descricao'];

    private static $typesAccepted = ['xls', 'xlsx', 'csv'];

    public function countRegistros()
    {
        return Cliente::where('lote_id', $this->id)->count();
    }

    public static function getLotes($id, $descricao, $dataIni, $dataFim)
    {
        $lote = Lote::orderBy('created_at', 'desc');

        if($id)
        {
            $lote->where('id', $id);
        }

        if($descricao)
        {
            $lote->where('descricao', 'ilike', "%$descricao%");
        }

        if($dataIni)
        {
            $lote->where('created_at', '>=', $dataIni);
        }

        if($dataFim)
        {
            $lote->where('created_at', '<=', $dataFim);
        }


        return $lote;

    }

    public static function readFile($file) : array
    {

        $fileType = $file->getClientOriginalExtension();

        if(!in_array($fileType, Lote::$typesAccepted))
        {
            return response()->json('Tipo de arquivo inválido', 500);
        }

        $filePath = $file->getRealPath();

        $reader = IOFactory::createReader(ucfirst($fileType));
        $reader->setLoadAllSheets();
        $reader->setReadDataOnly(true);

        $spreadsheet = $reader->load($filePath);
        $sheet = $spreadsheet->getActiveSheet();

        $maxRow = $sheet->getHighestRow();
        $maxCol = $sheet->getHighestColumn();

        $rows = $sheet->rangeToArray("A2:" . $maxCol . $maxRow);

        return $rows;

    }

    public static function readColumns($file) : array
    {

        $fileType = $file->getClientOriginalExtension();

        $filePath = $file->getRealPath();

        $reader = IOFactory::createReader(ucfirst($fileType));
        $reader->setLoadAllSheets();
        $reader->setReadDataOnly(true);

        $spreadsheet = $reader->load($filePath);
        $sheet = $spreadsheet->getActiveSheet();

        $columns = $sheet->toArray();

        return $columns[0];

    }

    public static function toSelect()
    {
        return Lote::select("id", \DB::raw("id || ' | ' || descricao as descricao"))->get()->toArray();
    }


}
