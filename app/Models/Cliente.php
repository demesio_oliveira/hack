<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{

    protected $table    = "cliente";
    protected $fillable = ['nome',
                           'email',
                           'telefone',
                           'cpf',
                           'data_nascimento',
                           'sexo',
                           'logradouro',
                           'numero',
                           'bairro',
                           'cidade',
                           'estado',
                           'cep'];

}
