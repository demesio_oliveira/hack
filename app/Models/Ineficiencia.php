<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ineficiencia extends Model
{
    protected $table    = "ineficiencia";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:ineficiencia,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do ineficiência não informada.',
        'descricao.unique' => 'Descrição do ineficiência já cadastrada.',
        'descricao.max' => 'Descrição do ineficiência deve conter no máximo 75 caracteres.',
    ];

    public function checkIneficiencias($arraySelect) {
        return (in_array($this->id, array_column($arraySelect, 'ineficiencia_id'))) ? true : false;
    }
}
