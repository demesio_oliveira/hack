<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusPadraoRamal extends Model
{
    protected $table    = "status_padrao_ramal";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:status_padrao_ramal,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do status de padrão ramal não informada.',
        'descricao.unique' => 'Descrição do status de padrão ramal já cadastrada.',
        'descricao.max' => 'Descrição do status de padrão ramal deve conter no máximo 75 caracteres.',
    ];
}
