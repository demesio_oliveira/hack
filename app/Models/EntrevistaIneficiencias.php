<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntrevistaIneficiencias extends Model
{
    protected $table = 'entrevista_ineficiencias';
    protected $fillable = ['entrevista_id', 'ineficiencia_id'];

    public function entrevista()
    {
        return $this->belongsTo(\App\Models\Entrevista::class, 'entrevista_id', 'id');
    }

    public function medidor()
    {
        return $this->belongsTo(\App\Models\Ineficiencia::class, 'ineficiencia_id', 'id');
    }

    public function rules() {
        return [
            'entrevista_id' => 'required',
        ];
    }
    public $mesages = [
        'entrevista_id.required' => 'Entrevista não informada.',
    ];
}
