<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RendaFamiliarMensal extends Model
{
    protected $table    = "renda_familiar_mensal";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:renda_familiar_mensal,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição da renda familiar mensal não informada.',
        'descricao.unique' => 'Descrição da renda familiar mensal já cadastrada.',
        'descricao.max' => 'Descrição da renda familiar mensal deve conter no máximo 75 caracteres.',
    ];
}
