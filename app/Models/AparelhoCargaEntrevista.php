<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AparelhoCargaEntrevista extends Model
{
    protected $table = 'aparelho_carga_entrevista';
    protected $fillable = ['entrevista_id', 'aparelho_carga_id'];

    public function entrevista()
    {
        return $this->belongsTo(\App\Models\Entrevista::class, 'entrevista_id', 'id');
    }

    public function aparelhoCarga()
    {
        return $this->belongsTo(\App\Models\AparelhoCarga::class, 'aparelho_carga_id', 'id');
    }

    public function rules() {
        return [
            'entrevista_id' => 'required',
        ];
    }
    public $mesages = [
        'entrevista_id.required' => 'Entrevista não informada.',
    ];
}
