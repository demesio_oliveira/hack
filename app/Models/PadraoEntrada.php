<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PadraoEntrada extends Model
{
    protected $table    = "padrao_entrada";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:padrao_entrada,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do padrão de entrada não informada.',
        'descricaodescricao.unique' => 'Descrição do padrão de entrada já cadastrada.',
        'descricao.max' => 'Descrição do padrão de entrada deve conter no máximo 75 caracteres.',
    ];
}
