<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medidor extends Model
{
    protected $table    = "medidor";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:medidor,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do medidor não informada.',
        'descricao.unique' => 'Descrição do medidor já cadastrada.',
        'descricao.max' => 'Descrição do medidor deve conter no máximo 75 caracteres.',
    ];

    public function checkMedidores($arraySelect) {
        return (in_array($this->id, array_column($arraySelect, 'medidor_id'))) ? true : false;
    }

}
