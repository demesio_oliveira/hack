<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProblemaEntrevista extends Model
{
    protected $table = 'problemas_entrevista';
    protected $fillable = ['entrevista_id', 'problema_id'];

    public function entrevista()
    {
        return $this->belongsTo(\App\Models\Entrevista::class, 'entrevista_id', 'id');
    }

    public function problema()
    {
        return $this->belongsTo(\App\Models\Problema::class, 'problema_id', 'id');
    }

    public function rules() {
        return [
            'entrevista_id' => 'required',
        ];
    }
    public $mesages = [
        'entrevista_id.required' => 'Entrevista não informada.',
    ];
}
