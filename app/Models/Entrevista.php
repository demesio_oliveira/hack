<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entrevista extends Model
{
    protected $casts = ['id' => 'id'];

    //para deixar uuid como PK
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $table    = "entrevista";
    protected $fillable = ['id', 'cliente_id', 'padrao_entrada_id', 'status_padrao_entrada_id', 'padrao_ramal_id', 'status_padrao_ramal_id',
    'status_situacao_local_id', 'status_medidor_id', 'tipo_estrutura_id', 'comodos_id', 'tipo_id', 'moradores_id',
    'renda_familiar_id', 'possui_conta', 'possui_cartao', 'lampadas', 'ventiladores', 'tomadas', 'observacoes', 'user_id'];


    public function rules() {
        return [
            'cliente_id' => 'required|not_in:0',
//            'cliente_id' => 'required|not_in:0|unique:entrevista,cliente_id'. (($this->id) ? ', ' . $this->id : ''),
            'lampadas' => 'required|max:8',
            'tomadas' => 'required|max:8',
            'ventiladores' => 'required|max:8',
            'padrao_entrada_id'=> 'required',
            'padrao_ramal_id'=> 'required',
            'tipo_estrutura_id' => 'required',
            'comodos_id' => 'required',
            'tipo_id' => 'required',
            'renda_familiar_id' => 'required',
            'moradores_id' => 'required'
        ];
    }

    public $mensages = [
        'cliente_id.required' => 'Cliente não selecionado.',
        'cliente_id.not_in' => 'Cliente não selecionado.',
        'cliente_id.unique' => 'Este cliente já possui uma entrevista.',
        'lampadas.required' => 'Número de lâmpadas não informado.',
        'tomadas.required' => 'Número de tomadas não informado.',
        'ventiladores.required' => 'Número de ventiladores não informado.',
        'padrao_entrada_id.required' => 'Selecione ao menos um padrão de entrada.',
        'padrao_ramal_id.required' => 'Selecione ao menos um padrão de ramal.',
        'tipo_estrutura_id.required' => 'Selecione ao menos um tipo de estrutura.',
        'comodos_id.required' => 'Selecione ao menos um número de cômodos.',
        'moradores_id.required' => 'Selecione ao menos um número de moradores.',
        'tipo_id.required' => 'Selecione ao menos um tipo.',
        'renda_familiar_id.required' => 'Selecione ao menos um valor de renda familiar mensal.',
        'lampadas.max' => 'O campo lâmpadas deve conter no máximo 8 dígitos.',
        'tomadas.max' => 'O campo tomadas deve conter no máximo 8 dígitos.',
        'ventiladores.max' => 'O campo ventiladores deve conter no máximo 8 dígitos.'
    ];

    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class, 'cliente_id', 'id');
    }

    public function getImagensThumbSituacaoLocal($situacaoLocal)
    {
        return getImagensThumb(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/situacao-local/imagens_situacao_local_'.$situacaoLocal->id.'/'), $this->id);
    }

    public function getImagensSituacaoLocal($situacaoLocal)
    {
        return getImagens(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/situacao-local/imagens_situacao_local_'.$situacaoLocal.'/'), $this->id);
    }

    public function getImagensThumbMedidor($medidor)
    {
        return getImagensThumb(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/medidor/imagens_medidor_'.$medidor->id.'/'), $this->id);
    }

    public function getImagensMedidor($medidor)
    {
        return getImagens(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/medidor/imagens_medidor_'.$medidor.'/'), $this->id);
    }

    public function getImagensThumbTransf()
    {
        return getImagensThumb(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/transferencia-nome/'), $this->id);
    }

    public function getImagemTransf()
    {
        return getImagens(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/transferencia-nome/'), $this->id);
    }

    public function getImagensThumbMorador()
    {
        return getImagensThumb(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/tarifa-social/'), $this->id);
    }

    public function getImagensVisita($visita)
    {
        return getImagens(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/visita/'), $visita);
    }

    public function getImagensThumbVisita($visita)
    {
        return getImagensThumb(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/visita/'), $visita);
    }

    public function getImagemMorador()
    {
        return getImagens(public_path('storage/entrevista/'.$this->cliente->instalacao.'/imagens/entrevista/tarifa-social/'), $this->id);
    }

    public static function filtroAvancado($request)
    {
        $nome = $request->input('nome');
        $cpf = $request->input('cpf');
        $instalacao = $request->input('instalacao');
        $endereco = $request->input('endereco');
        $nrMedidor = $request->input('nr_medidor');

        $entrevistas = Entrevista::join('cliente', 'cliente_id', '=', 'cliente.id')
                                 ->select('cliente.id as cliente_id',
                                          'cliente.nome as nome',
                                          'cliente.cpf as cpf',
                                          'cliente.instalacao as instalacao',
                                          'cliente.endereco as endereco',
                                          'cliente.nr_medidor as nr_medidor',
                                          'entrevista.id as uuid',
                                          'entrevista.padrao_entrada_id as padrao_entrada',
                                          'entrevista.status_padrao_entrada_id as status_padrao_entrada',
                                          'entrevista.padrao_ramal_id as padrao_ramal',
                                          'entrevista.status_padrao_ramal_id as status_padrao_ramal',
                                          'entrevista.status_situacao_local_id as status_situacao_local',
                                          'entrevista.status_medidor_id as status_medidor',
                                          'entrevista.tipo_estrutura_id as tipo_estrutura',
                                          'entrevista.comodos_id as comodos',
                                          'entrevista.moradores_id as moradores',
                                          'entrevista.tipo_id as tipo',
                                          'entrevista.renda_familiar_id as renda_familiar',
                                          'entrevista.possui_conta as possui_conta',
                                          'entrevista.possui_cartao as possui_cartao',
                                          'entrevista.lampadas as lampadas',
                                          'entrevista.tomadas as tomadas',
                                          'entrevista.ventiladores as ventiladores',
                                          'entrevista.observacoes as observacoes');

        if($nome)
        {
            $entrevistas->where('cliente.nome', 'ILIKE', "%$nome%");
        }
        if($cpf)
        {
            $entrevistas->where('cliente.cpf', 'ILIKE', "%$cpf%");
        }
        if($instalacao)
        {
            $entrevistas->whereRaw("CAST(cliente.instalacao as VARCHAR) ILIKE ?",["%".$instalacao."%"]);
        }
        if($endereco)
        {
            $entrevistas->where('cliente.endereco', 'ILIKE', "%$endereco%");
        }
        if($nrMedidor)
        {
            $entrevistas->whereRaw("CAST(cliente.nr_medidor as VARCHAR) ILIKE ?",["%".$nrMedidor."%"]);
        }

        return $entrevistas;

    }

}
