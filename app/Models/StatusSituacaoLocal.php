<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusSituacaoLocal extends Model
{
    protected $table    = "status_situacao_local";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:status_situacao_local,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do status de situação do local não informada.',
        'descricao.unique' => 'Descrição do status de situação do local já cadastrada.',
        'descricao.max' => 'Descrição do status de situação do local deve conter no máximo 75 caracteres.',
    ];
}
