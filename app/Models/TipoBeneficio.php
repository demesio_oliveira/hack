<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoBeneficio extends Model
{
    protected $table    = "tipo_beneficio";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:tipo_beneficio,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do tipo de benefício não informada.',
        'descricaodescricao.unique' => 'Descrição do tipo de benefício já cadastrada.',
        'descricao.max' => 'Descrição do tipo de benefício deve conter no máximo 75 caracteres.',
    ];
}
