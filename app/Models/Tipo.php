<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table    = "tipo";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:tipo,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do tipo não informada.',
        'descricao.unique' => 'Descrição do tipo já cadastrada.',
        'descricao.max' => 'Descrição do tipo deve conter no máximo 75 caracteres.',
    ];
}
