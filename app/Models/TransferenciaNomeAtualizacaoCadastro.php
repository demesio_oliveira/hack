<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferenciaNomeAtualizacaoCadastro extends Model
{
    protected $table    = "transferencia_nome_atualizacao_cadastro";
    protected $fillable = ['nome', 'cpf', 'rg', 'telefone', 'email', 'entrevista_id'];

    public function rules() {
        return [
            'nome_transf' => 'required|max:125',
            'cpf_transf' => 'required|max:11|unique:transferencia_nome_atualizacao_cadastro,cpf'. (($this->id) ? ', ' . $this->id : ''),
            'rg_transf' => 'required|max:8|unique:transferencia_nome_atualizacao_cadastro,rg'. (($this->id) ? ', ' . $this->id : ''),
            'telefone_transf' => 'required|max:11',
            'email_transf' => 'required|max:255',
        ];
    }

    public $mensages = [
        'nome_transf.required' => 'Nome não informado.',
        'nome_transf.max' => 'Nome deve conter no máximo 75 caracteres.',
        'cpf_transf.required' => 'CPF não informado.',
        'cpf_transf.max' => 'CPF deve conter no máximo 11 caracteres.',
        'rg_transf.unique' => 'RG já cadastrado.',
        'rg_transf.max' => 'RG deve conter no máximo 8 caracteres.',
        'telefone_transf.required' => 'Telefone não informado.',
        'telefone_transf.max' => 'Telefone deve conter no máximo 11 caracteres.',
        'email_transf.required' => 'Email não informado.',
    ];
}
