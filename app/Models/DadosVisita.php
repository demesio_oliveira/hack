<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DadosVisita extends Model
{
    protected $casts = ['id' => 'id'];

    //para deixar uuid como PK
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $table    = "dados_visita";
    protected $fillable = ['data_visita', 'medidor_encontrado', 'medidor_vizinho', 'medidor_antes', 'medidor_depois', 'verificador_id', 'impedimento_id', 'entrevista_id'];
}
