<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Problema extends Model
{
    protected $table    = "problema";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:problema,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do problema não informada.',
        'descricao.unique' => 'Descrição do problema já cadastrada.',
        'descricao.max' => 'Descrição do problema deve conter no máximo 75 caracteres.',
    ];

    public function checkProblemas($arraySelect) {
        return (in_array($this->id, array_column($arraySelect, 'problema_id'))) ? true : false;
    }
}
