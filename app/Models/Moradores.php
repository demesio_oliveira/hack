<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Moradores extends Model
{
    protected $table    = "moradores";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:moradores,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição dos moradores não informada.',
        'descricao.unique' => 'Descrição dos moradores já cadastrada.',
        'descricao.max' => 'Descrição dos moradores deve conter no máximo 75 caracteres.',
    ];
}
