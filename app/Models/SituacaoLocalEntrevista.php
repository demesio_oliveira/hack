<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SituacaoLocalEntrevista extends Model
{
    protected $table = 'situacao_local_entrevista';
    protected $fillable = ['entrevista_id', 'situacao_local_id'];

    public function entrevista()
    {
        return $this->belongsTo(\App\Models\Entrevista::class, 'entrevista_id', 'id');
    }

    public function situacao_local()
    {
        return $this->belongsTo(\App\Models\SituacaoLocal::class, 'situacao_local_id', 'id');
    }

    public function rules() {
        return [
            'entrevista_id' => 'required',
            'situacao_local_id' => 'required',
        ];
    }
    public $mesages = [
        'entrevista_id.required' => 'Entrevista não informada.',
        'situacao_local_id.required' => 'Situação do local não informada.',
    ];
}
