<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedidorEntrevista extends Model
{
    protected $table = 'medidor_entrevista';
    protected $fillable = ['entrevista_id', 'medidor_id'];

    public function entrevista()
    {
        return $this->belongsTo(\App\Models\Entrevista::class, 'entrevista_id', 'id');
    }

    public function medidor()
    {
        return $this->belongsTo(\App\Models\Medidor::class, 'medidor_id', 'id');
    }

    public function rules() {
        return [
            'entrevista_id' => 'required',
        ];
    }
    public $mesages = [
        'entrevista_id.required' => 'Entrevista não informada.',
    ];
}
