<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SituacaoLocal extends Model
{
    protected $table    = "situacao_local";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:situacao_local,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição da situação do local não informada.',
        'descricao.unique' => 'Descrição da situação do local já cadastrada.',
        'descricao.max' => 'Descrição da situação do local deve conter no máximo 75 caracteres.',
    ];

    public function checkSituacoesLocal($arraySelect) {
        return (in_array($this->id, array_column($arraySelect, 'situacao_local_id'))) ? true : false;
    }
}
