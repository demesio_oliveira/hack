<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusPadraoEntrada extends Model
{
    protected $table    = "status_padrao_entrada";
    protected $fillable = ['descricao'];

    public function rules() {
        return [
            'descricao' => 'required|max:75|unique:status_padrao_entrada,descricao'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'descricao.required' => 'Descrição do status de padrão entrada não informada.',
        'descricao.unique' => 'Descrição do status de padrão entrada já cadastrada.',
        'descricao.max' => 'Descrição do status de padrão entrada deve conter no máximo 75 caracteres.',
    ];
}
