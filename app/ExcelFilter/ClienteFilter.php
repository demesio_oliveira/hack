<?php
/**
 * Created by PhpStorm.
 * User: demesio
 * Date: 16/05/18
 * Time: 15:45
 */

namespace App\ExcelFilter;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class ClienteFilter implements IReadFilter
{
    private $rowLimit = 0;

    public function __construct($row = 0)
    {
        $this->rowLimit = $row;
    }


    /**
     * Should this cell be read?
     *
     * @param $column string Column address (as a string value like "A", or "IV")
     * @param $row int Row number
     * @param $worksheetName string Optional worksheet name
     *
     * @return bool
     */
    public function readCell($column, $row, $worksheetName = '')
    {
        return ($row > $this->rowLimit) ? true : false;
    }
}